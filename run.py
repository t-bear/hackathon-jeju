# -*- coding: utf-8 -*-
import os, sys
sys.path.insert(1, os.path.dirname(os.path.abspath(__file__)))

from backend.app import app

if __name__ == '__main__':
    if os.getenv('KOSHO_MODE') == 'DEV':
        set_debug = True
    else:
        set_debug = False

    app.run(
        host='0.0.0.0',
        port=int(sys.argv[1]) if len(sys.argv) > 1 else 5000,
        debug=set_debug,
        threaded=True
        )
