import pymongo
import csv
import xlrd
import numpy as np
import pandas as pd
import zipfile
import requests
import random
import time
import json
from bs4 import BeautifulSoup

from flask import Blueprint, jsonify, request
from datetime import datetime, timedelta

from .database import load_db_list, DB_JSON_PATH, get_db, Mysql, get_db_info
from .findata import db
from .util import utc
from .top10 import top10

api = Blueprint('api', __name__)

load_db_list(DB_JSON_PATH)
db.api = Mysql(get_db_info('qara'))
db.fin = get_db('fin')
db.ml = get_db('qara_ml')
db.kosho = get_db('kosho')
db.kosho3d = get_db('kosho3d')
db.kosho_map = get_db('kosho_map')
db.meta = get_db('meta')
db.ai = get_db('ai')
db.hackathon = get_db('hackathon')


@api.route('/ticker/search', methods=['GET'])
def json_stock_search():
    """@
        stock 정보를 검색한다
        #params POST keywork[str]`삼성` 질의어
        #params POST page[*int=1] 페이지
    """
    keyword = request.args.get('keyword', '')
    page = request.args.get('page', 1)
    if not keyword or not page:
        return jsonify({'msg': 'missing required param'})

    page = int(page)

    perpage = 20
    set_locale = 'us'
    cols = ['name', 'symbol', 'trans_en.name_trans', 'trans_ko.name_trans', 'trans_hk.name_trans', 'trans_zh-hans.name_trans']
    exchange_base = [1,2,60,110,130,19,21,20,131,68,54,103]  
    # USA 1:NY, 2:NASDQ, 
    # South Kore 60:KOSPI, 110:KOSDAQ, 130:KONEX, 
    # Singapore 19:SGX
    # Hong Kong 21:Hong Kong
    # Japan 20:Japan
    # Taiwan 68:Taiwan, 131:TPEX
    exchange_nation = {
        'kr':[60,110,130],
        'sg':[19],
        'us':[1,2],
        'hk':[21],
        'jp':[20],
        'tw':[68,131],
        'cn':[54,103]
    }
    exchange_cols = [x for x in exchange_base if x not in exchange_nation[set_locale]]
    # 주식 검색시 해당 국가 + US 주식만 검색하도록 변경 @kevin
    # exchange_cols = exchange_nation['us']

    query = {'$or': [{col:{'$regex':keyword, '$options':'i'}} for col in cols], 'exchange_ID':{'$in':exchange_nation[set_locale]}, 'security_type':'ORD'}
    sort_option = 'trans_en.name_trans'

    rows = list(db.fin.investing_stock.find(query, {'_id': 0, 'stock_symbol': 1, 'code': 1, 'trans_en.name_trans': 1 }).sort(sort_option))
    rows_first = rows[(page-1)*perpage:page*perpage+1]

    if len(rows_first) > perpage:
        return jsonify({'result': rows_first[:perpage], 'has_next':True})
    else:
        total_count = len(rows)
        rp = int(total_count / perpage + 1)
        rb = perpage - total_count % perpage
        query = {'$or': [{col:{'$regex':keyword, '$options':'i'}} for col in cols], 'exchange_ID':{'$in':exchange_cols}, 'security_type':'ORD'}

        if page == rp:
            rows_second = list(db.fin.investing_stock.find(query, {'_id': 0, 'stock_symbol': 1, 'code': 1, 'trans_en.name_trans': 1 }).sort(sort_option).limit(rb + 1))
        else:
            rows_second = list(db.fin.investing_stock.find(query, {'_id': 0, 'stock_symbol': 1, 'code': 1, 'trans_en.name_trans': 1 }).sort(sort_option).skip(page-rp*perpage+rb).limit(perpage + 1))
        

        if len(rows_first) -1 == perpage:
            has_next = len(rows_second) > 0
            return jsonify({'result': rows_first[:perpage], 'has_next':has_next})
        else:
            rows_result = rows_first + rows_second
            has_next = len(rows_result) > perpage

            return jsonify({'result': rows_result[:perpage], 'has_next':has_next})


@api.route('/ticker/report', methods=['GET'])
def json_ml_v3_predict():
    """@
        개별 지수 및 종목의 최신 report 조회
        @param GET ticker[str]`investing:312313`
        @param GET md_id[str]`kosho_index` index, stock, crypto
    """
    ticker = request.args.get('ticker', '')
    md_id = request.args.get('md_id', '')

    if not ticker or not md_id:
        return jsonify({'msg': 'missing required params'}), 400

    index_map = dict([(row['ticker'], row) for row in db.meta.sheet.find_one({'id':'index_crypto'})['data']]);
    ai_ticker = index_map[ticker]['id'] if md_id == 'kosho_index' else ticker + ';close';

    
    query_ts = {'md_id':str(md_id)}
    if md_id =='kosho_stock':
        tmp_code = ticker.split(":")[1]
        country_flag = db.fin.investing_stock.find_one({'code':tmp_code})['flag']
        query_ts.update({'flag':str(country_flag)})

    base_ts = db.ai.master.find_one(query_ts)['last_base_timestamp']

    pd = db.ai.marketdreamer.find_one({'md_id':md_id, 'ticker':ai_ticker, 'base_timestamp':base_ts}, {'_id':0})
    if not pd:
        return jsonify({'success':False, 'message':'no prediction found'})
    
    # report = pd[0]
    report = pd
    report['report_date_str'] = utc(report['base_timestamp'], 'kr').strftime('%Y-%m-%d')

    return jsonify({'success':True, 'report':report})

@api.route('/news/tag', methods=['GET'])
def get_news_with_tag():
    tag_list = request.args.get('tag', '').split(',')
    print(tag_list)

    return jsonify({'stock_news':tag_list})

@api.route("/fin/series")
def json_fin_series():
    """@#
        한 티커의 시리즈 데이터 가져오기

        @param GET ticker[string] src:code(;column)? 형태의 ticker
        @param GET days[*int=1095] 가져올 일수
    """
    ticker = request.args.get('ticker', '')
    src, code = ticker.split(':', 1)
    days = request.args.get('days', '')
    now = utc('kr')

    sd = (now - timedelta(days=days)).strftime('%Y%m%d')
    ed = now.strftime('%Y%m%d')
    extra = None

    if 'USTREASURY/HQMYC' in code:
        code, extra = code.split(';')

    code_meta = list(db.fin.code.find({'src': src, 'code': code}))
    if code_meta:
        code_meta = code_meta[0]
    else:
        inv_search(code)

        code_meta = list(db.fin.code.find({'src': src, 'code': code}))
        if not code_meta:
            return jsonify({'success': False, 'message': 'ticker [{}:{}] not found'.format(src, code)})
        code_meta = code_meta[0]

    ticker_series = fin(src, code, sd=sd, ed=ed, returns="dict")

    if code_meta.has_key('ohlc_key'):  # candle chart
        ohlc_map = zip(code_meta['ohlc_key'].split(','), 'open,high,low,close'.split(','))
        data = []
        for row in ticker_series:
            r = dict(d=row['d'])
            for bef, aft in ohlc_map:
                r[aft] = row[bef]
            data.append(r)
        chart_type = 'candle'
    else:
        data = []
        if code == 'USTREASURY/HQMYC':
            for row in ticker_series:
                data.append(dict(d=row['d'], close=row[extra]))
        else:
            if code == 'FMAC/HPI':
                main_key = 'United States not seasonaly adjusted'
            elif code_meta.has_key('main_key'):
                main_key = code_meta['main_key']
            elif src == 'quandl':
                main_key = 'Value'
            else:
                return jsonify({'success': False,
                           'message': 'main_key of ticker [{}:{}] not found'.format(src, code)})
            for row in ticker_series:
                data.append(dict(d=row['d'], close=row[main_key]))
        chart_type = 'area'

    # cachecontrol decorator에서 처리 @kevin
    # headers = {'Cache-Control': 'max-age={}'.format(3600)}
    # headers = {'Expires': utc(now + timedelta(seconds=3600)).strftime("%a, %d %b %Y %H:%M:%S GMT")}

    return jsonify({'success': True, 'chart_series': data, 'chart_type': chart_type})


def inv_search(q, tab_id='All', country_id='0', db_fin=None):
    """
        investing 의 정보를 검색한다
        
        검색된 정보는 db.fin.code에 남겨둔다
        tab_id: All, Indices, Stocks, ETFs, Funds, Commodities, Forex, Bonds
        country_id: 0-전체,
        
        python findata.py inv_search gungho
        
        @args db_fin[Database] 별도로 사용할 MongoClient Database
    """
    db_fin = db_fin or db.fin
    q = str(q)
    res = requests.post('https://www.investing.com/search/service/search', data='search_text='+q+'&term='+q+'&country_id='+str(country_id)+'&tab_id='+tab_id, headers=INVESTING_POST_HEADERS_EN)
    
    all = json.loads(res.text)['All']
    ilog(len(all), 'records found')
    
    codes = []
    for row in all:
        if db_fin.code.find_one({'code':str(row['pair_ID']), 'src':'investing'}):
            continue
        isin = None
        if row['pair_type'] in ['equities','etf','fund']:
            ilog('Fetching', row['pair_ID'], 'ISIN ...')
            res = requests.get('https://www.investing.com'+row['link'], headers=INVESTING_GET_HEADERS).text.replace('\n','')
            isin = str_slice(res, '<span>ISIN:<.*?<span class="elp".*?>', '<')
            if isin == None:
                wlog(row['pair_ID'], 'isin not found')
                dlog(res)
            else:
                isin = isin.replace('&nbsp;','').replace(' ','')
            
        obj = dict(code=str(row['pair_ID']), name=row['name'], eng_name=row['trans_name'], isin=isin, type=row['pair_type'], url=row['link'].split('?')[0], src='investing', org=row, save_time=utc(), main_key='close', ohlc_key='open,high,low,close')
        db_fin.code.update_one({'code':obj['code'], 'src':'investing'}, {'$set':obj}, True)
    return all



def fin(src, code=None, sd=None, ed=None, cols=None, order='asc', returns='pandas', db_fin=None, option=None):
    """
        @args src[str]`krx` 가격을 가져오려는 source(krx, investing, bloomberg, quandl). source:code 형태로 사용할 수도 있다.
        @args code[str] 각 데이터 소스별 구분코드
        @args sd[str] YYYYMMDD 형식의 시작일자
        @args ed[str] YYYYMMDD 형식의 종료일자
        @args cols[list] 데이터 컬럼중 projection할 컬럼명
        @args order[str] 날짜기반 정렬순서(asc, desc)
        @args returns[str] list, pandas 등을 선택 가능. 기본은 pandas
        @args db_fin[Database] 별도로 사용할 MongoClient Database
        @args option[dict] src별 선택적 옵션
            release : YYYYMMDD 형식의 date 가 주어진 경우 해당 일자에 맞는 데이터를 이용
            method : 미정
            sheet: src가 xls 일 경우 사용할 sheet의 이름
            cell_range : src가 xls 일 경우 사용할 cell의 범위
            bb_api : bloomberg 단말기의 주소
            no_update: update를 하지 않고 DB의 데이터만 가져온다
    """
    if ':' in src and code == None:
        src, code = src.split(':', 1)
    
    option = option or {}
    db_fin = db_fin or db.fin
    db_fin.code.ensure_index([('code', pymongo.ASCENDING)])
    db_fin.prepare.ensure_index([('code', pymongo.ASCENDING)])
    
    if src in ['investing', 'krx', 'mirae', 'quandl', 'bloomberg', 'ext_db']:
        rows = prepare(src, code, sd, ed, db_fin=db_fin, **option)
    elif src == 'custom_xls':
        rows = prepare(src, code, sd, ed)
        if option.get('release'):
            for row in rows:
                if row.get('release_cnt', 1) > 1: # 정렬되어있음을 전제로 함
                    rel = list(filter(lambda a:a['d'] < option.get('release'), row['release']))
                    if not rel:
                        rel = list(filter(lambda a:a['d'] in ['99999999', '99999998'], row['release']))
                        if rel:
                            rel = rel[-1]
                        else:
                            rel = row['release'][0]
                    else:
                        rel = rel[-1]
                    row['release_date'] = rel['d']
                    row['amount'] = rel['amount']
    elif src == 'xls': # 파일 종류는 캐싱(prepare) 하지 않고 바로 사용한다.
        rows = xls_history(code, sd, ed, sheet=option.get('sheet'), cell_range=option.get('cell_range'))
    elif src == 'csv':
        rows = csv_history(code, sd, ed)
    elif src == 'file':
        rows = file_history(code, sd, ed)
    else:
        rows = []
    
    rows.sort(key=lambda a:a.get('d'))
    if order != 'asc':
        rows = rows.reverse()
    if returns == 'pandas':
        rows = pd.DataFrame(rows, index=pd.to_datetime([row['d'] for row in rows]))
        rows.index.names = ['d']
        if len(rows):
            rows = rows[list(set(rows.columns) - {'code','d'})]

    return rows



@api.route('/news/search', methods=['GET'])
def get_news():
    print(request.args)
    symbol = request.args.get('symbol', '')
    print(symbol)
    stock_news = list(db.hackathon.news.aggregate([
        {'$match': {'symbol': symbol}},
        {'$project': {'_id': 0, 'time':0}},
        {'$limit': 200}
    ]))

    news_title = [x['title'] for x in stock_news]
    top_news = top10(news_title)

    ret = [cand for cand in stock_news if cand['title'] in top_news]
    print (ret)

    sum = 0.0
    for s in ret:
        sum += s['predict']
    sum /= len(ret)
    print(ret)
    
    return jsonify({'stock_news': ret, 'predict': sum})