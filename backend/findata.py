from .init_db import get_db
import pymongo


db = get_db()
db.db_fin = pymongo.MongoClient('db.qara.kr')['fin'] # 명시적으로 db.qara.kr을 이용할 경우