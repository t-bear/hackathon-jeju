# -*- coding: utf-8 -*-
"""
    python에서 사용되는 공통기능 모음
"""
import requests, pytz, phonenumbers, socket, hashlib, sys, os, re, json, ujson, base64, inspect, time, random, traceback, string, flask, logging, logging.handlers
import dateutil.parser
import xlsxwriter
import xlsxwriter.utility as xlsutil
from _thread import *
import pickle
import numpy as np
from threading import Thread, Lock
from subprocess import Popen, PIPE
from datetime import datetime, timedelta, date, tzinfo
from functools import wraps
from flask import request, jsonify, abort, g, make_response, session
from io import StringIO as SIO
from Crypto.Cipher import AES
from multiprocessing import Queue
from collections import deque


# json_util.default 를 변형한 json_default 용 import
import calendar
import uuid

import numpy as np
from decimal import Decimal
from bson import EPOCH_AWARE, RE_TYPE, SON
from bson.binary import Binary
from bson.code import Code
from bson.dbref import DBRef
from bson.max_key import MaxKey
from bson.min_key import MinKey
from bson.objectid import ObjectId
from bson.regex import Regex
from bson.timestamp import Timestamp
from bson.py3compat import PY3, text_type


# http://stackoverflow.com/questions/1132941/least-astonishment-in-python-which-scope-is-the-mutable-default-argument-in
# default로 주는 {}, [] 등의 객체는, 함수 내에서 변경하게 되면 값이 계속 유지되게 되므로 매우 주의해야 한다.
# orders=[] 에 1 을 추가하여 [1] 이 되었다면, 다음번 default값 호출 때는 orders=[1] 로 시작하는 것과 마찬가지이다.


### random 관련

rand = random.random # rand()
randint = random.randint # randint(2,4) : 2 ~ 4
def randchr():
    pool = " _0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
    return randone(pool)
    
def randstr(length=10):
    return ''.join(map(lambda a:randchr(), range(length)))

def randword(length=10):
    word_pool = ["small","big","large","heavy","light","married","single","old","young","dangerous","strong","weak","youth","adult","gentleman","lady","host","people","son","daughter","baby","child","wife","husband","fish","gold fish","jelly fish","star fish","shellfish","dolphin","whale","shark","head","shoulder","knee","toe","elbow","forehead","eyebrow","hip","chest","waist","traffic","traffic jam","traffic light","street light","bus stop","subway","subway station","ticket","train","train station","car","parking lot","cook","start","throw","wait","catch","repeat","give","send","speak","assist","lift","collect","mark","build","sing","guide","dig","spend","shake","cure","buy","pour","steal","worry","ride","forgive","hang","fight","ring","run","stir","put"]
    str = randone(word_pool)
    while len(str) < length:
        str += " " + randone(word_pool)
    return str[:length]

def randone(arr):
    """ 주어진 list중 하나를 랜덤하게 선택 """
    return arr[random.randint(0,len(arr)-1)]

def randprob(arr, prob): # arr, prob = ["data", "data2"], [30, 50]
    """ 주어진 list와 확률에 맞게 하나를 랜덤하게 선택 """
    # prob의 sum을 구한다
    total = sum(prob)
    random_point = random.random()*total
    current_prob = prob[0]; current_index = 0
    while current_prob < random_point:
        current_index += 1
        current_prob += prob[current_index]
    return arr[current_index]




### http 관련

def http_get(url, params={}, headers={}, cookies={}, encoding=None, debug=False):
    """
        http get 요청을 보낸다.
    """
    try:
        r = requests.get(url, params=encode_data(params), headers=encode_data(headers), cookies=cookies)
        if debug: print(r.status_code, url)
        # r.text는 charset을 적절히 파악하여 unicode로 받아온다.
        # euckr과 utf8이 섞인 경우는 utf8쪽에 맞게 받아오고 euckr은 뭉개진다.
        return r.content.encode(encoding or r.encoding)
    except Exception as e: # timeout error 등
        print('Exception in http_get with url '+url+', '+str(e))
        #raise e

def http_post(url, params={}, json=None, headers={}, cookies={}, encoding=None, debug=False):
    """
        http post 요청을 보낸다.
    """
    try:
        r = requests.post(url, data=encode_data(params) if not json else None, json=encode_data(json) if json else None, headers=encode_data(headers), cookies=cookies)
        if debug: print(r.status_code, url)
        # r.text는 charset을 적절히 파악하여 unicode로 받아온다.
        # euckr과 utf8이 섞인 경우는 utf8쪽에 맞게 받아오고 euckr은 뭉개진다.
        return r.content.encode(encoding or r.encoding)
    except Exception as e: # timeout error 등
        print('Exception in http_post with url '+url+', '+str(e))
        #raise e

def thread_get(url, params={}, headers={}, cookies={}, encoding=None, debug=False):
    """
        http get 요청을 thread로 보낸다. log 서버 등으로 보낼 때 사용한다.
    """
    t = Thread(target=http_get, args=(url, encode_data(params), encode_data(headers), cookies, encoding, debug))
    t.daemon = True
    t.start()

def thread_post(url, params={}, headers={}, cookies={}, encoding=None, debug=False):
    """
        http post 요청을 thread로 보낸다. log 서버 등으로 보낼 때 사용한다.
    """
    t = Thread(target=http_post, args=(url, encode_data(params), encode_data(headers), cookies, encoding, debug))
    t.daemon = True
    t.start()


### thread 관련

def start_thread(target, args=(), kwargs=None, name=None, daemon=True, start=True):
    t = Thread(target=target, name=name, args=args, kwargs=kwargs)
    t.daemon = daemon
    if start:
        t.start()
    return t


### network 관련

def tcp(host=None, port=None, myport=None, timeout=None):
    """
        tcp 소켓을 생성하여 연결까지 한 뒤 반환한다. myport가 있다면 bind시킨다.
    """
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    if myport: s.bind(('',myport))
    if timeout: s.settimeout(timeout)
    if host and port:
        s.connect((host,port))
    return s

def udp(myport=None, timeout=None):
    """
        udp 소켓을 생성하여 반환한다. myport가 있다면 bind시킨다.
    """
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    if myport: s.bind(('',myport))
    if timeout: s.settimeout(timeout)
    return s

def etcp(host, port, myport=None, timeout=None):
    """
        extended tcp 소켓을 생성한다. 4byte의 헤더로 length값을 보내서 빠른 송수신이 가능하게 한다.
    """
    pass

def eudp(host, port, myport=None, timeout=None):
    """
        extended udp 소켓을 생성하여 반환한다. 1472 보다 큰 size의 데이터도 보내고 받을 수 있도록 한다.
    """
    pass

def local_ip():
    """ local ip 알아내기 """
    # 아래 두 방식은 임의지정 hostname에서는 socket.gaierror: [Errno -2] Name or service not known 가 발생한다.
    #socket.getaddrinfo(socket.gethostname(), None)[0][4][0] # ::1 이 나오기도 한다.
    #socket.gethostbyname(socket.gethostname()) # 127.0.0.1이 나오기도 한다.

    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(('10.0.0.0', 0))
    return s.getsockname()[0]

def network_ip():
    """ internet ip 알아내기 """
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(('8.8.8.8', 0))
    return s.getsockname()[0]
    
def public_ip():
    """ public ip 알아내기 """
    return http_get('http://ipinfo.io/ip').strip()
    
    

### dict 관련

def copy_dict(d):
    """
        request.form 이나 request.args 가 일반적인 dict가 아니기에 복제해서 사용할 경우
    """
    return dict([(k,v) for k,v in d.iteritems()])

def copy_params(d):
    """
        copy_dict를 Params로 반환
    """
    return Params([(k,v) for k,v in d.iteritems()])



### object 관련

def printdir(obj, run_method=False, lines=None):
    """
        dir을 통해 가져온 object의 attribute, value 를 전부 출력한다.
    """
    for key in dir(obj):
        attr = getattr(obj, key)
        if key == '__doc__' and attr:
            print(key, ':\n', ''.join(map(lambda s:'    '+s+'\n', attr.splitlines()[:lines])))
        elif hasattr(attr, '__call__'):
            try:
                if run_method:
                    print(key, ':', attr())
                else:
                    print(key, ':', attr)
            except:
                print(attr)
            finally:
                if hasattr(attr, '__doc__') and attr.__doc__ != None:
                    print(''.join(map(lambda s:'    '+s+'\n', attr.__doc__.splitlines()[:lines])))
        else:
            print(key, ':', attr)

def printvars(obj):
    """
        vars를 통해 가져온 object의 key, value 를 전부 출력한다.
    """
    try:
        d = vars(obj)
    except:
        return
    for k,v in d.iteritems():
        print(k, ':', v)

def printsource(obj):
    """
        function object를 받아서 그 소스파일을 출력한다
    """
    try:
        print(inspect.getsource(obj))
    except IOError as e:
        print(e.message)
        import dis
        dis.dis(obj)

def decomppyc(name):
    """
        pyc 파일위치를 받아서 그 소스파일을 출력한다
        https://github.com/wibiti/uncompyle2
    """
    try:
        import uncompyle2 as uc
        m = MemoryFile()
        uc.uncompyle_file(name, m)
        print(m.str.decode('string_escape'))
    except Exception as e:
        print(e.message)



### Excel 관련 (xlsxwriter)

def row2xls(rows, cols=None, file=None, workbook=None, sheetname=None):
    """
        2차원 배열 데이터(list of dict or list of list)를 엑셀로 바꿔서 반환한다
        list of dict인 경우 순서는 cols의 순서를 따라가거나 첫 행의 items()의 순서에 의존한다
        list of list인 경우 cols를 단순 제목행으로 나열하고 나머지는 rows의 데이터를 나열한다
        col 중 tzinfo를 담고 있는 datetime이 있다면 kr 기준으로 바뀌게 된다
        @args rows[list] list들 혹은 dict들을 담고 있는 list
        @args cols[list] (key, 이름) list 혹은 이름 list
        @args file[str|IO] 저장할 파일명 혹은 파일객체나 IO를 받아온다. 지정되지 않았다면 workbook을 닫지 않고 반환한다
    """
    # Create an new Excel file and add a worksheet.
    wb = workbook or xlsxwriter.Workbook(file)
    ws = wb.add_worksheet(sheetname)
    
    #ws.set_column('A:A', 20) # 너비 조절
    #ws.set_row(1, 30) # 높이 조절
    bold = wb.add_format({'bold': True}) # 스타일 프리셋 추가
    format_d = wb.add_format({'num_format': 'yyyy.mm.dd'})
    format_dt = wb.add_format({'num_format': 'yyyy.mm.dd hh:mm:ss'})
    def write_data(worksheet, y, x, data):
        if type(data) == datetime:
            if data.tzinfo:
                worksheet.write_datetime(y, x, utc(data, 'kr').replace(tzinfo=None), format_dt)
            else:
                worksheet.write_datetime(y, x, data, format_dt)
        elif isinstance(data, date):
            worksheet.write_datetime(y, x, data, format_d)
        else:
            worksheet.write(y, x, data)

    max_len = {}
    if rows:
        rows = decode_data(rows)
        line = 0; col_is_tuple = False
        if cols: # 제목행이 있는 경우
            cols = decode_data(cols)
            line += 1
            if isinstance(cols[0], (tuple, list)):
                col_is_tuple = True
                for i, col in enumerate(cols):
                    ws.write(0, i, col[1], bold)
                    max_len[i] = len(unicode(col[1]).encode('euckr'))
            else:
                for i, col in enumerate(cols):
                    ws.write(0, i, col, bold)
                    max_len[i] = len(unicode(col).encode('euckr'))
        if isinstance(rows[0], dict): # dict인 경우 cols 순서에 맞게 나열, cols에 없는 경우 제외
            for y, row in enumerate(rows):
                if not cols:
                    cols = row.keys()
                for x, col in enumerate(cols):
                    data = row.get(col[0] if col_is_tuple else col, '')
                    data_len = len(unicode(data).encode('euckr'))
                    write_data(ws, y+line, x, data)
                    max_len[x] = max_len.get(x, 0) if max_len.get(x, 0) > data_len else data_len
        elif isinstance(rows[0], (list, tuple)): # list인 경우 단순나열
            for y, row in enumerate(rows):
                for x, col in enumerate(row):
                    data_len = len(unicode(col).encode('euckr'))
                    write_data(ws, y+line, x, col)
                    max_len[x] = max_len.get(x, 0) if max_len.get(x, 0) > data_len else data_len
        else: # 한 줄에 전부 나열
            for x, col in enumerate(rows):
                data_len = len(unicode(col).encode('euckr'))
                write_data(ws, line, x, col)
                max_len[x] = max_len.get(x, 0) if max_len.get(x, 0) > data_len else data_len
            
    # max_len에 맞춰서 사이즈를 조절한다
    for x, size in max_len.items():
        ws.set_column(cell(x)+':'+cell(x), 50 if size > 50 else size if size > 6 else 6)

    if file: # 주어진 file에 바로 저장하는 경우
        wb.close()
        if hasattr(file, 'seek'): # 파일 객체라면 되돌린다
            file.seek(0)
        return file
    
    return wb # 닫지 않고 반환한다

def cell(*args, **kwargs):
    """
        다양항 방식으로 cell <-> rowcol 변환을 한다
        import xlsxwriter.utility as xlsutil
        cell(1, 2) # = xl_rowcol_to_cell(1, 2)  # C2
        cell(0, 0, row_abs=True, col_abs=True) # = xl_rowcol_to_cell(0, 0, row_abs=True, col_abs=True)  # $A$1
        cell('B1') # = xl_cell_to_rowcol('B1')    # (0, 1)
        cell('A1:B2') # = xl_cell_to_rowcol('B1')    # (0, 0, 1, 1)
        cell(1) # = xl_col_to_name(1)    # B
        cell(0, True) # = xl_col_to_name(0, True)   # $A
        cell(0, 0, 9, 0) # = xl_range(0, 0, 9, 0)  # A1:A10
        cell(0, 0, 9, 0, abs=True) # = xl_range_abs(0, 0, 9, 0)  # $A$1:$A$10
    """
    if len(args) == 0:
        return None
    if type(args[0]) == int:
        if len(args) == 1 or len(args) == 2 and type(args[1]) == bool:
            return xlsutil.xl_col_to_name(*args)
        if len(args) == 2:
            return xlsutil.xl_rowcol_to_cell(*args, **kwargs)
        if len(args) == 4:
            if kwargs.get('abs'):
                return xlsutil.xl_range_abs(*args)
            else:
                return xlsutil.xl_range(*args)
    elif isinstance(args[0], basestring):
        if ':' in args[0]:
            st, ed = args[0].split(':')
            return xlsutil.xl_cell_to_rowcol(st) + xlsutil.xl_cell_to_rowcol(ed)
        return xlsutil.xl_cell_to_rowcol(args[0])
    return None

def xlstest():
    xls = rows2xls([[0,1,2],[3,4],[5,6,7,8]], cols=['가','나','다'])
    f = open('aa.xlsx', 'w')
    f.write(xls.getvalue())
    f.close()



### CSV 관련

def escape_csv(v):
    if isinstance(v, (str, int, float, bool)):
        v = str(v)
    elif v:
        v = printable_data(v)
    else:
        v = ''
    if ',' in v or '\n' in v or '"' in v:
        if '"' in v:
            v = v.replace('"', '""')
        v = '"' + v + '"'
    return v



### 정보 출력 관련

def printpip():
    """
        pip 를 이용하여 설치된 모듈들을 출력한다.
        콘솔에서 다음 커맨드들을 이용해도 된다.
        pydoc modules
        python -c 'help("modules")'
        pip list
        pip freeze
    """
    import pip,pprint
    pprint.pprint(pip.get_installed_distributions())



### encode, decode 관련

def encode_data(d, depth=10, encoding='utf8'):
    """
        encode unicode to utf-8(or selected encoding) str
        convert Decimal to flot
        depth is dict's or list's depth. ex) {'depth 1':{'depth 2':'some value'}, 'depth 1':'some value'}
    """
    if depth < 0:
        return d
    elif type(d) is Params:
        return Params(map(lambda k,v:(encode_data(k, encoding=encoding), encode_data(v, depth-1, encoding)), vars(d).iteritems() ))
    elif isinstance(d, dict):
        return dict(map(lambda k,v:(encode_data(k, encoding=encoding), encode_data(v, depth-1, encoding)), d.iteritems() ))
    elif isinstance(d, list):
        return [encode_data(data, depth-1, encoding) for data in d]
    elif isinstance(d, tuple):
        return tuple([encode_data(data, depth-1, encoding) for data in d])
    elif isinstance(d, set):
        return set([encode_data(data, depth-1, encoding) for data in d])
    elif isinstance(d, unicode):
        return d.encode(encoding)
    elif isinstance(d, Decimal):
        return float(d)
    elif isinstance(d, str):
        try:
            return d.decode('utf8').encode(encoding)
        except UnicodeDecodeError:
            try:
                return d.decode('euckr').encode(encoding)
            except UnicodeDecodeError:
                return d
    else:
        return d

def decode_data(d, depth=10):
    """
        decode to unicode from any str
        depth is dict's or list's depth. ex) {'depth 1':{'depth 2':'some value'}, 'depth 1':'some value'}
    """
    if depth < 0:
        return d
    elif type(d) is Params:
        return Params(map(lambda k,v:(decode_data(k), decode_data(v, depth-1)), vars(d).iteritems() ))
    elif isinstance(d, dict):
        return dict(map(lambda k,v:(decode_data(k), decode_data(v, depth-1)), d.iteritems() ))
    elif isinstance(d, list):
        return [decode_data(data, depth-1) for data in d]
    elif isinstance(d, tuple):
        return tuple([decode_data(data, depth-1) for data in d])
    elif isinstance(d, set):
        return set([decode_data(data, depth-1) for data in d])
    elif isinstance(d, str):
        try:
            return d.decode('utf8')
        except UnicodeDecodeError:
            try:
                return d.decode('euckr')
            except UnicodeDecodeError:
                return d
    else:
        return d

def decode_decimal(d):
    """
        convert Decimal to flot
    """
    if type(d) is Params:
        return Params(map(lambda a:(decode_decimal(a[0]), decode_decimal(a[1])), vars(d).items()))
    elif isinstance(d, dict):
        return dict(map(lambda a:(decode_decimal(a[0]), decode_decimal(a[1])), d.items()))
    elif isinstance(d, list):
        return [decode_decimal(data) for data in d]
    elif isinstance(d, tuple):
        return tuple([decode_decimal(data) for data in d])
    elif isinstance(d, set):
        return set([decode_decimal(data) for data in d])
    elif isinstance(d, Decimal):
        return float(d)
    elif isinstance(d, float):
        if d != d: # nan
            return None
        if d == float('inf'): # infinity
            return None
        return float(d)
    elif isinstance(d, (np.int, np.int0, np.int8, np.int16, np.int32, np.int64, np.uint, np.uint0, np.uint8, np.uint16, np.uint32, np.uint64)):
        return int(d)
    return d


### parameter 관련

# deprecated. 고민의 흔적이 아까워서 남겨둔다.
class OldParams(object):
    """ 
        값 저장용 빈 class.
        정상적인 동작을 위해 'get' 이나 'keys' 등은 key로 쓰지 말아야 한다.
        dict 형태의 참조를 가져오려면 p.__dict__ 나 vars(p)를, copy를 가져오려면 dict(p)를 하면 된다.
    """
    def __init__(self, d=None): # d는 dict이어야 한다
        if type(d) == dict:
            self.__dict__ = d
    def __getitem__(self, key): # p['x']
        # return getattr(self, key, None) # TypeError: getattr(): attribute name must be string
        return self.__dict__.get(key, None) # key가 숫자나 bool, 객체인 경우 커버하기 위해 사용
    def __setitem__(self, key, item): # p['x'] = 1
        # setattr(self, key, item) # TypeError: attribute name must be string
        self.__dict__[key] = item # key가 숫자나 bool, 객체인 경우 커버하기 위해 사용
    def __delitem__(self, key): # del p['x']
        # delattr(self, key)
        del self.__dict__[key]
    def __getattr__(self, key): # p.x 값이 없을 때 호출된다.
        print(key)
        return None
    def __iter__(self): # list(), tuple() 대응
        return self.__dict__.__iter__()
    def __len__(self): # list(), tuple() 대응
        return self.__dict__.__len__()
    def __repr__(self): # repr() 대응
        return self.__dict__.__repr__()
    def __str__(self): # print(), str() 대응
        return self.__dict__.__str__()
    def __add__(self, other): # p + {} 구현
        self.__dict__.update(other)
        return self
    def __radd__(self, other): # {} + p 구현
        other.update(self.__dict__)
        return other
    def __sub__(self, other): # p - {} or p - [] 구현
        map(lambda x:self.__dict__.pop(x,None), list(other))
        return self
    def __rsub__(self, other): # {} - p 구현
        map(lambda x:other.pop(x,None), self.__dict__.keys())
        return other
    def get(self, key, default=None): # p.get('x')
        return self.__dict__.get(key, default)
    def keys(self): # dict 대응
        return self.__dict__.keys()
    def update(self, other): # {}.update 구현, 가능한한 __add__ 를 사용한다.
        return self.__dict__.update(other)

class Params(dict):
    """ 
        값 저장용 dict의 wrapper. p.key == 'value' 등으로 접근할 수 있다.
        정상적인 동작을 위해 'get' 이나 'keys' 등의 dict의 attribute는 key로 쓰지 말아야 한다.
        dict 형태의 참조를 가져오려면 p.__dict__ 나 vars(p)를, copy를 가져오려면 dict(p)를 하면 된다.
        +, +=, -, -= 등을 사용할 수 있다
    """
    def __init__(self, *args, **kwargs):
        dict.__init__( self, *args, **kwargs )
        self.__dict__ = self
    def __getattr__(self, key): # p.x 값이 없을 때 호출된다.
        if key[:2] == '__' and key[-2:] == '__': # 내부 함수 접근 시도시 없는 경우는 에러, flask render_template 등의 문제 해결
            raise AttributeError("'Params' object has no attribute '%s'" % key)
        return None
    def __add__(self, other): # p + {} 구현
        d = dict(self); d.update(other)
        return d
    def __iadd__(self, other): # p += {} 구현
        self.__dict__.update(other)
        return self
    def __radd__(self, other): # {} + p 구현
        d = dict(other); d.update(self)
        return d
    def __sub__(self, other): # p - {}, [], (), set 구현
        d = dict(self)
        map(lambda x:d.pop(x,None), list(other))
        return d
    def __isub__(self, other): # p -= {}, [], (), set 구현
        map(lambda x:self.__dict__.pop(x,None), list(other))
        return self
    def __rsub__(self, other): # {}, [], (), set - p 구현
        # remove로 작동하여 같은 요소가 여럿 있을 경우 하나씩 제거된다.
        # ex) [3,3,4,'b','b'] - Params({'b':1}) #-> [3,3,4,'b']
        if isinstance(other, dict):
            d = dict(other)
            map(lambda x:d.pop(x,None), self.__dict__.keys())
            return d
        elif isinstance(other, (list, tuple, set)):
            l = list(other)
            map(lambda x:l.remove(x) if x in l else None, self.__dict__.keys())
            if isinstance(other, tuple):
                return tuple(l)
            if isinstance(other, set):
                return set(l)
            return l
        else:
            raise TypeError(other + 'is not dict, list, tuple, set')
    def __html__(self):
        return self.__json__()
    def __json__(self):
        return to_json(self)

class DefaultParams(Params):
    """
        default를 설정해 놓을 경우 만약 self에 attr이 없는 경우 default로 넘긴다.
        다음과 같은 상황에서 이용 가능하다.
        
        db = DefaultParams()
        db.default = db.api = Redis()
        db.cache = Redis(db=1)
        db.g_all() # -> db.api.g_all()가 호출된다. redis는 dict와 겹치는 메소드가 많아서 사용하기에 좋지 않다.
    """
    def __init__(self, *args, **kwargs):
        Params.__init__( self, *args, **kwargs )
    def __getattr__(self, key): # p.x 값이 없을 때 호출된다.
        if self.get('default'): # default가 있는 경우 default에게 넘긴다
            return getattr(self.get('default'), key, None)
        return None


# doc2param cache용 함수들
def param_default(default_value):
    return lambda v:default_value if not v else default_value
def param_required(k):
    def required(v):
        if not v:
            raise KeyError('parameter not given: '+k)
        return v
    return required
def param_file(k):
    return lambda v:request.files.get(k)
def parse_bool(v):
    if v.upper() == 'TRUE':
        return True
    elif v.upper() == 'FALSE':
        return False
    else:
        raise ValueError('could not convert string to bool: '+v)


#proc_cache = Params()

def doc2param(req=None, all=False):
    """
        주석을 통해 필요한 객체들을 GET, POST 등으로 가져오며 Params 객체를 반환한다.
        필수 객체가 없는 경우 custom ValueError를 raise 한다.
        all이 True인 경우 request에 담긴 파라미터들을 모두 Params에 넣는다.
        doc2param은 cache를 사용할 경우 기존 방식보다 2배정도 느리며 사용하지 않을 경우 3배정도 느리다.
        그러나 redis 등 외부에 비하면 100 ~ 1000배 정도 빠르므로 큰 문제는 되지 않는다.
    """
    p = Params()
    if req == None: # flask
        args = request.args
        form = request.form
        files = request.files
    else: # django
        args = req.GET
        form = req.POST
        files = req.FILES
    # traceback.extract_stack()[-1][2] # current function name
    # traceback.extract_stack()[-2][2] # before function name
    # inspect.stack()[0] => (<frame object at 0x7fa4fc006a80>, '/home/ssh/bridge/apps/../../bridge_apps/python_common/bridge_common.py', 242, 'doc2param', ['    print inspect.stack()[0]\n'], 0)
    # inspect.stack()[0][0] # current frame
    # inspect.stack()[0][3] # current function name
    # before_stack = inspect.stack()[1] # 너무 느려서(100배) sys로 교체하였다.
    # f = before_stack[0] # before frame
    # before_stack[3] # before function name
    # f.f_globals[f.f_code.co_name].__doc__ # frame function doc
    # doc = f.f_code.co_consts[0] or '' # 주석의 추정 위치, 주석이 없는 경우 None
    before_f_code = sys._getframe().f_back.f_code
    before_name = before_f_code.co_name

    """
    ### cache pattern
    if not proc_cache.get(before_name):
        before_doc = before_f_code.co_consts[0] or '' # 주석의 추정 위치, 주석이 없는 경우 None
        params = re.findall('\\s*@param ([A-Z]+)( ([^\\[ ]+)\\[(\\*?)([a-z_A-Z]*)(\\=([^\\]]*))?\\](`[^`]*`)?\\s*?(.*))?', before_doc)
        # @param A b[*c=d]`e` f => ('A', ' b[*c=d]`e` f', 'b', '*', 'c', '=d', 'd', '`e`', 'f')
        # index : A = 0, b = 2, * = 3, c = 4, d = 6, `e` = 7, f = 8
        
        # preprocessing
        param_process = {'get':{},'post':{}}
        for param in params:
            if param[0] in ['GET','POST']:
                key = param[2]
                procs = []
                # default value
                if param[6]:
                    procs.append(param_default(param[6]))
                # required check
                if param[3] != '*': # 공백('')도 required에 위배된 것으로 판단한다.
                    procs.append(param_required(param[3]))
                # type check, string|bool|int|float|file|jsonObject|jsonArray|jsonObjectArray|fixed|enum
                if param[4] == 'file':
                    procs.append(param_file(key))
                elif param[4] == 'int':
                    procs.append(int)
                elif param[4] == 'float':
                    procs.append(float)
                elif param[4] == 'bool':
                    procs.append(parse_bool)
                param_process[param[0].lower()][key] = procs
        
        proc_cache[before_name] = param_process
        print 'caching'
    else:
        param_process = proc_cache.get(before_name)

    # processing
    try:
        for k, v in param_process['get'].iteritems():
            p[k] = reduce(lambda v,f:f(v), [args.get(k)]+v)
        for k, v in param_process['post'].iteritems():
            p[k] = reduce(lambda v,f:f(v), [form.get(k)]+v)
    except ValueError as e:
        raise ValueError('key '+k+' : '+e.message)
    
    """
    
    ### non-cache pattern
    before_doc = before_f_code.co_consts[0] or '' # 주석의 추정 위치, 주석이 없는 경우 None
    params = re.findall('\\s*@param ([A-Z]+)( ([^\\[ ]+)\\[(\\*?)([a-z_A-Z]*)(\\=([^\\]]*))?\\](`[^`]*`)?\\s*?(.*))?', before_doc)
    
    for param in params:
        if param[0] in ['GET','POST']:
            # flask, django 둘 다 dict(args) 등을 하면 k:[v, v2, v3] 형태로 되지만, get을 했을 때 flask는 v를, django는 v3를 가져온다
            v = (args if param[0] == 'GET' else files if param[4] == 'file' else form).get(param[2])
            # default value
            if (param[6] or param[5] == '=') and not v: # param[5] == '=' 의 경우 [*string=]의 형태.
                v = param[6]
            # required check
            if param[3] != '*' and not v: # 공백('')도 required에 위배된 것으로 판단한다.
                raise KeyError('parameter not given: '+param[2])
            # type check, string|bool|int|float|file|jsonObject|jsonArray|jsonObjectArray|fixed|enum
            try:
                if param[4] == 'file': # 항상 list로 반환
                    v = files.getlist(param[2])
                elif v:
                    if param[4] == 'int':
                        v = int(v)
                    elif param[4] == 'float':
                        v = float(v)
                    elif param[4] == 'bool':
                        if v.upper() == 'TRUE':
                            v = True
                        elif v.upper() == 'FALSE':
                            v = False
                        else:
                            raise ValueError('could not convert string to bool: '+v)
            except ValueError as e:
                raise ValueError('key '+param[2]+' : '+e.message)
            p[param[2]] = v
    
    if all:
        for k, v in args.iteritems():
            if p.get(k) == None:
                p[k] = v
        for k, v in form.iteritems():
            if p.get(k) == None:
                p[k] = v
    
    return p

def doc2class():
    """
        미완성.
        @result 주석을 통해 필요한 field를 담을 수 있는 class를 생성한다.
    """
    before_f_code = sys._getframe().f_back.f_code
    before_name = before_f_code.co_name





#### string 관련

def str_slice(s, pre, post, idx=0):
    """ 
        s 안에서 pre와 post 사이의 idx 번째 문자열을 반환한다. idx는 0부터 시작한다.
        str_slice("<tr><td>data</td></tr>","<td>","</td>") => "data"
    """
    arr = re.findall('('+pre+')([\s\S]*?)('+post+')', s)
    data_idx = 1+pre.replace('\\(','').count('(')
    if idx >= len(arr): return None
    return arr[idx][data_idx]

def str_slice_inline(s, pre, post, idx=0):
    """ 
        s 안에서 pre와 post 사이의 idx 번째 문자열을 반환한다. idx는 0부터 시작한다.
        pre와 post는 동일 line에 존재해야 한다.
        str_slice_inline("<tr><td>data\n1</td><td>data2</td></tr>","<td>","</td>") => "data2"
    """
    arr = re.findall('('+pre+')(.*?)('+post+')', s)
    data_idx = 1+pre.replace('\\(','').count('(')
    if idx >= len(arr): return None
    return arr[idx][data_idx]

def str_split(s, pre, post):
    """
        str_split("<tr><td>data</td></tr><tr><td>data2</td></tr>","<td>","</td>") => ["data","data2"]
    """
    arr = re.findall('('+pre+')([\s\S]*?)('+post+')', s)
    data_idx = 1+pre.replace('\\(','').count('(')
    return [a[data_idx] for a in arr];

def nvl(val, default):
    """
        nvl(None, "def") => "def"
        nvl("", "def") => "def"
    """
    return val or default

def camel2under(camel):
    s1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', camel)
    return re.sub('([a-z0-9])([A-Z])', r'\1_\2', s1).lower()

def remove_dot(email):
    id, site = email.split('@')
    return id.replace('.','') + '@' + site

def printable_data(d, depth=10, cur_depth=1, padding=2, encoding='utf8', quot=False, pretty=False):
    """
        make printable string encoded to utf-8(or selected encoding) for any data type
        depth is dict's or list's depth. ex) {'depth 1':{'depth 2':'some value'}, 'depth 1':'some value'}
    """
    lf = '\n'+' '*padding*cur_depth if pretty else '' # linefeed
    lfc = ', '+lf if pretty else ','
    c = ', ' if pretty else ','
    lfdict = '\n'+' '*padding*(cur_depth-1) if pretty else '' # dict close line feed
    listpadding = '' # ' '*int(padding/2) if pretty else '' # list end ] padding
    if depth < 0:
        return d
    elif isinstance(d, dict):
        if len(d.keys()) == 0: 
            return '{ }'
        if len(d.keys()) == 1:
            k, v = d.items()[0]
            if not isinstance(k, (dict,list,tuple,set)) and not isinstance(v, (dict,list,tuple,set)):
                lf = ' '; lfdict = ' ';
        return '{'+lf+lfc.join([printable_data(k, depth-1, cur_depth+1, padding, encoding=encoding, quot=quot, pretty=pretty)+':'+printable_data(v, depth-1, cur_depth+1, padding, encoding=encoding, quot=quot, pretty=pretty) for k,v in d.iteritems()])+lfdict+'}'
    elif isinstance(d, list):
        return '['+lf+lfc.join([printable_data(data, depth-1, cur_depth+1, padding, encoding=encoding, quot=quot, pretty=pretty) for data in d])+lfdict+listpadding+']'
    elif isinstance(d, tuple):
        return '('+c.join([printable_data(data, depth-1, cur_depth+1, padding, encoding=encoding, quot=quot, pretty=pretty) for data in d])+')'
    elif isinstance(d, set):
        return 'set(['+c.join([printable_data(data, depth-1, cur_depth+1, padding, encoding=encoding, quot=quot, pretty=pretty) for data in d])+'])'
    elif isinstance(d, str):
        try:
            s = d
        except UnicodeDecodeError:
            try:
                s = d.decode(encoding)
            except UnicodeDecodeError:
                s = d
        if not quot and cur_depth == 1:
            return s
        return '"'+s+'"'
    else:
        return d.__repr__()




### worker, multi thread 관련

class SimpleWorker(Thread):
    """
        Background worker using threads
        
        reserved keys : f, res, s, tried, error
    """
    def __init__(self, q, done, failed=None, retry=1):
        Thread.__init__(self)
        self.doing = False
        self.q = q
        self.done = done
        self.failed = failed
        self.retry = retry
        self.stopped = False
    
    def run(self):
        self.t_ident = str(thread.get_ident())[-4:]
        while not self.stopped:
            job = self.q.get()
            self.doing = True
            try:
                self.do_work(job)
                job['s'] = True
                self.done.append(job)
            except Exception as e:
                tried = job['tried'] = job.get('tried', 0) + 1
                job['error'] = e
                if callable(job.get('onerror')):
                    job.get('onerror')(job)
                if tried >= self.retry:  # max retry
                    job['s'] = False
                    if self.failed:
                        self.failed.append(job)
                    else:
                        self.done.append(job)
                else:
                    self.q.put(job)
            finally:
                self.doing = False
                self.q.task_done()
    
    def do_work(self, job):
        job['res'] = job['f'](**job)
    
    def is_doing(self):
        return self.doing
    
    def stop(self):
        self.stopped = True
        return self.stopped

class WebWorker(SimpleWorker):
    """
        web worker with proxy
        
        required : url
        optional : method, params, data, json, headers, cookies, files, auth, timeout
    """
    def __init__(self, q, done, failed=None, retry=1, proxy=None):
        SimpleWorker.__init__(self, q, done, failed, retry)
        self.proxies = {'http': proxy, 'https':proxy} if proxy else None # '112.216.16.250:3128'
        self.headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36'}
        
    def do_work(self, job):
        job['res'] = requests.request(
                job.get('method') or 'GET',
                job['url'],
                params = job.get('params'),
                data = job.get('data'),
                json = job.get('json'),
                headers = job.get('headers', self.headers),
                cookies = job.get('cookies'),
                files = job.get('files'),
                auth = job.get('auth'),
                timeout = job.get('timeout'),
                proxies=self.proxies
            )
        job['res'] = job['f'](**job)

def prepare_workers(cnt=5, retry=1):
    """
        작업 시작 전 주어진 수 만큼 worker thread를 마련한다.
    """
    q = Queue()
    done = deque()
    failed = deque()
    workers = []
    
    for _ in range(cnt):
        sw = SimpleWorker(q, done, failed, retry=retry)
        sw.setDaemon(True)
        sw.start()
        workers.append(sw)
    
    return q, done, failed, workers

def prepare_webworkers(cnt=5, prx=None, retry=1):
    """
        작업 시작 전 주어진 수 만큼 worker thread를 마련한다.
    """
    q = Queue()
    done = deque()
    failed = deque()
    workers = []
    
    for p in (prx or range(cnt)):
        ww = WebWorker(q, done, failed, retry=retry, proxy=p if prx else None)
        ww.setDaemon(True)
        ww.start()
        workers.append(ww)
    
    return q, done, failed, workers

"""
    # worker 사용 샘플
    
    def job(some_input, **_):
        pass
    q, done, failed, workers = prepare_workers()
    [q.put({'f':job, 'some_input':i}) for i in range(10)]
    q.join()
    
    [wk.stop() for wk in workers]
    
    # web worker 의 경우 proxy 를 사용할 때 아래와 같이 사용
    q,d,f,w = prepare_webworkers(prx=['218.50.2.102:8080', '112.216.16.250:3128', '220.120.87.174:3128'])
"""

def run_workers(f, args, cnt=5, retry=1):
    """
        아래 형태로 f 를 설정해준다.
        def f(arg, **_):
            pass
    """
    q, done, failed, workers = prepare_workers(cnt, retry)
    [q.put({'f': f, 'arg': arg}) for arg in args]
    q.join()
    [wk.stop() for wk in workers]
    return q, done, failed

"""
    # run_workers 사용 샘플
    
    def job(arg, **_):
        return requests.get('http://example.com/{}'.format(arg).text
    q, d, f = run_workers(job, list(range(30)), 10)
    print len(d). len(f)
"""



### proxy 관련


def prepare_proxy(rdb=None):
    REDIS_KEY = 'proxy'

    def get_proxy():
        proxies = []
        for site, func in PROXY_SITE.items():
            if callable(func):
                proxies += func()

        prx = ping_proxy(proxies)
        prx = sorted(filter(lambda a: a[1] is not None and a[1] < 2, prx), key=lambda a: a[1])
        if rdb:
            prx_map = dict([(k, json.loads(v)) for k, v in (rdb.hgetall(REDIS_KEY) or {}).items()])
            for p, ping in prx:
                log = prx_map.get(p) or {'p': p, 'prob': 0, 'fail_cnt': 0, 'ping': [], 'ping_avg': 0, 'last': False}
                if ping == 0:
                    log['fail_cnt'] += 1
                    log['last'] = False
                else:
                    log['last'] = True
                if log['fail_cnt'] > 3:
                    prx_map.pop(p)
                    continue
                log['ping'].append(ping)
                log['ping'] = log['ping'][-20:] # 최종 20개만 쓴다
                real_ping = filter(lambda a: a > 0, log['ping'])
                if real_ping:
                    log['ping_avg'] = round(sum(real_ping) * 1.0 / len(real_ping), 3)
                    log['prob'] = round(len(log['ping']) * 1.0 / len(real_ping), 3)
                else:
                    log['ping_avg'] = log['prob'] = 0
                prx_map[p] = log

            rdb.delete(REDIS_KEY)
            for k, v in prx_map.items():
                if type(v) != dict:
                    print(k, v)
                rdb.hset(REDIS_KEY, k, json.dumps(v))

        return prx

    if rdb:  # 있는 것을 가져온다.
        # p:{p, prob:100, fail_cnt:2, ping:[0,1,0,1], ping_avg:, last:True}
        prx_map = rdb.hgetall(REDIS_KEY)
        # thread로 별도로 작업한다.
        t = Thread(target=get_proxy)
        t.start()

        prx = [json.loads(v) for v in prx_map.values()]
        prx_filtered = sorted(filter(lambda a: a['last'] and a['prob'] >= 0.5 and a['ping_avg'] < 1, prx), key=lambda a: a['ping_avg'])

        return [(p['p'], p['ping_avg']) for p in prx_filtered]

    return get_proxy()


def ping_proxy(proxies):
    def job(p, **_):
        ip, port = p.split(':')
        sec = None
        try:
            r = requests.get('http://ip4.ssh.works/', proxies={'http': p, 'https': p}, timeout=4)
            if r.text.strip() == ip:
                sec = r.elapsed.total_seconds()
        except Exception as e:
            pass
            #print e
        return p, sec

    q, done, failed, workers = prepare_workers(10)
    [q.put({'f': job, 'p': p}) for p in proxies]
    q.join()
    
    [wk.stop() for wk in workers]
    return [j['res'] for j in done]

def xroxy():
    url = 'http://www.xroxy.com/proxy-country-KR.htm'
    url_2 = 'http://www.xroxy.com/proxylist.php?port=&type=&ssl=&country=KR&latency=&reliability=&sort=reliability&desc=true&pnum=1#table'
    trs = str_split(requests.get(url).text, "title='View this Proxy details'", "title='Select proxies of")
    trs += str_split(requests.get(url_2).text, "title='View this Proxy details'", "title='Select proxies of")
    proxies = []

    for tr in trs:
        ip = str_slice(tr, '>', '<!--').replace('\n', '')
        port = str_slice(tr, 'Select proxies with port number.*?>', '<')
        proxies.append(ip + ':' + port)

    return proxies

def sslproxies():
    url = 'https://www.sslproxies.org/'
    html = requests.get(url).text
    trs = str_split(str_slice(html, 'id="proxylisttable"', '<\\/table>'), "<tr><td>", "<\\/td><\\/tr>")
    proxies = []

    for tr in trs:
        tds = tr.split('</td><td>')
        ip = tds[0]
        port = tds[1]
        proxies.append(ip + ':' + port)

    return proxies

PROXY_SITE = {
    'xroxy': xroxy,
#    'sslproxies':sslproxies,
}



### form validation 관련

def isNumber(s):
    try:
        float(s)
        return s == s # float('nan') is False
    except (ValueError, TypeError) as e:
        return False

def isNaN(s):
    return not isNumber(s) or float('inf') == s

def ifNaN(v, alt):
    return alt if isNaN(v) else float(v)

def fNotEmpty(v):
    return '' != v and None != v

def fEmail(v):
    return '' == v or bool(re.match(r'^[\w.]+@(\w+)(\.\w+)+$', v))

def fPhone(v):
    return '' == v or bool(re.match(r'^01([16789][2-9]|[0169][5-9]\d|0[2-4]\d|9[1-4]\d)\d{6}$', re.sub(r'[^\d]','', v)))

def fSafePhone(v):
    return '' == v or bool(re.match(r'^050\d{8,9}$', re.sub(r'[^\d]','', v)))

def fAllPhone(v):
    return fPhone(v) or fSafePhone(v)

def fLandLine(v):
    return '' == v or bool(re.match(r'(02|0[3-9][0-9])[1-9][0-9]{6,7}$', re.sub(r'[^\d]','', v)))

def fTel(v):
    return fAllPhone(v) or fLandLine(v)

def fNum(v):
    return not isNaN(v)

def fPnum(v):
    return not isNaN(v) and float(v) > 0

def fMnum(v):
    return not isNaN(v) and float(v) < 0

def fCommaNum(v):
    return not isNaN(str(v).replace(',',''))

def fCommaPnum(v):
    return not isNaN(str(v).replace(',','')) and float(v.replace(',','')) > 0

def fCommaMnum(v):
    return not isNaN(str(v).replace(',','')) and float(v.replace(',','')) < 0

def fJson(v):
    try:
        json.loads(v)
        return True
    except:
        return False

def fSame(v, r):
    return v == r

def fDiff(v, r):
    return v != r

def fLt(v, r):
    return v < r

def fLte(v, r):
    return v <= r

def fGt(v, r):
    return v > r

def fGte(v, r):
    return v >= r

def fNumLt(v, r):
    return float(str(v).replace(',','')) < r

def fNumLte(v, r):
    return float(str(v).replace(',','')) <= r

def fNumGt(v, r):
    return float(str(v).replace(',','')) > r

def fNumGte(v, r):
    return float(str(v).replace(',','')) >= r

def fLenLte(v, r):
    if type(v) == str:
        v = v.decode('utf8')
    return len(v) <= r

def fLenGte(v, r):
    if type(v) == str:
        v = v.decode('utf8')
    return len(v) >= r

def fLenEqual(v, r):
    if type(v) == str:
        v = v.decode('utf8')
    return len(v) == r

def fLenBLte(v, r):
    if type(v) == unicode:
        v = v.encode('utf8')
    return len(v) <= r

def fLenBGte(v, r):
    if type(v) == unicode:
        v = v.encode('utf8')
    return len(v) >= r

def fLenBEqual(v, r):
    if type(v) == unicode:
        v = v.encode('utf8')
    return len(v) == r

def fJumin(s):
    jumin = s.replace("-","")
    if len(jumin) != 13 or not re.match('^\\d+$', jumin):
        return False
    
    weight = '234567892345' # 자리수 weight 지정
    total = 0
    
    for i in range(12):
        total += int(jumin[i]) * int(weight[i]);
    
    return ((11 - total%11)%10 == int(jumin[12]) or fOtherJumin(jumin))

def fOtherJumin(s): # 외국인인 경우
    jumin = s.replace("-","")
    if len(jumin) != 13 or not re.match('^\\d+$', jumin):
        return False
    
    if int(jumin[8]) % 2 != 0 or int(jumin[11]) < 6:
        return False
    
    weight = '234567892345' # 자리수 weight 지정
    total = reduce(lambda a,b:a+b, map(lambda t:int(t[1])*int(jumin[t[0]]), enumerate(weight)))
    total = 11 - (total%11)
    total = ((total % 10) + 2) % 10
    
    if total != int(jumin[12]):
        return False
    
    return True

def fSafePW(pw):
    num = 1 if re.search(r'[0-9]', pw) else 0
    eng = 1 if re.search(r'[a-zA-Z]', pw) else 0
    spe = 1 if re.search(r'[`~!@@#$%^&*|\\\'\";:\/?,.<>\-_+=]', pw) else 0
    mixCnt = num + eng + spe
    
    if len(pw) < 8:
        return False
    if len(pw) < 10 and mixCnt < 3 or len(pw) >= 10 and mixCnt < 2:
        return False
    return True


validation_functions = Params(dict(NotEmpty=fNotEmpty, Email=fEmail, Phone=fPhone, SafePhone=fSafePhone, AllPhone=fAllPhone, LandLine=fLandLine, Tel=fTel, Num=fNum, Pnum=fPnum, Mnum=fMnum, CommaNum=fCommaNum, CommaPnum=fCommaPnum, CommaMnum=fCommaMnum, Json=fJson, Jumin=fJumin, SafePW=fSafePW, Same=fSame, Diff=fDiff, Lt=fLt, Lte=fLte, Gt=fGt, Gte=fGte, NumLt=fNumLt, NumLte=fNumLte, NumGt=fNumGt, NumGte=fNumGte, LenLte=fLenLte, LenGte=fLenGte, LenEqual=fLenEqual, LenBLte=fLenBLte, LenBGte=fLenBGte, LenBEqual=fLenBEqual))

def validate_params(params, rules):
    """
        js의 validation과 호환을 위해 만들어진 함수
        
        valid, msg = validate_params(p, {
          'address':{'f':{'NotEmpty':''}, 'invalid':'bd-faded-red', 'msg':'자택 주소를 입력해주세요'},
          'acc_no':{'f':{'NotEmpty':''}, 'invalid':'bd-faded-red', 'msg':'계좌번호를 입력해주세요'},
        })
    """
    vf = validation_functions
    for k, r in rules.items():
        if k[0] == '#' or k[0] == '.': # #id, .class 형태 보정
            k = k[1:]
        v = params[k]
        for f, m in r['f'].items(): # f = NotEmpty, m = "Message" or [compare, "Message"]
            if isinstance(m, list):
                valid = vf[f](*[v, m[0]])
                msg = m[1]
            else:
                valid = vf[f](v)
                msg = m
            if not valid:
                return valid, msg or r['msg']
    return True, ''




### 출력 및 로깅 관련

SYSTEM_ENCODING = (sys.stdout.encoding or 'cp949') if sys.platform == 'win32' else 'utf8'

def hanprint(*args):
    """
        printable_data를 이용하여 한글도 잘 나오도록 프린트한다.
    """
    print(' '.join([printable_data(d, encoding=SYSTEM_ENCODING, pretty=True) for d in args]))
ph = hanprint # shortcut

def fprint(*args):
    """
        파일명 및 호출위치와 함께 hanprint를 진행한다. 디버그용 임시 프린트에 적합
    """
    before_frame = sys._getframe().f_back
    f_code = before_frame.f_code
    name = f_code.co_name
    data = Params({
        "lineno":before_frame.f_lineno,
        "file":f_code.co_filename,
    })
    sys.stdout.write(data.file.split('/')[-1]+':'+str(data.lineno)+' ')
    hanprint(*args)


def tfprint(*args):
    """ 시간, 파일명과 함께 출력하기 """
    print(tflog(*args))

def tflog(*args):
    """ 시간, 파일명과 함께 Log 형태를 반환 """
    before_frame = sys._getframe().f_back
    f_code = before_frame.f_code
    name = f_code.co_name
    data = Params({
        "lineno":before_frame.f_lineno,
        "file":f_code.co_filename,
    })
    now = pytz.utc.localize(datetime.utcnow()).astimezone(pytz.timezone('Asia/Tokyo'))
    return (str(now)[5:19] + ' ' + data.file.split('/')[-1]+':'+str(data.lineno)+' ' + ' '.join(map(lambda s: printable_data(s, encoding=SYSTEM_ENCODING), args)))



debug_logger = logging.getLogger('debug')
info_logger = logging.getLogger('info')
error_logger = logging.getLogger('error')

debug_logger.setLevel(logging.DEBUG)
info_logger.setLevel(logging.INFO)
error_logger.setLevel(logging.WARNING)

#debug_logger.addHandler(logging.StreamHandler())
info_logger.addHandler(logging.StreamHandler())
error_logger.addHandler(logging.StreamHandler())


log_levels = {'C':logging.CRITICAL, 'E':logging.ERROR, 'W':logging.WARNING, 'I':logging.INFO, 'D':logging.DEBUG}
loggers = {'C':error_logger, 'E':error_logger, 'W':error_logger, 'I':info_logger, 'D':debug_logger}


def init_log_file(dir_name='/tmp/', maxBytes=10*1024*1024, backupCount=10, owner=None):
    """
        각 로거에 파일로깅을 지정위치에 일괄 추가한다.
    """
    owner = owner or ('www', 'www')
    dfh = owned_rfhandler(dir_name+'debug.log', maxBytes=maxBytes, backupCount=backupCount, owner=owner)
    #dfh.setFormatter(logging.Formatter('[%(levelname)s|%(filename)s:%(lineno)s] %(asctime)s > %(message)s'))
    if len(debug_logger.handlers) < 1:
        debug_logger.addHandler(dfh)
    if len(info_logger.handlers) == 1:
        info_logger.addHandler(owned_rfhandler(dir_name+'info.log', maxBytes=maxBytes, backupCount=backupCount, owner=owner))
    if len(error_logger.handlers) == 1:
        error_logger.addHandler(owned_rfhandler(dir_name+'error.log', maxBytes=maxBytes, backupCount=backupCount, owner=owner))

def owned_rfhandler(filename, mode='a', maxBytes=0, backupCount=0, encoding=None, delay=0, owner=None):
    if owner:
        import os, pwd, grp
        # convert user and group names to uid and gid
        uid = pwd.getpwnam(owner[0]).pw_uid
        gid = grp.getgrnam(owner[1]).gr_gid
        owner = (uid, gid)
        if not os.path.exists(filename):
            open(filename, 'a').close()
        if os.getuid() == 0: # root일 경우
            os.chown(filename, *owner)
            os.chmod(filename, '0664')
    return logging.handlers.RotatingFileHandler(filename, mode, maxBytes=maxBytes, backupCount=backupCount, encoding=encoding, delay=0)


def log(level='I', *args):
    """
        기본 로깅
        
        level을 지정하지 않으면 info로 처리한다.
        log('D', 'debug message')
        log('info')
    """
    before_frame = sys._getframe().f_back
    if before_frame.f_code.co_name in 'dlog,ilog,wlog,elog,clog':
        before_frame = before_frame.f_back
    f_code = before_frame.f_code
    name = f_code.co_name
    
    data = Params({
        "lineno":before_frame.f_lineno,
        "file":f_code.co_filename,
    })
    now = pytz.utc.localize(datetime.utcnow()).astimezone(pytz.timezone('Asia/Tokyo'))
    
    # level이 없이 바로 메시지가 온 경우
    if level not in list('CEWIDN') or len(args) == 0:
        args = (level,) + args
        level = 'I'

    loggers[level].log(log_levels[level], (str(now)[5:19] + ' ' + data.file.split('/')[-1]+':'+name+':'+str(data.lineno)+' ' + ' '.join(map(lambda s: printable_data(s, encoding=SYSTEM_ENCODING), args))))

def dlog(*args):
    return log('D', *args)

def ilog(*args):
    return log('I', *args)

def wlog(*args):
    return log('W', *args)

def elog(*args):
    return log('E', *args)

def clog(*args):
    return log('C', *args)



### 숫자 관련

def between(v, min=None, max=None):
    """
        v가 min과 max 사이에 놓이도록 재조정한다
    """
    if min != None and v < min:
        return min
    if max != None and v > max:
        return max
    return v

def sign(v):
    return 0 if v == 0 else 1 if v > 0 else -1


### geoip 관련

def ip2country(ip):
    if 'geolite2' in globals(): # pc와 구분하기 위해
        geoip = geolite2.lookup(ip)
        if geoip:
            return geoip.country
    else:
        rec = gi.record_by_addr(ip)
        if rec:
            return rec['country_code']
    
    return 'ETC'





### datetime 관련

# http://haruair.com/blog/1759
# datetime.replace(tzinfo=seoul) 등을 사용하면 'Asia/Seoul' LMT+8:28:00 STD 등으로 변하는 문제가 생긴다
# datetime.astimezone, tzinfo.localize(datetime) 만 써야 한다
seoul = pytz.timezone('Asia/Seoul') # 'Asia/Seoul' KST+8:30:00 STD , 'Asia/Seoul' KST+9:00:00 STD 를 오락가락한다
tokyo = pytz.timezone('Asia/Tokyo') # 한국 시각은 도쿄 표준시로 쓰도록 한다.

def utc2kr(dt=None):
    """
        utc 를 받아서 한국시각을 반환한다.
    """
    if not dt:
        dt = datetime.utcnow()
    t = dt.replace(tzinfo=pytz.utc).astimezone(seoul)
    return t

def kr2utc(dt=None):
    """
        한국시각을 받아서 utc를 반환한다.
    """
    if not dt:
        return datetime.utcnow().replace(tzinfo=pytz.utc)
    t = seoul.localize(dt.replace(tzinfo=None)).astimezone(pytz.utc)
    return t

def kst(dt=None):
    if not dt:
        dt = utc('kr')
    if not dt.tzinfo:
        dt = seoul.localize(dt)
    return utc(dt, 'kr')

def utc(dt=None, tzobj=None, settz=None, msec=None):
    """
        시간변환 함수
        utc() # 기본적으로 현재의 utc 시각을 timezone과 함께 반환한다.
        utc(datetime.now().replace(tzinfo=tokyo)) # dt가 들어온 경우 해당 시각을 utc로 바꾸며
        utc(datetime.utcnow(), 'cn') # dt와 tzobj가 들어온 경우 해당시각을 해당 timezone으로 바꾼다
        utc(datetime.now(), settz='kr') # tz만 덧씌우고 싶다면 settz를 사용한다
        
        utc('kr') # tzobj만 들어온 경우 현재시각을 해당 timezone으로 바꾼다
        utc(1466321405111) # ms 단위로 들어온 경우 utc시각으로 변환한다
        utc(1466321405) # sec 단위로 들어온 경우 utc시각으로 변환한다
        utc('2016.06.01') # str로 들어온 경우 파싱하여 utc시각으로 변환한다
        utc('20180101123000', settz='kr') # 중립적 데이터에 tz만 덧씌우고 싶다면 settz를 사용한다
    """
    if not (isinstance(dt, (str, int, float, datetime, date)) or dt == None) and tzobj == None: # tzobj만 입력한 경우
        tzobj = dt
        dt = None
    elif isinstance(dt, (str)) and tzobj == None:
        # datetime으로 파싱이 된다면 datetime, 아니면 tz로 판단한다
        try:
            dt = str2date(dt)
        except:
            tzobj = dt
            dt = None
        
    if not dt:
        dt = datetime.utcnow()
    elif type(dt) == date: # datetime화 시킨다
        dt = datetime.fromordinal(dt.toordinal())
    elif isinstance(dt, (int, float)): # datetime화 시킨다
        ms = dt % 1
        if not msec and (dt > 5000000000 or dt < -5000000000): # ms 기준 1811년 ~ 2128년 이외, ,3000년 이상, sec 기준 1969.11.4~1970.2.27
            ms = dt % 1000
            dt = dt // 1000
        dt = datetime.utcfromtimestamp(dt)
        dt = dt.replace(microsecond=int(ms*1000))
    elif isinstance(dt, (str)):
        dt = str2date(dt)
    
    if settz:
        if dt.tzinfo:
            dt = dt.replace(tzinfo=None)
        dt = get_tz(settz).localize(dt)
        return dt
    
    if not dt.tzinfo:
        dt = dt.replace(tzinfo=pytz.utc)
    if not tzobj:
        tzobj = pytz.utc
    else:
        tzobj = get_tz(tzobj)
    return dt.astimezone(tzobj)

def day_only(dt=None, tzobj=None):
    """
        timezone을 기준으로 해당 시각의 그날 자정 시각을 반환한다.
        tzobj가 지정된 경우 해당 timezone으로 변환 후 해당일을 반환한다
        date로 반환하지 않는 이유는 mongodb나 json_util에서 문제가 발생하기 때문.
        dayonly
    """
    if isinstance(dt, datetime) and dt.tzinfo and tzobj == None:
        tzobj = dt.tzinfo
    dt = utc(dt, tzobj)
    t = datetime(dt.year, dt.month, dt.day, tzinfo=dt.tzinfo)
    return t

def month_only(dt=None, tzobj=None):
    """
        timezone을 기준으로 해당 시각의 그달의 첫 날 자정 시각을 반환한다.
    """
    dt = day_only(dt, tzobj)
    t = datetime(dt.year, dt.month, 1, tzinfo=dt.tzinfo)
    return t

def year_only(dt=None, tzobj=None):
    """
        timezone을 기준으로 해당 시각의 그달의 1월 1일 자정 시각을 반환한다.
    """
    dt = day_only(dt, tzobj)
    t = datetime(dt.year, 1, 1, tzinfo=dt.tzinfo)
    return t

def str2date(str='1970-01-01'):
    """
        날짜나 시각 등의 표현 스트링을 받아서 datetime 객체를 반환한다.
    """
    t = dateutil.parser.parse(str)
    return t

def to_timestamp(dt):
    """
        utc 기준 unix timestamp 를 반환한다. utc가 아니라면 utc로 변경한 뒤 진행한다.
    """
    return int((utc(dt) - datetime(1970, 1, 1, tzinfo=pytz.utc)).total_seconds())


tzdict = { # pytz.country_timezones['KR'] 로 대체한다
    'KR':'Asia/Seoul',
    'JP':'Asia/Tokyo',
    'US':'America/New_York',
    'HK':'Asia/Hong_Kong',
    'CN':'Asia/Shanghai',
}
def get_tz(tzobj):
    """
        기존 timezone, datetime, locale str, timezone str 등을 받아서 pytz.timezone을 반환한다
    """
    if isinstance(tzobj, tzinfo):
        return tzobj
    if type(tzobj) == datetime:
        if tzobj.tzinfo:
            return pytz.timezone(tzobj.tzinfo.name)
        else:
            return pytz.utc
    if type(tzobj) == str or type(tzobj) == unicode:
        if len(tzobj) == 2 or len(tzobj) == 4 or len(tzobj) == 5: # KR, JP, CN, US, koKR, ja-JP, en_US 등
            tzobj = pytz.country_timezones.get(tzobj[-2:].upper(), [tzobj])[0]
        try:
            tz = pytz.timezone(tzobj)
        except pytz.UnknownTimeZoneError as e:
            tz = pytz.utc
        return tz
    return pytz.utc



#@deprecated

def day_only_kr(time=None):
    """
        한국시각 기준으로 해당 시각의 그날 자정 시각을 반환한다.
        @deprecated
    """
    if not time: time = utc2kr()
    t = datetime(time.year,time.month,time.day,0,0,0,tzinfo=tokyo)
    return t

def month_only_kr(time=None):
    """
        한국시각 기준으로 해당 시각의 그달의 첫 날 자정 시각을 반환한다.
        @deprecated
    """
    if not time: time = utc2kr()
    t = datetime(time.year,time.month,1,0,0,0,tzinfo=tokyo)
    return t


# command 실행
def cmd(s, shell=True):
    output, error = Popen(s, shell=shell, stdout=PIPE, stderr=PIPE).communicate()
    return output or error
    # return commands.getstatusoutput(s)[1] # deprecated



def _json_convert(obj):
    """Recursive helper method that converts BSON types so they can be
    converted into json.
    """
    if hasattr(obj, 'iteritems') or hasattr(obj, 'items'):  # PY3 support
        return SON(((k, _json_convert(v)) for k, v in obj.iteritems()))
    elif hasattr(obj, '__iter__') and not isinstance(obj, (text_type, bytes)):
        return list((_json_convert(v) for v in obj))
    try:
        return json_default(obj)
    except TypeError:
        return obj

def json_default(obj):
    # We preserve key order when rendering SON, DBRef, etc. as JSON by
    # returning a SON for those types instead of a dict.
    if isinstance(obj, ObjectId):
        return {"$oid": str(obj)}
    if isinstance(obj, DBRef):
        return _json_convert(obj.as_doc())
    if isinstance(obj, datetime):
        # TODO share this code w/ bson.py?
        if obj.utcoffset() is not None:
            obj = obj - obj.utcoffset()
        millis = int(calendar.timegm(obj.timetuple()) * 1000 +
                     obj.microsecond / 1000)
        return {"$date": millis}
    if isinstance(obj, date):
        # TODO share this code w/ bson.py?
        millis = int(calendar.timegm(obj.timetuple()) * 1000)
        return {"$date": millis}
    if isinstance(obj, (RE_TYPE, Regex)):
        flags = ""
        if obj.flags & re.IGNORECASE:
            flags += "i"
        if obj.flags & re.LOCALE:
            flags += "l"
        if obj.flags & re.MULTILINE:
            flags += "m"
        if obj.flags & re.DOTALL:
            flags += "s"
        if obj.flags & re.UNICODE:
            flags += "u"
        if obj.flags & re.VERBOSE:
            flags += "x"
        if isinstance(obj.pattern, text_type):
            pattern = obj.pattern
        else:
            pattern = obj.pattern.decode('utf-8')
        return SON([("$regex", pattern), ("$options", flags)])
    if isinstance(obj, MinKey):
        return {"$minKey": 1}
    if isinstance(obj, MaxKey):
        return {"$maxKey": 1}
    if isinstance(obj, Timestamp):
        return {"$timestamp": SON([("t", obj.time), ("i", obj.inc)])}
    if isinstance(obj, Code):
        return SON([('$code', str(obj)), ('$scope', obj.scope)])
    if isinstance(obj, Binary):
        return SON([
            ('$binary', base64.b64encode(obj).decode()),
            ('$type', "%02x" % obj.subtype)])
    if isinstance(obj, uuid.UUID):
        return {"$uuid": obj.hex}
    if isinstance(obj, Decimal):
        return float(obj)
    raise TypeError("%r is not JSON serializable" % obj)

def encode_json(obj):
    """ 객체들을 json으로 호환 가능한 형태로 바꾼다 """
    if type(obj) is Params:
        return Params(map(lambda k,v:(k, encode_json(v)), obj.iteritems() ))
    if isinstance(obj, dict):
        return dict(map(lambda k,v:(k, encode_json(v)), obj.iteritems() ))
    if isinstance(obj, list):
        return [encode_json(data) for data in obj]
    if isinstance(obj, tuple):
        return tuple([encode_json(data) for data in obj])
    if isinstance(obj, set): 
        return set([encode_json(data) for data in obj])
    if isinstance(obj, Decimal):
        return float(obj)
    if isinstance(obj, ObjectId):
        return {"$oid": str(obj)}
    if isinstance(obj, DBRef):
        return _json_convert(obj.as_doc())
    if isinstance(obj, datetime):
        # TODO share this code w/ bson.py?
        if obj.utcoffset() is not None:
            obj = obj - obj.utcoffset()
        millis = int(calendar.timegm(obj.timetuple()) * 1000 + obj.microsecond / 1000)
        return {"$date": millis}
    if isinstance(obj, date):
        # TODO share this code w/ bson.py?
        millis = int(calendar.timegm(obj.timetuple()) * 1000)
        return {"$date": millis}
    if isinstance(obj, (RE_TYPE, Regex)):
        flags = ""
        if obj.flags & re.IGNORECASE:
            flags += "i"
        if obj.flags & re.LOCALE:
            flags += "l"
        if obj.flags & re.MULTILINE:
            flags += "m"
        if obj.flags & re.DOTALL:
            flags += "s"
        if obj.flags & re.UNICODE:
            flags += "u"
        if obj.flags & re.VERBOSE:
            flags += "x"
        if isinstance(obj.pattern, text_type):
            pattern = obj.pattern
        else:
            pattern = obj.pattern.decode('utf-8')
        return SON([("$regex", pattern), ("$options", flags)])
    if isinstance(obj, MinKey):
        return {"$minKey": 1}
    if isinstance(obj, MaxKey):
        return {"$maxKey": 1}
    if isinstance(obj, Timestamp):
        return {"$timestamp": SON([("t", obj.time), ("i", obj.inc)])}
    if isinstance(obj, Code):
        return SON([('$code', str(obj)), ('$scope', obj.scope)])
    if isinstance(obj, Binary):
        return SON([
            ('$binary', base64.b64encode(obj).decode()),
            ('$type', "%02x" % obj.subtype)])
    if isinstance(obj, uuid.UUID):
        return {"$uuid": obj.hex}
    if isinstance(obj, float):
        if obj != obj: # nan
            #return None
            return {'$float':'NaN'}
        if obj == float('inf'): # infinity
            return {'$float':'Infinity'}
    return obj

def decode_json(obj):
    """ json 호환 형태로 표현된 객체들을 원래 형태로 바꾼다. 미완성 """
    if isinstance(obj, dict):
        if isinstance(obj.get('$date'), (int, long)):
            return utc(obj['$date'])
        elif isinstance(obj.get('$oid'), basestring):
            return ObjectId(obj['$oid'])
        elif isinstance(obj.get('$uuid'), basestring):
            return uuid.UUID(obj['$uuid'])
        elif '$regex' in obj and '$options' in obj:
            return ''
        elif '$code' in obj and '$scope' in obj:
            return ''
        elif '$binary' in obj and '$type' in obj:
            return ''
        elif '$timestamp' in obj:
            return ''
        elif '$minKey' in obj:
            return MinKey()
        elif '$maxKey' in obj:
            return MaxKey()
        if type(obj) is Params:
            return Params(map(lambda k,v:(k, decode_json(v)), obj.iteritems() ))
        return dict(map(lambda k,v:(k, decode_json(v)), obj.iteritems() ))
    if isinstance(obj, list):
        return [decode_json(data) for data in obj]
    if isinstance(obj, tuple):
        return tuple([decode_json(data) for data in obj])
    if isinstance(obj, set):
        return set([decode_json(data) for data in obj])
    return obj

def encode_mongo(obj):
    """ 객체들을 mongodb와 호환 가능한 형태로 바꾼다. key값에 $ 가 있으면 안된다. """
    if type(obj) is Params:
        return Params(map(lambda k,v:(k, encode_mongo(v)), obj.iteritems() ))
    if isinstance(obj, dict):
        return dict(map(lambda k,v:(k, encode_mongo(v)), obj.iteritems() ))
    if isinstance(obj, list):
        return [encode_mongo(data) for data in obj]
    if isinstance(obj, tuple):
        return tuple([encode_mongo(data) for data in obj])
    if isinstance(obj, set):
        return set([encode_mongo(data) for data in obj])
    if isinstance(obj, Decimal):
        return float(obj)
    if isinstance(obj, DBRef):
        return _json_convert(obj.as_doc())
    if isinstance(obj, date):
        # TODO share this code w/ bson.py?
        return datetime.fromordinal(obj.toordinal())
    return obj

def to_json(obj):
    """ 객체를 json string으로 변환한다. bson.json_util.default 스타일로 변환 """
    return ujson.dumps(encode_json(obj), ensure_ascii=False)

def from_json(s):
    """ json 에서 객체를 가져와서 원래 형식으로 바꿔준다 """
    return decode_json(ujson.loads(s))



### 암호화 관련

class AES256():
    """
        AES256 encryption module
        
        aes = AES256()
        print 'plain', aes.enc('plain'), aes.dec(aes.enc('plain'))
        print 'plain and key', aes.enc('plain and key', 'key'), aes.dec(aes.enc('plain and key', 'key'), 'key')
    """
    def __init__(self, key=None, iv=None):
        self.key = key or self._gen_key()
        self.iv = iv or chr(0) * 16

    def _gen_key(self):
        chars = string.lowercase + string.uppercase + string.digits
        return ''.join([chars[random.randint(0, len(chars)-1)] for i in range(16)])

    def _adjust_key(self, key):
        return hashlib.sha1(key).hexdigest()[:16]

    def pad(self, s):
        return s + (16 - len(s) % 16) * chr(16 - len(s) % 16)

    def unpad(self, s):
        return s[:-ord(s[-1])]

    def enc(self, plain_text, key=None):
        if plain_text == None: return None
        engine = AES.new(self._adjust_key(key or self.key), AES.MODE_CBC, self.iv)
        return base64.b64encode(engine.encrypt(self.pad(plain_text.encode('utf8') if type(plain_text) is unicode else str(plain_text))))

    def dec(self, encrypted_text, key=None):
        if encrypted_text == None: return None
        engine = AES.new(self._adjust_key(key or self.key), AES.MODE_CBC, self.iv)
        try:
            plain_text = self.unpad(engine.decrypt(base64.b64decode(encrypted_text)))
            return plain_text
        except:
            return encrypted_text




### hash 관련

def md5_(obj, iter=1, n=None):
    """ md5 hash값을 반환, 민감정보에는 사용하지 않도록 한다. """
    obj = str(obj)
    for i in range(iter):
        obj = hashlib.md5(obj).digest()
    return obj[0:n or 128]

def md5h(obj, iter=1, n=None):
    """ md5 hash값을 hex string으로 반환, 민감정보에는 사용하지 않도록 한다. """
    obj = str(obj)
    for i in range(iter):
        obj = hashlib.md5(obj).hexdigest()
    return obj[0:n or 128]

def md5b(obj, iter=1, n=None):
    """ md5 hash값을 base64로 반환, 민감정보에는 사용하지 않도록 한다. """
    obj = str(obj)
    for i in range(iter):
        obj = base64.b64encode(hashlib.md5(obj).digest())
    return obj[0:n or 128]

def sha2(obj, iter=1, n=None):
    """ sha256을 사용한 hash값을 반환 """
    obj = str(obj)
    for i in range(iter):
        obj = hashlib.sha256(obj).digest()
    return obj[0:n or 128]

def sha2h(obj, iter=1, n=None):
    """ sha256을 사용한 hash값을 hex string으로 반환 """
    obj = str(obj)
    for i in range(iter):
        obj = hashlib.sha256(obj).hexdigest()
    return obj[0:n or 128]

def sha2b(obj, iter=1, n=None):
    """ sha256을 사용한 hash값을 base64로 반환 """
    obj = str(obj)
    for i in range(iter):
        obj = base64.b64encode(hashlib.sha256(obj).digest())
    return obj[0:n or 128]

def sha3(obj, iter=1, n=None):
    """ sha384를 사용한 hash값을 반환 """
    obj = str(obj)
    for i in range(iter):
        obj = hashlib.sha384(obj).digest()
    return obj[0:n or 128]

def sha3h(obj, iter=1, n=None):
    """ sha384를 사용한 hash값을 hex string으로 반환 """
    obj = str(obj)
    for i in range(iter):
        obj = hashlib.sha384(obj).hexdigest()
    return obj[0:n or 128]

def sha3b(obj, iter=1, n=None):
    """ sha384를 사용한 hash값을 base64로 반환 """
    obj = str(obj)
    for i in range(iter):
        obj = base64.b64encode(hashlib.sha384(obj).digest())
    return obj[0:n or 128]

def sha5(obj, iter=1, n=None):
    """ sha512를 사용한 hash값을 반환 """
    obj = str(obj)
    for i in range(iter):
        obj = hashlib.sha512(obj).digest()
    return obj[0:n or 128]

def sha5h(obj, iter=1, n=None):
    """ sha512를 사용한 hash값을 hex string으로 반환 """
    obj = str(obj)
    for i in range(iter):
        obj = hashlib.sha512(obj).hexdigest()
    return obj[0:n or 128]

def sha5b(obj, iter=1, n=None):
    """ sha512를 사용한 hash값을 base64로 반환 """
    obj = str(obj)
    for i in range(iter):
        obj = base64.b64encode(hashlib.sha512(obj).digest())
    return obj[0:n or 128]

def hashtest(obj, iter=1):
    """ hash 함수들 test """
    print('md5 :', len(md5_(obj, iter)), 'byte,', md5_(obj, iter))
    print('md5h :', len(md5h(obj, iter)), 'byte,', md5h(obj, iter))
    print('md5b :', len(md5b(obj, iter)), 'byte,', md5b(obj, iter))
    print('sha2 :', len(sha2(obj, iter)), 'byte,', sha2(obj, iter))
    print('sha2h :', len(sha2h(obj, iter)), 'byte,', sha2h(obj, iter))
    print('sha2b :', len(sha2b(obj, iter)), 'byte,', sha2b(obj, iter))
    print('sha3 :', len(sha3(obj, iter)), 'byte,', sha3(obj, iter))
    print('sha3h :', len(sha3h(obj, iter)), 'byte,', sha3h(obj, iter))
    print('sha3b :', len(sha3b(obj, iter)), 'byte,', sha3b(obj, iter))
    print('sha5 :', len(sha5(obj, iter)), 'byte,', sha5(obj, iter))
    print('sha5h :', len(sha5h(obj, iter)), 'byte,', sha5h(obj, iter))
    print('sha5b :', len(sha5b(obj, iter)), 'byte,', sha5b(obj, iter))



### 파일 관련

def file2md5(filename, n=None):
    """
        파일의 내용을 읽어서 n자리의 hash 만들기
        md5는 안전하지 않으나 파일의 동일성 확인을 위한 hash 정도에는 문제가 없다.
        n은 최대 32 까지 가능
    """
    md5 = hashlib.md5()
    if type(filename) == str:
        with open(filename, 'rb') as f:
            for chunk in iter(lambda: f.read(8192), ''):
                md5.update(chunk)
            f.close()
    else:
        for chunk in iter(lambda: filename.read(8192), ''):
            md5.update(chunk)
    return md5.hexdigest()[0:n or 128]

def file2sha2(filename, n=None):
    """
        파일의 내용을 읽어서 n자리의 sha256 hash 만들기
        n은 최대 64 까지 가능
    """
    h = hashlib.sha256()
    if type(filename) == str:
        with open(filename, 'rb') as f:
            for chunk in iter(lambda: f.read(8192), ''):
                h.update(chunk)
            f.close()
    else:
        for chunk in iter(lambda: filename.read(8192), ''):
            h.update(chunk)
    return h.hexdigest()[0:n or 128]

class MemoryFile():
    """
        임시적인 파일을 디스크에 생성하고 싶지 않을 때 사용한다.
        @deprecated StringIO, cStringIO 를 사용하면 된다. sio = StringIO(); sio.write('aaa')
    """
    def __init__(self):
        self.str = ''
        self.len = 0
    def write(self, s):
        self.str += str(s)
        self.len += len(s)
    def tell(self):
        return self.len
    def flush(self):
        pass
    def read(self):
        return self.str



### 전화번호 관련

def extract_itut(phone):
    """
        8210.. 등의 번호에서 ITUT 코드인 82 를 추출해낸다.
    """
    try:
        p = phonenumbers.parse('+'+phone)
    except Exception as e:
        return None
    return p.country_code

def std2local(phone):
    """
        821X 로 시작하고 11~12자인 국내 휴대폰 형식의 번호를 01X 형태로 바꾼다.
        822x 로 시작하고 10~11자인 서울 번호도 우선은 바꾼다.
    """
    if phone and (len(phone) == 11 or len(phone) == 12) and phone[:3] == '821':
        return '01'+phone[3:]
    if phone and (len(phone) == 10 or len(phone) == 11) and phone[:3] == '822':
        return '02'+phone[3:]
    return phone

def local2std(phone):
    """
        01X 로 시작하고 10~11자인 국내 휴대폰 형식의 번호를 821X 형태로 바꾼다.
        02x 로 시작하고 9~10자인 서울 번호를 822X 형태로 우선은 바꾼다.
    """
    if phone and (len(phone) == 10 or len(phone) == 11) and phone[:2] == '01':
        return '821'+phone[2:]
    if phone and (len(phone) == 9 or len(phone) == 10) and phone[:2] == '02':
        return '822'+phone[2:]
    return phone


### Error Handler 패턴 관련

def get_error_handler(collection, err=lambda:'error'):
    """
        collection - exception을 기록할 mongodb의 collection
        @app.errorhandler(Exception) 등의 패턴에 대응하는 Error Logger 이다.
        _common.py 등에서 app.errorhandler(Exception)(get_error_handler(db.client.error.voice, error)) 로 바로 적용 가능하다.
    """
    # 필요시 이하의 함수를 _common.py 등으로 옮기면 바로 사용할 수 있다.
    def error_handler(e, code=None):
        """
            에러 발생시 해당 위치를 mongodb의 error db에 기록.
        """
        time_str = utc('kr').strftime('%Y.%m.%d %H:%M:%S')
        if g.get('started'):
            elapsed = (datetime.utcnow() - g.started).total_seconds()
        else:
            elapsed = None
            
        exc_type, exc_obj, exc_tb = sys.exc_info()
        tb = exc_tb
        tblist = []
        skip_ends_with = ['__database.py', '_common.py', '_return.py', '_filter.py']
        while tb.tb_next:
            tblist.append(tb)
            tb = tb.tb_next
        while tb and (tb.tb_frame.f_code.co_filename.startswith('/usr/') or tb.tb_frame.f_code.co_filename.rsplit('/', 1)[-1] in skip_ends_with):
            # /usr/ 하에 위치하는 library 소스를 제외하도록 다시 거슬러 올라간다
            tb = tblist.pop() if tblist else None
        if not tb:
            tb = exc_tb
        func_name = tb.tb_frame.f_code.co_name
        
        # source 가져오기
        try:
            source = inspect.getsource(tb.tb_frame.f_code)
        except:
            source = None
        
        # error database에 저장
        error_data = Params({
            "method":request.method,
            "path":request.path,
            "url":request.url,
            "args":copy_dict(request.args),
            "form":copy_dict(request.form),
            "data":request.data,
            "json":request.json,
            "headers":dict(request.headers.to_list()),
            "session":encode_mongo(dict(session)),
            "unity_id":session.get('unity_id'),
            "user_id":session.get('user_id'),
            "func":func_name,
            "lineno":tb.tb_frame.f_lineno,
            "file":tb.tb_frame.f_code.co_filename,
            "func_source":source,
            "func_lineno":tb.tb_frame.f_code.co_firstlineno,
            "except":e.__class__.__name__,
            "except_message":e.message,
            "except_args":e.args,
            "except_repr":e.__repr__(),
            "stack":traceback.format_tb(exc_tb),
            "remote_addr":request.remote_addr,
            "time":time_str,
            "elapsed":elapsed,
        })
        collection and collection.insert(error_data)
        
        traceback.print_tb(exc_tb)
        print('#####', e.__repr__())
        
        return err(code, error_data=error_data) if code else err(error_data=error_data)
    return error_handler

def get_error_logger(collection):
    """
        collection - error response를 기록할 mongodb의 collection
        elog = get_error_logger(db.client.error.voice_error) 등으로 가져와서 _common.py 의 error 안에서 elog(code) 등으로 사용 가능하다.
    """
    # 필요시 이하의 함수를 _common.py 등으로 옮기면 바로 사용할 수 있다.
    def error_logger(code, response):
        """
            에러 response 발생시 해당 request와 결과를 기록
        """
        time_str = utc('kr').strftime('%Y.%m.%d %H:%M:%S')
        if g.get('started'):
            elapsed = (datetime.utcnow() - g.started).total_seconds()
        else:
            elapsed = None

        # error database에 저장
        collection and collection.insert({
            "method":request.method,
            "path":request.path,
            "session":encode_mongo(dict(session)),
            "unity_id":session.get('unity_id'),
            "user_id":session.get('user_id'),
            "url":request.url,
            "args":copy_dict(request.args),
            "form":copy_dict(request.form),
            "data":request.data,
            "json":request.json,
            "headers":dict(request.headers.to_list()),
            "remote_addr":request.remote_addr,
            "time":time_str,
            "elapsed":elapsed,
            "code":code,
            "response":response
        })
    return error_logger

def print_tb():
    """
        에러가 났을 때 스택을 출력한다
    """
    exc_type, exc_obj, exc_tb = sys.exc_info()
    if exc_tb:
        traceback.print_tb(exc_tb)
        print('#####', exc_obj.__repr__())
    else:
        print('===== No Exception Info =====')

def throw(*args):
    """
        Exception을 발생시킨다.
        
        주로 로그에 남기기 위해 메시지를 정비해서 재발생시키거나,
        DB의 롤백을 위한 try - except 구문에서 스택을 연장시키기 위해 사용한다.
        
        @args args_0[*Exception or str] 새로운 Exception 객체 혹은 단순 메시지
    """
    exc_type, exc_obj, exc_tb = sys.exc_info()
    if args:
        a = args[0]
        if isinstance(a, BaseException):
            raise(a, None, exc_tb)
        elif isinstance(a, basestring):
            raise(RuntimeError, a, exc_tb)
    else: # 기존 exception의 stack을 이어받는다
        raise(exc_obj, None, exc_tb)

        

### Response Dump 관련

def dump_response(response, dump_coll, users_coll=None):

    # 모든 request, response 저장하기
    elapsed = (datetime.utcnow() - g.started).total_seconds()
    req = {
        "method":request.method,
        "path":request.path,
        "session":encode_mongo(dict(session)),
        "unity_id":session.get('unity_id'),
        "user_id":session.get('user_id'),
        "remote_addr":request.remote_addr,
        "time":utc('kr'),
        "elapsed":elapsed,
        "request":{
            "url":request.url,
            "host":request.host,
            "url_rule":request.url_rule.rule,
            "args":dict([(k,v) for k,v in request.args.iteritems()]),
            "form":dict([(k,v) for k,v in request.form.iteritems()]),
            "data":request.data,
            "json":request.json,
            "headers":dict(request.headers.to_list()),
            "user_agent":request.user_agent.string
        }
    }
    if session.get('phone') and users_coll:
        user = users_coll.find_one({'phone':session.get('phone')}, {'_id':0,'name':1,'model':1,'rcall_id':1})
        if user:
            req.update(user)
    try:
        j = from_json(response.get_data())
        req.update(j)
    except: # body가 json이 아닐 때
        pass

    try:
        req['response'] = {
            'data':response.get_data(),
            'headers':dict(response.headers.to_list()),
            'status':response.status,
            'status_code':response.status_code
        }
    except Exception as e: # RuntimeError
        req['response'] = 'RuntimeError'

    try:
        dump_coll.insert_one(req)
    except Exception as e: # response가 DB에 담을 수 없는 형태일 때 (key가 '$oid' 등)
        try:
            req['response'] = 'InsertError'
            dump_coll.insert_one(req)
        except: # DB 에러일 때
            fprint('insert error', e, request.path)
            pass



### Decorator 패턴 관련
# Decorator는 내부에서 db등의 의존성이 발생할 경우 getter 를 한겹 더 감싸야 한다.

def deco_cached(cache):
    def cached(expire=10*60):
        """
            cached는 기본 10분
        """
        def decorator(f):
            @wraps(f)
            def _wrap(*args, **kwargs):
                if not cache:
                    return f(*args, **kwargs)
                cache_key = request.url
                res = cache.get(cache_key)
                if res != None:
                    return pickle.loads(res)
                res = f(*args, **kwargs)
                # response 이고 200 ok 가 아니라면 캐시하지 않는다
                if type(res) != flask.wrappers.Response or res.status_code == 200:
                    cache.setex(cache_key, pickle.dumps(res), expire) # redis
                return res
            return _wrap
        return decorator
    return cached

def deco_lazy_cached(cache):
    def lazy_cached(refresh=10*60, expire=None):
        """
            lazy_cached 는 cache가 존재하면 바로 그 값을 반환하고
            만약 refresh 시간이 지났다면 background에서 값을 새로 갱신한다
            refresh만 주어졌다면 expire는 2배의 값으로 정해진다
        """
        if expire == None:
            expire = refresh * 2
        def decorator(f):
            @wraps(f)
            def _wrap(*args, **kwargs):
                if not cache:
                    return f(*args, **kwargs)
                cache_key = request.url
                res = cache.get(cache_key)
                ttl = cache.ttl(cache_key)
                if res != None:
                    if ttl < expire - refresh and not cache.get('lazy cache '+cache_key):
                        cache.setex('lazy cache '+cache_key, 1, 3600)
                        #print 'middle with refresh, expire', ttl
                        def update_in_background():
                            #time.sleep(2)
                            res = f(*args, **kwargs) # TODO: thread에서 request 에 접근하면 문제가 발생한다
                            # response 이고 200 ok 가 아니라면 캐시하지 않는다
                            if type(res) != flask.wrappers.Response or res.status_code == 200:
                                cache.setex(cache_key, pickle.dumps(res), expire).del_('lazy cache '+cache_key) # redis_wrap
                        # do with thread or MQ, only once
                        t = Thread(target=update_in_background)
                        t.daemon = True
                        t.start()
                    return pickle.loads(res)
                res = f(*args, **kwargs)
                # response 이고 200 ok 가 아니라면 캐시하지 않는다
                if type(res) != flask.wrappers.Response or res.status_code == 200:
                    cache.setex(cache_key, pickle.dumps(res), expire).del_('lazy cache '+cache_key) # redis
                return res
            return _wrap
        return decorator
    return lazy_cached

def deco_time_cached(cache):
    from croniter import croniter
    def time_cached(refresh=10*60, expire=None, cronstr='* * * * *'):
        """
            time_cached 는 lazy_cached 처럼 동작하되 cache를 하는 시간대를 지정할 수 있다
        """
        if expire == None:
            expire = refresh * 2
        def decorator(f):
            @wraps(f)
            def _wrap(*args, **kwargs):
                if not cache:
                    return f(*args, **kwargs)
                cache_key = request.url
                now = utc('kr') # kst
                iter = croniter(cronstr, now - timedelta(minutes=1))
                next_time = iter.get_next(datetime) # kst
                if next_time > now: # 시간 범위 내가 아니라면 cache를 delete하고 skip
                    cache.del_(cache_key)
                    return f(*args, **kwargs)
                res = cache.get(cache_key)
                ttl = cache.ttl(cache_key)
                if res != None:
                    if ttl < expire - refresh and not cache.get('lazy cache '+cache_key):
                        cache.setex('lazy cache '+cache_key, 1, 3600)
                        #print 'middle with refresh, expire', ttl
                        def update_in_background():
                            #time.sleep(2)
                            res = f(*args, **kwargs) # TODO: thread에서 request 에 접근하면 문제가 발생한다
                            # response 이고 200 ok 가 아니라면 캐시하지 않는다
                            if type(res) != flask.wrappers.Response or res.status_code == 200:
                                cache.setex(cache_key, pickle.dumps(res), expire).del_('lazy cache '+cache_key) # redis_wrap
                        # do with thread or MQ, only once
                        t = Thread(target=update_in_background)
                        t.daemon = True
                        t.start()
                    return pickle.loads(res)
                res = f(*args, **kwargs)
                # response 이고 200 ok 가 아니라면 캐시하지 않는다
                if type(res) != flask.wrappers.Response or res.status_code == 200:
                    cache.setex(cache_key, pickle.dumps(res), expire).del_('lazy cache '+cache_key) # redis
                return res
            return _wrap
        return decorator
    return time_cached


# deprecated
def deco_trace_except(collection, error=lambda:'error'):
    """
        collection - exception을 기록할 mongodb의 collection
        _common.py 등에서 trace_except = deco_trace_except(db.client.error.voice, error) 로 가져온 뒤
        @trace_except 로 사용한다.
    """
    # 필요시 이하의 함수를 _common.py 등으로 옮기면 바로 사용할 수 있다.
    def trace_except(fn):
        """
            에러 발생시 해당 위치를 mongodb의 error db에 기록.
            모든 app.route를 트래킹하려면 위의 get_error_handler 쪽을 사용하는게 좋다.
        """
        @wraps(fn)
        def _wrap(*args, **kwargs):
            try:
                return fn(*args, **kwargs)
            except Exception as e:
                exc_type, exc_obj, exc_tb = sys.exc_info()
                tb = exc_tb
                while tb.tb_next: tb = tb.tb_next
                time_str = utc('kr').strftime('%Y.%m.%d %H:%M:%S')

                # error database에 저장
                collection.insert({
                    "file":fn.func_code.co_filename,
                    "func":fn.func_name,
                    "lineno":tb.tb_frame.f_lineno,
                    "time":time_str,
                    "except":e.__repr__(),
                    "stack":traceback.format_tb(exc_tb),
                    "method":request.method,
                    "url":request.url,
                    "req_headers":dict(request.headers.to_list()),
                    "args":copy_dict(request.args),
                    "form":copy_dict(request.form),
                    "data":request.data,
                    "remote_addr":request.remote_addr
                })
                traceback.print_tb(exc_tb)
                print(e.__repr__())
                return error()
        return _wrap
    return trace_except


def deco_ip_proof(ip_list=('121.66.58.',), api_db=None, error=lambda:'not allowed'):
    """
        api_db - bridge api mongodb
    """
    # 필요시 이하의 함수를 _common.py 등으로 옮기면 바로 사용할 수 있다.
    def ip_proof(fn):
        """
            요청자의 ip가 각 서버나 주어진 ip(ex:사무실 - 121.66.58.202/24)에 해당하는지를 체크한다.
        """
        @wraps(fn)
        def _wrap(*args, **kwargs):
            server_list = api_db.server_list.find({},{'ip':1,'private_ip':1}) if api_db else []
            server_ip_list = [row['ip'] for row in server_list] + [row['private_ip'] for row in server_list if row.get('private_ip')]
            if request.remote_addr in server_ip_list:
                return fn(*args, **kwargs)
            for ip in ip_list:
                if request.remote_addr.startswith(ip):
                    return fn(*args, **kwargs)
            return error()
        return _wrap
    return ip_proof


# 바로 사용 가능한 decorator는 그대로 쓴다.


def req_header(fn):
    """
        apidoc의 try it으로 보낸 request의 헤더값을 response의 헤더에 보낸다.
    """
    @wraps(fn)
    def _wrap(*args, **kwargs):
        response = fn(*args, **kwargs)
        if request.headers.get('X-Requested-With') == 'XMLHttpRequest':
            if type(response) is not flask.wrappers.Response:
                response = make_response(response)
            response.headers.extend({"Apidoc-Request-Header":json.dumps(dict(request.headers.to_list()))}) # 헤더 추가
        return response
    return _wrap

def synchronized(fn=None, lock=Lock()):
    """
        Synchronization decorator.
        
        Global lock은 그냥 사용
        @synchronized
        def global_func():
            ....
            
        특정 lock을 사용할 때는 선언후 적용
        from threading import Lock
        mysql_lock = Lock()
        @synchronized(mysql_lock)
        def db_func():
            ....
    """
    if hasattr(fn, 'locked'): # @synchronized(Lock()) 같이 lock을 주는 경우
        lock = fn
    def wrap(f):
        def newFunction(*args, **kw):
            lock.acquire()
            try:
                return f(*args, **kw)
            finally:
                lock.release()
        return newFunction
    if hasattr(fn, 'locked'):
        return wrap
    return wrap(fn)

def nocache(fn):
    """
        backspace등으로 돌아간 경우라도 브라우저 캐시를 이용하지 않고 새로고침 하도록 한다.
        
        실시간 데이터가 중요한 경우.
        no-cache : 캐쉬는 보관하지만 서버에 요청하여 유효성 검사
        no-store : 어떤 상황에서도 저장하지 않음
        no-transform : 중간에 프록시 서버가 있는 경우 html 최적화 등의 변경을 하지 않게 요청
        must-revalidate : 오프라인일 때 유효하지 않은 캐시를 쓰는 경우가 있지만 그것도 막는다
        http://www.letmecompile.com/http-cache-%ED%8A%9C%ED%86%A0%EB%A6%AC%EC%96%BC/ 참조
    """
    import flask.wrappers
    @wraps(fn)
    def _wrap(*args, **kwargs):
        response = fn(*args, **kwargs)
        if type(response) is not flask.wrappers.Response:
            response = make_response(response)
        response.headers.set("Cache-Control", "no-cache, no-store, must-revalidate, no-transform, max-age=1") # history에 과거화면 남겨두지 않기
        # response['Cache-Control'] = 'no-cache, no-store, must-revalidate, no-transform, max-age=1' # history에 과거화면 남겨두지 않기, django
        return response
    return _wrap



### Api Document 관련
# /apidoc이나 /hashkey 같은 tool 그리고 apidoc.html 파일은 각 api 가 별도로 가지고 있어야 한다.

def parse_api_file(path=os.path.dirname(os.path.abspath(__file__))+'/', basefile='__init__.py',\
        error_reason={}, error_desc={}):
    """
        __init__.py 파일에서 대상들 가져오기
    """
    f = open(path+basefile)
    src_init = f.read()
    f.close()
    
    lines = re.split('\r?\n', str_slice(src_init, '###apidoc begin###', '###apidoc end###'))
    base_files = []
    for line in lines:
        if 'import' in line and line[0] != '#':
            filepath = path
            p = str_slice(line, 'from\s+', '\s+import')
            if p:
                p = p.replace('.','/').replace('//','../')
            else:
                p = ''
            file = str_slice(line, 'import\s+', '(\s+|$)').replace('.','/')+'.py'
            file = p + file
            if file.startswith('base/'): # base 일 경우만 특별취급
                # sys.path 의 뒤쪽 2개를 가져온다
                for sys_path in sys.path[-2:]:
                    if os.path.exists(sys_path+'/'+file):
                        filepath = sys_path+'/'
                        break;
            name = re.split('#+\s*', line)
            name = name[1] if len(name) > 1 else ''
            base_files.append({'path':filepath, 'file':file, 'name':name})
    
    docs = []
    for file in base_files:
        try:
            f = open(file['path']+file['file'])
        except:
            base_dir = os.environ.get('BASE_DIR')
            if base_dir:
                try:
                    f = open(base_dir+file['file'])
                except:
                    print('in parse_api_file,', file['path']+file['file'], base_dir+file['file'], 'not exists')
                    continue
            else:
                print('in parse_api_file,', file['path']+file['file'], 'not exists')
                continue
        src = f.read()
        f.close()
        
        # 해당 파일을 설명하는 맨 처음의 주석
        part = file['file'].replace('.py','').replace('../', '~') # parent 경로의 치환
        src_desc = re.findall('\r?\n"""(@?#?)\s*\r?\n([\s\S]*?)\r?\n"""', src)
        title = ""; desc = ""; menu = ""; menu_icon = None; hide = False; desc_fin = False
        if src_desc: # 주석 존재시
            if '@' in src_desc[0][0]: # apidoc 스타일인 경우 추가 파싱
                hide = '#' in src_desc[0][0]
                lines = src_desc[0][1].strip().splitlines()
                title = lines[0]
                for i in range(1, len(lines)):
                    line = lines[i].strip()
                    if not desc_fin and not line.startswith('@'):
                        desc += re.sub('^# ', '', line)+'\n'
                        continue
                    else:
                        desc_fin = True
                    if line.startswith('@menu'): # @menu 010100 fa-icon # gnb숫자 이후값 존재시 전부 icon class에 추가됨
                        menu = line.replace('@menu', '').split('#')[0].strip().split(' ', 1)
                        if len(menu) > 1:
                            menu_icon = menu[1]
                            if menu_icon.startswith('fa') or menu_icon.startswith('glyphicon'): # fa fa-icon 처럼 fa 추가 처리
                                menu_icon = menu_icon.split('-', 1)[0]+' '+menu_icon
                        menu = menu[0]
            else: # 전체를 설명으로
                desc = src_desc[0][1]
        doc = {
            'file':file['file'].replace('.py',''),
            'name':file['name'],
            'part':part,
            'title':title,
            'menu':menu,
            'menu_icon':menu_icon,
            'hide':hide,
            'desc':desc,
        }
        
        # app.route 부터 설명 주석까지의 정규식
        src_apis = re.findall('(((@app.route\([\'"].*?[\'"](\)|, methods=\[.*?\]\))\s*\r?\n)+)'+\
            '((\s*(@\w+\s*(\([^)]*\))?|#.*?)\s*\r?\n)*)'+\
            #'(@ip_proof\s*\r?\n)?(@auth\s*\r?\n)?'+\
            'def ([^:(]+)\(.*?\):.*\r?\n\s*("""@(#?)([\s\S]*?)""")?)', src)

        apis = []
        for src_api in src_apis:
            # src_api => ("전체", "@app.route('/apidoc')\r\n@app.route('/apidoc/')\r\n", "@app.route('/apidoc/')\r\n", ")", "@ip_proof\r\n@auth\r\n", "@auth\r\n", "", "", "함수이름", "주석전체", "#", "주석내용")
            routes = [
                    {"uri":route[0],
                    "methods":[m.replace(" ","").replace("'","") for m in route[2].split(',')] if route[2] else ['GET']
                    }
                for route in re.findall('@app.route\([\'"](.*?)[\'"](\)|, methods=\[(.*?)\]\))\s*', src_api[1])
            ]
            #decos = [ route[0] for route in re.findall('(@\w+)\s*(\([^)]*\))?\s*\r?\n', src_api[4]) ]
            decos = [deco[0][0] for deco in [re.findall('^\s*(@\w+)\s*(\([^)]*\))?\s*', line) for line in src_api[4].splitlines() if line] if deco]
            
            # route => ('/apidoc/<part>', ", methods=['GET','POST'])", "'GET','POST'")
            """@ <- apidoc임을 구분하기 위해 맨 앞에 @ 를 붙여야 합니다. apidoc에서 숨기고싶다면 @#으로 시작합니다(이 경우 doc2param, role 등만 사용하기 위함)
                첫 줄은 기능의 한 줄 설명입니다(필수)
                # 샵이 붙은 곳은 상세설명입니다. 샵은 생략 가능합니다
                # 여러 줄이 나와도 됩니다. 샵과 뒤의 스페이스 한 칸까지 제거 후 실제로 출력됩니다. 샵이 없는 경우 strip을 하므로 스페이스를 포함시키려면 샵이 필요합니다.
                # {
                #   "desc":"json 형태를 보여주려면 이렇게"
                # }
                # 파라미터의 기본 형태는 
                # URL은 url경로에 포함된 파라미터 입니다. ex) /apidoc/<ver>/ -> @param URL ver[string]
                # GET은 query string으로 주어지는 파라미터 입니다. ex) /v2/?show=true -> @param GET show[bool]
                # POST는 request의 body로 주어지는 파라미터 입니다. Content-type: application/x-www-form-urlencoded
                # JSON은 request의 body로 주어지는 json 입니다. Content-type: application/json
                # type은 [string|bool|int|float|file|jsonObject|jsonArray|jsonObjectArray|fixed|enum] 등이 있습니다. type 앞에 * 이 있는 경우는 optional 입니다. [*type=value] 의 value는 default 값이며 default값이 있으면 항상 optional입니다. no[int]`32` 처럼 뒤에 나오는 32 는 예제값 입니다.
                # JSON result는 기본 object({"code":xxxxx, "reason":""}) 로 시작하며 하위 데이터는 | 로 시작하고 그 하위가 될 때마다 |가 추가됩니다.
                @deprecated deprecated의 내용 
                @role LOGIN API ETC ADMIN ALL # 이 uri에 접근 가능한 role들, 생략시 ALL
                @menu 090100 fa-desktop # admin에서 메뉴로 노출될 uri인 경우
                @param URL ver[*string=v2]`v2` API 버전
                @param GET show[*bool]`true` 보여줄지의 여부
                @param POST phones[string] 연락처 리스트
                @param POST file[file] 음성파일
                @param JSON 
                    phones[jsonArray] 연락처 리스트
                        |value[string] 연락처
                    row[jsonObject] 
                        |os_type[enum]`ANDROID` os 타입(IOS, ANDROID 중 하나)
                        |user[jsonObject] 유저정보
                            ||name[string] 이름
                            ||phone[string] 연락처
                @result phone[string] 전화번호
                @result code[int] 결과값
                @result JSONC code가 기본 탑재된 형태의 json response
                @result JSON
                    code[int] 결과코드
                    reason[string] 결과메시지
                    row[jsonObject] 결과 row
                        |one[string] 성공여부(1 - 성공, 0 - 실패)
                @result HTML
                @result TEXT
                @result BINARY 음성파일
                @fail 40000
                @fail 40100
            """
            lines = src_api[-1].strip().splitlines()
            if not lines: # apidoc이 없는 경우
                api = {
                    "routes":routes,
                    "decos":decos,
                    "func":src_api[-4],
                    "hide":True,
                    "title":src_api[-4],
                    "desc":"",
                    "params":[],
                    "param_example":"",
                    "results":[],
                    "result_example":"",
                    "fails":[],
                    "deprecated":"",
                    "role":['ALL'],
                    "menu":"",
                    "menu_icon":None,
                }
                apis.append(api)
                continue
                
            title = lines[0]
            desc = ""; params = []; param_example = ""; results = []; result_example = ""; fails = []; deprecated = ""; role = ['ALL']; menu = ""; menu_icon = None; desc_fin = False
            for i in range(1, len(lines)):
                line = lines[i].strip()
                if not desc_fin and not line.startswith('@'):
                    desc += re.sub('^# ', '', line)+'\n'
                    continue
                else:
                    desc_fin = True
                if line.startswith('@param '):
                    arr = re.findall('\\s*@param ([A-Z]+)( ([^\\[ ]+)\\[(\\*?)([a-z_A-Z]*)(\\=([^\\]]*))?\\](`[^`]*`)?\\s*?(.*))?', line)[0]
                    # @param A b[*c=d]`e` f => ('A', ' b[*c=d]`e` f', 'b', '*', 'c', '=d', 'd', '`e`', 'f')
                    # index : A = 0, b = 2, * = 3, c = 4, d = 6, `e` = 7, f = 8
                    param = {"group":arr[0], "name":arr[2], "optional":arr[3], "type":arr[4], "default_assign":arr[5], "default":arr[6], "example":arr[7].replace('`',''), "desc":arr[8]}
                    if arr[0] == 'JSON': # JSON인 경우
                        j = []
                        while len(lines) > i+1 and lines[i+1].strip()[0] not in ['#', '@']:
                            i += 1
                            sub_arr = re.findall('([^\\[ ]+)\\[(\\*?)([a-z_A-Z]*)(\\=([^\\]]*))?\\](`[^`]*`)?\\s?(.*)', lines[i].strip())[0]
                            # phones[jsonarray] 연락처 => ('phones', '', 'jsonarray', '', '', '연락처')
                            name = sub_arr[0]
                            depth = name.count('|')
                            j.append({"name":name.replace('|',''), "depth":depth, "optional":sub_arr[1], "type":sub_arr[2], "default":sub_arr[4], "example":sub_arr[5].replace('`',''), "desc":sub_arr[6]})
                        param['json'] = j
                    params.append(param)
                elif line.startswith('@param_example'):
                    padding = lines[i+1].index(lines[i+1].strip())
                    arr = []
                    while len(lines) > i+1 and lines[i+1].strip()[0] not in ['#', '@']:
                        i += 1
                        arr.append(lines[i][padding:])
                    param_example = "\n".join(arr)
                elif line.startswith('@result '):
                    arr = re.findall('@result ([A-Z]+|([^\\[ ]+)\\[(\\*?)([a-z_A-Z]*)\\])\\s?(.*)?', line)[0]
                    # @result phone[string] 전화번호 => ('phone[string]', 'phone', '*', 'string', '전화번호')
                    # @result JSON 문서 => ('JSON', '', '', '', '문서')
                    if arr[0] in ['HTML','TEXT','BINARY']: # 단일 타입인 경우
                        result = {"prop":arr[0], "desc":arr[4]}
                    elif arr[0][:4] == 'JSON': # JSON 으로 시작하는 경우
                        result = {"prop":arr[0], "desc":arr[4]}
                        j = []
                        while len(lines) > i+1 and lines[i+1].strip()[0] not in ['#', '@']:
                            i += 1
                            sub_arr = re.findall('([^\\[ ]+)\\[(\\*?)([a-z_A-Z]*)\\]\\s?(.*)', lines[i].strip())[0] # |id[int] 판매정보 ID     의 형태
                            # phones[jsonarray] 연락처 => ('phones', '', 'jsonarray', '연락처')
                            name = sub_arr[0]
                            depth = name.count('|')
                            j.append({"name":name.replace('|',''), "depth":depth, "optional":sub_arr[1], "type":sub_arr[2], "desc":sub_arr[3]})
                        result['json'] = j
                    else:
                        result = {"prop":"FIELD", "name":arr[1], "type":arr[3], "optional":arr[2], "desc":arr[4]}
                    results.append(result)
                elif line.startswith('@result_example'):
                    padding = lines[i+1].index(lines[i+1].strip())
                    arr = []
                    while len(lines) > i+1 and (not lines[i+1].strip() or lines[i+1].strip()[0] not in ['#', '@']):
                        i += 1
                        arr.append(lines[i][padding:])
                    result_example = "\n".join(arr)
                elif line.startswith('@fail '):
                    arr = re.findall('@fail ([0-9]+)\\s?(.*)?', line)[0]
                    # @fail 40000 해시키 에러 => ('40000', '해시키 에러')
                    fail = {"code":int(arr[0]), "reason":error_reason.get(int(arr[0])), "desc":str(error_desc.get(int(arr[0])) or '')+('. '+arr[1] if arr[1] else '')}
                    fails.append(fail)
                elif line.startswith('@deprecated'):
                    deprecated = line.replace('@deprecated', '').strip()+' '
                elif line.startswith('@role'): # 없을 경우 ['ALL']
                    role = [r.strip() for r in line.replace('@role', '').split('#')[0].replace(',','').strip().split(' ')]
                elif line.startswith('@menu'): # @menu 010100 fa-icon # gnb숫자 이후값 존재시 전부 icon class에 추가됨
                    menu = line.replace('@menu', '').split('#')[0].strip().split(' ', 1)
                    if len(menu) > 1:
                        menu_icon = menu[1]
                        if menu_icon.startswith('fa') or menu_icon.startswith('glyphicon'): # fa fa-icon 처럼 fa 추가 처리
                            menu_icon = menu_icon.split('-', 1)[0]+' '+menu_icon
                    menu = menu[0]
            
            # src_api[-2] = '' or '#' => hide
            api = {
                "routes":routes,
                "decos":decos,
                "func":src_api[-4],
                "hide":src_api[-2],
                "title":title,
                "desc":desc,
                "params":params,
                "param_example":param_example,
                "results":results,
                "result_example":result_example,
                "fails":fails,
                "deprecated":deprecated,
                "role":role,
                "menu":menu,
                "menu_icon":menu_icon,
            }
            apis.append(api)
        doc['apis'] = apis
        docs.append(doc)
    return docs
    
def parse_role(db, path=os.path.dirname(os.path.abspath(__file__))+'/', basefile='__init__.py', role_rel=None, key=''):
    """
        role만 추출하여 db에 replace한다. role cache도 날린다.
        db는 api, role(redis)을 가지고 있는 Params이며 api에는 role_uri 테이블이 있어야 한다
        추후 uri access때 redis에 해당 유저의 role 추출이 없다면 DB에서 추출해온다.
        menu도 우선 가능한 모든 menu를 추출하여 cache에 넣어둔다
    """
    if not db.api or not db.api.show_tables(name='role_uri') or not db.role:
        return False
        
    docs = parse_api_file(path, basefile)
    now = kr2utc()
    roles = []; menus = []
    for doc in docs:
        submenu = []
        for api in doc['apis']:
            for role in api['role']:
                for route in api['routes']:
                    roles.append({
                        'role':role,
                        'uri':route['uri'],
                        'methods':','.join(route['methods']),
                        'create_time':now,
                    })
            if api['menu']: # 메뉴 존재시
                route = api['routes'][-1] # 마지막 route를 메뉴로 추가
                submenu.append({
                    'code':api['menu'],
                    'link':route['uri'],
                    'name':api['title'],
                    'icon':api['menu_icon'],
                    'role':api['role'],
                })
        if doc['menu'] and submenu:
            menu = {
                'group':doc['file'],
                'hide':doc['hide'], # side menu에서 보일지의 여부
                'code':doc['menu'],
                'name':doc['title'],
                'icon':doc['menu_icon'],
                'submenu':submenu,
            }
            menus.append(menu)
    
    # role 만 추출하여 db에 저장
    db.api.query('truncate table role_uri');
    db.api.insert('role_uri', roles)
    
    # role_rel 이 있을 경우 관계도도 설정
    if role_rel: # {'ADMIN':'LOGIN,DASH,MANAGE', 'SUPER':'ADMIN,ADMIN_MANAGE', ..}
        role_rels = []
        for role, own_role in role_rel.iteritems():
            [role_rels.append({'role':role, 'own_role':v, 'create_time':now}) for v in own_role.replace(' ','').split(',')]
        db.api.query('truncate table role_rel');
        db.api.insert('role_rel', role_rels)
    
    # cache 날리기
    db.role.delete('SIDE_MENU_{}'.format(key))
    db.role.del_('ROLE_{}_*'.format(key))
    
    # menu의 원형을 cache에 생성해둔다
    """
    SIDE_MENU = [
        {
            'group':'dashboard', 'code':'01', 'name':'Dashboard', 'icon':'tachometer',
            'submenu':[
                {'code':'0101', 'link':'/dashboard', 'name':'Dashboard', 'icon':None},
            ]
        },
    ]
    """
    #hanprint(menus)
    db.role.json('SIDE_MENU_{}'.format(key), menus)
    
    return True

def make_api_file(path=os.path.dirname(os.path.abspath(__file__))+'/', basefile='__init__.py', error_reason={}, error_desc={}):
    """
        __init__.py 파일에서 api comment를 가져와서 api.py 파일을 만든다
    """
    docs = parse_api_file(path, basefile, error_reason, error_desc)
    for doc in docs:
        for api in doc['apis']:
            params = api['params']
            # 필수인 것을 앞으로 가져온다
            sorted(params, lambda a,b:a['optional'] < b['optional'])
            print(api['routes'][-1]['uri'], ', '.join(map(lambda a:a['name']+'['+a['type']+']', params)))
    return docs
    
    
def attach_dummy_route(app):
    """
        app을 받아와서 해당 app에서 /dummy로 시작하는 route는 dummy 데이터(apidoc의 result_example)를 반환하도록 한다.
    """
    @app.route('/dummy/', methods=["GET","POST","PUT","PATCH","DELETE"])
    @app.route('/dummy/<path:uri>', methods=["GET","POST","PUT","PATCH","DELETE"])
    def dummy_route(uri=''):
        """
            result example을 실제 response로 보내준다. result example이 없을 경우 404를 보낸다.
        """
        for rule in app.url_map.iter_rules():
            if type(rule.match('|'+request.path[6:])) is dict:
                if request.method in rule.methods:
                    doc = app.view_functions[rule.endpoint].__doc__
                    example = extract_result_example(doc)
                    if example:
                        try:
                            resp = make_response(to_json(json.loads(example)))
                            resp.headers.set("Content-Type", "application/json")
                            return resp
                        except:
                            return example
                    else:
                        abort(404)
                abort(405)
        abort(404)

def extract_result_example(doc):
    """
        주어진 document 에서 @result_example 파트를 가져와서 반환한다
    """
    lines = doc.strip().splitlines()
    result_example = ""
    for i in range(0,len(lines)):
        line = lines[i].strip()
        if line.startswith('@result_example'):
            padding = lines[i+1].index(lines[i+1].strip())
            arr = []
            while len(lines) > i+1 and lines[i+1].strip()[0] not in ['#', '@']:
                i += 1
                arr.append(lines[i][padding:])
            result_example = "\n".join(arr)
    return result_example

def attach_test_route(app, db_list=None):
    """
        app을 받아와서 해당 app에서 /test로 시작하는 route는 실제로 db나 기타 파일들을 기록하지 않는 test용도로 사용할 수 있도록 한다.
    """
    @app.route('/test/', methods=["GET","POST","PUT","PATCH","DELETE"])
    @app.route('/test/<path:uri>', methods=["GET","POST","PUT","PATCH","DELETE"])
    def test_route(uri=''):
        """
            해당 기능을 실제로 테스트한다. DB의 경우는 transaction으로 관리하여 실제로 기록되지 않게 한다.
        """
        import re
        for rule in app.url_map.iter_rules():
            match = rule.match('|'+request.path[5:])
            if type(match) is dict:
                if request.method in rule.methods:
                    # do some transaction
                    func = app.view_functions[rule.endpoint]
                    res = func(**match)
                    # end transaction
                    return res
                abort(405)
        abort(404)

###

except_list = (globals().get('except_list') or []) + list(globals()) # standalone.py 용 제외리스트
