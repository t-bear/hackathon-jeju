# -*- coding: utf-8 -*-
"""
    DB 및 기타등등 초기화
    
    모듈간 DB를 공통으로 쓰기 위함
"""
from socketIO_client import SocketIO

SOCKET_SERVER_KEY = '909e83d21d4cf0828cba11ffeeb60b2a18c90fd813bc35d628269b506b6dcaa6'
SOCKET_ADMIN_KEY = '5beaf18d9ef15c15cca7df549142ea36e722e472d95ec97a1a22148187f562d9'
try:
    from .findata import db
    from .database import load_db_list, get_db as _get_db, Mysql
    from .util import *
    SOCKET_KEY = SOCKET_ADMIN_KEY
except:
    import sys, os
    stdin, stdout, stderr = sys.stdin, sys.stdout, sys.stderr
    sys.stdin, sys.stdout, sys.stderr = stdin, stdout, stderr
    sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/..')
    if os.getenv('DEV') == 'SSH_WIN':
        os.environ["SETTING_DIR"] = "/qara"
    else:
        os.environ["SETTING_DIR"] = "/www/qara/settings"

    from .database import load_db_list, get_db as _get_db, Mysql, Redis, redis, MongoClient
    from .util import *

    if os.getenv('KOSHO_MODE') == 'DEV':
        load_db_list(os.getenv('SETTING_DIR')+'/db_kevin.json')
    else:
        load_db_list(os.getenv('SETTING_DIR')+'/db.json')
    db = None
    SOCKET_KEY = SOCKET_SERVER_KEY


def set_db(dbname='qara_real', host='localhost'):
    db = DefaultParams()
    db.dbname = dbname
    db.host = host
    db.api = db.default = _get_db(dbname)
    db.cache = Redis(host, db=1)
    db.search = redis.Redis(host, db=3) # 3번 db는 검색어 캐시 전용이다
    db.chart = Redis(host, db=5) # 5번 db는 chart 용 데이터 및 계좌조회용 데이터
    db.sess = Redis(host, db=8) # session db
    db.role = Redis(host, db=11) # 11번 db는 role 전용이다.
    db.pubsub = redis.Redis(host, db=15) # pubsub db
    db.dump = _get_db('dump')
    db.fin = _get_db('fin')
    db.meta = _get_db('meta')
    db.proxy = _get_db('proxy')
    
    return db
    #db = sys._getframe().f_back.f_globals.get('db') # 윗 프레임의 db를 가져온다.

def get_db(dbname='qara_real', host='localhost'):
    if not db:
        return set_db(dbname, host)
    return db

def get_socket():
    try:
        so = SocketIO('https://app.qara.kr', 8081, params={'id':SOCKET_KEY})
        so.on('echo', echo)
        #so.wait(seconds=1)
        return so
    except Exception as e:
        print('socket error', e)
    return None






if __name__ == "__main__": command(globals())