from flask import Flask, jsonify, render_template

from .api import api

app = Flask(__name__,
            static_folder = "../dist/static",
            template_folder = "../dist", 
            instance_relative_config=True)

app.register_blueprint(api, url_prefix='/api')


# check healthy
@app.route('/ping', methods=['GET'])
def ping_pong():
    return jsonify('pong!')


@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def catch_all(path):
    return render_template("index.html")