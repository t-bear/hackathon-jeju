import torch.utils.data as data
import torch
import os
import numpy as np
import random
from preprocessing import Data


def process_to_chop_pad(orgids, requiredsize):
    if (len(orgids) >= requiredsize):
        return orgids[:requiredsize]
    else:
        padids = [0] * (requiredsize - len(orgids))
        return (orgids + padids)


class Dataset(data.Dataset):
    def __init__(self, config, data_mode):
        self.config = config
        self.data_mode = data_mode
        self.data = torch.load(os.path.join(config.input_directory,
                                            "{}.{}".format(config.data_type, data_mode)))

    def __getitem__(self, index):
        docs = np.zeros((self.config.docu_len, self.config.sent_len), dtype=int)
        for i, sentence in enumerate(self.data.docs[index]):
            docs[i] = (process_to_chop_pad(sentence, self.config.sent_len))

        randidx = np.random.randint(0, self.config.num_label-1)
        if randidx >= len(self.data.labels[index]):
            randidx = 0
        labels = np.array([[1,0] if i in self.data.labels[index][randidx] else [0,1] for i in range(self.config.docu_len)])

        rewards = self.data.rewards[index][randidx]
        weights = np.array(process_to_chop_pad(self.data.weights[index], self.config.docu_len), dtype=np.float32)

        return docs, labels, rewards, weights, self.data.filenames[index]

    def __len__(self):
        return self.data.len


def get_loader(config, data_mode):
    dataset = Dataset(config, data_mode)
    loader = data.DataLoader(dataset, batch_size=config.batch_size, shuffle=True, num_workers=2)
    return loader


