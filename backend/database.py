# -*- coding: utf-8 -*-
"""
    데이터베이스 관리 일원화.
    
    아래와 같은 형태의 db.json을 _constant.py의 DB_JSON_PATH에 설정해줘야 한다
    [  
       {
          "role":"api",
          "type":"mongodb_rs",
          "replSet":"bridge_api",
          "db":"api",
          "uri":"ec2-54-248-233-51.ap-northeast-1.compute.amazonaws.com",
          "uri_dev":"localhost"
       },
       {  
          "role":"sip",
          "type":"mysql_master",
          "host":"117.52.91.106",
          "user":"root",
          "password":"",
          "db":"asteriskrealtime",
          "desc":"gcloud sip db master"
       },
       {  
          "role":"sip",
          "type":"mysql_slave",
          "host":"117.52.91.106",
          "user":"root",
          "password":"",
          "db":"asteriskrealtime",
          "desc":"gcloud sip db slave"
       },
       {  
          "role":"campaign_cache",
          "type":"redis_wrap",
          "db":8,
          "host":"search.bridgecall.kr",
          "host_dev":"localhost"
       },
       {  
          "role":"admin_lang",
          "type":"sqlite",
          "path":"/www/bridge_admin/translator.db"
       }
    ]
    
    혹은 django 형식의 setting도 가져올 수 있다
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.mysql', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
            'NAME': 'mpb',                      # Or path to database file if using sqlite3.
            'USER': 'root',                      # Not used with sqlite3.
            'PASSWORD': '',                  # Not used with sqlite3.
            'HOST': 'db.qara.kr',                      # Set to empty string for localhost. Not used with sqlite3.
            'PORT': '3306',                      # Set to empty string for default. Not used with sqlite3.
        },
        'readdb': {
            'ENGINE': 'django.db.backends.mysql',
            'NAME': 'mpb',
            'USER': 'root',
            'PASSWORD': '',
            'HOST': 'qara-slave.cqbv4t9ze5wf.ap-northeast-2.rds.amazonaws.com',
            'PORT': '3306',
        },
    }
"""
import os, sys, socket, json, pymysql, redis, sqlite3, re, time, logging, random, threading
import xlsxwriter
from DBUtils.PooledDB import PooledDB
from pymongo import MongoClient
from pymongo.read_preferences import ReadPreference
from werkzeug.contrib.cache import MemcachedCache
from datetime import datetime, date

from .util import encode_data, copy_dict, hanprint, fprint, Params, json_default, local_ip, AES256, str_slice, tokyo, DefaultParams, utc

try: # db 경로 별도 설정 확인
    from .constant import API_NAME, DB_JSON_PATH
    # from _constant import API_NAME, DB_JSON_PATH, DEV_LOCAL_IP # 이전버전 호환 방식
    # db.json 파일 위치는 _constant.py 에서 지정한다.
    api_is_dev = API_NAME.startswith('dev.') or API_NAME.startswith('dev_')
except:
    api_is_dev = False

try: # dev ip 지정하여 dev와의 차이를 둘 때
    from .constant import DEV_LOCAL_IP
    ip_is_dev = local_ip() in [DEV_LOCAL_IP] # dev server local ip
except:
    ip_is_dev = False

databases = []

# For local test by kevin
# DB_JSON_PATH = '/Users/hyeklee/workspace/qara/kosho/settings/db.json'

def is_dev():
    """
        현재 서버가 개발서버인지를 알려준다
    """
    return ip_is_dev or api_is_dev

def get_db_list():
    """
        현재 db.json의 내용을 반환한다.
    """
    return databases

def load_db_list(filename=None):
    """
        기본 혹은 지정된 db.json의 내용을 불러온다
    """
    global databases
    try:
        [databases.append(db) for db in json.load(open(filename or DB_JSON_PATH))]
    except Exception as e:
        fprint('load_db_list error :', e.__repr__())
        pass


def load_from_django():
    """
        from django.conf import settings 에서 DB값을 읽어온다
    """
    from django.conf import settings
    global databases
    
    try:
        for k, v in settings.DATABASES.items():
            type = v['ENGINE'].split('.')[-1] # django.db.backends.mysql
            db = Params(role=k, type=type, host=v['HOST'], user=v['USER'], password=v['PASSWORD'],  db=v['NAME'], desc=k+' from django settings.DATABASES')
            databases.append(db)
    except:
        pass

    
def check_mysql(dbinfo):
    keys = ['init_command', 'local_infile', 'conv', 'read_default_group', 'compress', 'sql_mode', 'unix_socket', 'db', 'ssl', 'read_default_file', 'user', 'host', 'auth_plugin_map', 'password', 'client_flag', 'autocommit', 'named_pipe', 'use_unicode', 'database', 'charset', 'no_delay', 'port', 'max_allowed_packet', 'passwd', 'defer_connect', 'cursorclass', 'connect_timeout']
    key_to_remove = list(set(dbinfo.keys()) - set(keys))
    [dbinfo.pop(key) for key in key_to_remove]
    dbinfo['port'] = dbinfo.get('port') or 3306
    return dbinfo

def get_db_info(role, idx=0):
    try:
        dbinfo = [x for x in databases if x.get('role') == role][idx]
        if dbinfo['type'].startswith('mysql'):
            return check_mysql(dbinfo)
        return dbinfo
    except:
        return None

def get_db(role, idx=0, local=False, pref=None, force_real=False):
    """
        db.json 의 값을 따라 적절한 db client를 반환한다.
        role이 동일할 경우, 보통 idx 번째의 db를 반환하지만 mysql_master, mysql_slave의 경우는 replica set을 만들어 반환한다.
        pref 는 db에 따라 달라진다.
            mongodb : ReadPreference(default ReadPreference.PRIMARY_PREFERRED)
    """
    try:
        dbinfo = [x for x in databases if x.get('role') == role][idx]
    except:
        print('not exist db role in databases :', role)
        return None
    dev_post = '_dev' if is_dev() and not force_real else '' # dev이면 uri 등에 _dev가 붙는다.
    
    def adjust_dev(dbinfo, dev_post, local):
        uri = 'localhost' if local else (dbinfo.get('uri'+dev_post) or dbinfo.get('uri'))
        db = dbinfo.get('db'+dev_post) or dbinfo.get('db')
        host = 'localhost' if local else (dbinfo.get('host'+dev_post) or dbinfo.get('host'))
        port = dbinfo.get('port'+dev_post) or dbinfo.get('port')
        user = dbinfo.get('user'+dev_post) or dbinfo.get('user')
        password = dbinfo.get('password'+dev_post) or dbinfo.get('password')
        replSet = dbinfo.get('replSet'+dev_post) or dbinfo.get('replSet')
        path = dbinfo.get('path'+dev_post) or dbinfo.get('path')
        file = dbinfo.get('file'+dev_post) or dbinfo.get('file')
        group = dbinfo.get('group'+dev_post) or dbinfo.get('group')
        return Params(uri=uri, db=db, host=host, port=port, user=user, password=password, replSet=replSet, path=path, file=file, group=group)
    
    p = adjust_dev(dbinfo, dev_post, local)
    
    if dbinfo['type'] == 'mongodb':
        return MongoClient(p.uri)[p.db]
    if dbinfo['type'] == 'mongodb_rs':
        if is_dev():
            return MongoClient(p.uri)[p.db]
        else:
            pref = pref or ReadPreference.PRIMARY_PREFERRED
            return MongoClient(p.uri, replicaSet=p.replSet, read_preference=pref)[p.db]
    if dbinfo['type'] == 'mysql':
        check_mysql(p)
        return Mysql(p)
    if dbinfo['type'] == 'mysql_master' or dbinfo['type'] == 'mysql_slave':
        # replset 구성
        master = [check_mysql(x) for x in databases if x.get('role') == role and x.get('type') == 'mysql_master'] # master db
        slave = [check_mysql(x) for x in databases if x.get('role') == role and x.get('type') == 'mysql_slave'] # slave db
        return Mysql(master, slave)
    if dbinfo['type'] == 'redis':
        return redis.Redis(host=p.host, port=p.port or 6379, db=p.db or 0)
    if dbinfo['type'] == 'redis_wrap':
        return Redis(host=p.host, port=p.port or 6379, db=p.db or 0)
    if dbinfo['type'] == 'memcached':
        return MemcachedCache(p.group)
    if dbinfo['type'] == 'sqlite' or dbinfo['type'] == 'sqlite3':
        return Sqlite(p.file)

def mongo_data_table(args, coll, projection=None, selection=None, skipCount=False, sort=True):
    """
        MongoDB Data Table 표준 검색형태
        검색어(search_query)에서 띄어쓰기는 and 를 의미한다. '가 라' -> '가나다라' '가거라' 등이 검색됨
        특정 컬럼을 타깃으로 검색할 수 있다. no:33 은 no컬럼에서만 검색한다.
    """
    search = []
    target_search = []
    if args.has_key('search_query') and args.get('search_query') != '' and args.get('search_field'):
        search_query = args['search_query']
        search_field = args['search_field'].split(',')

        # search_query를 정비한다.
        arr = re.findall(u"[\\w.]+:\\S+", search_query) # target 검색(field:value) 추출
        search_query = re.sub(u"[\\w.]+:\\S+", "", search_query).strip() # 추출 후 남은 부분
        for a in arr:
            col, val = a.split(':')
            #chk_num = re.findall('^num\\((\d+(.\d+)?)\\)$', val) # num(12345) 형식인지
            #if chk_num:
            #    val = float(chk_num[0][0])
            
            # 필요시 숫자, bool, 문자 모두 검색
            col_arr = [(col, val)]
            if val.isdigit():
                col_arr.append((col, float(val)))
            if val == 'true' or val == 'false':
                col_arr.append((col, val == 'true'))
            if len(col_arr) == 1:
                target_search.append(col_arr[0])
            else: # 숫자 또는 bool 입력으로 2개 이상
                target_search.append(('$or', [dict([pair]) for pair in col_arr]))

        search_query = re.sub('\\s+','.*?',search_query)
        # {field:{$regex:''}} 형태와 {field:re.compile(search_query)} 형태가 있는데 속도는 대동소이하다.
        # {$regex:'', $options:'i'} 이나 re.compile(search_query, re.IGNORECASE) 처럼 flag를 써서 대소문자를 무시할 수 있다. 이 경우 index를 태울 수 없다.
        if search_query:
            search.append({'$or':[dict([(k,re.compile(search_query, re.IGNORECASE))]) for k in search_field]})

    if selection:
        search.append(selection)
    if target_search:
        if len(target_search) > 1:
            search.append({'$and':[dict([pair]) for pair in target_search]})
        else:
            search.append(dict(target_search))

    # search 재조정
    if not search:
        search = {}
    elif len(search) == 1:
        search = search[0]
    else:
        search = {'$and':search}

    skip = int(args.get('iDisplayStart'))
    limit = int(args.get('iDisplayLength'))
    # [(sort_col, sort_dir)...] 정렬이 중첩되도 처리가 가능하게 변경
    sort_arr = []
    for sort_num in range(int(args.get('iColumns'))):  # field len
        iSortCol = args.get('iSortCol_%s' % sort_num)  # 정렬하려는 field num
        sSortDir = 1 if args.get('sSortDir_%s' % sort_num) == 'asc' else -1  # 정렬조건
        if not iSortCol: break
        sort_arr.append((args.get('mDataProp_%s' % iSortCol), sSortDir))

    cnt_all = coll.count() # 전체 갯수
    cursor = coll.find(search, projection).limit(limit).skip(skip)
    if sort and sort_arr: cursor = cursor.sort(sort_arr) # 정렬 필요시 정렬

    cnt = cnt_all if skipCount else cursor.count() # cursor counting 이 느리다면 그냥 전체 갯수로 대체한다.
    data_table = {'sEcho':args.get('sEcho'),
        'iTotalRecords':cnt_all,
        'iTotalDisplayRecords':cnt,
        'aaData':list(cursor)}
    return data_table

def mongo_field(coll, ratio=1.0, divide=5, min_cnt=0, max_cnt=0, with_stat=False, printout=False):
    """
        mongodb의 collection에 어떤 field들이 있는지 알려준다.
        document마다 field가 상이할 수 있으므로 전수검사를 해야 하지만
        데이터량이 너무 많은 경우 ratio, divide를 통해 양과 검사위치를 조절할 수 있다
        ratio = 0.3, divide = 3 인 경우 10% 만큼 3 조각을 검사하는데,
        처음과 끝은 항상 포함이므로 아래와 같이 검사하게 된다(* = 검사파트, # = 검사하지 않는 파트)
        **#######**#######**
        with_stat = True일 경우 아래와 같은 세부통계 형태로 반환한다.
        {'elapsed': 0.066,
         'fields': [u'key',
                    u'deleted',
                    u'_id',
                    u'size'],
         'total_count': 1493,
         'stat': {u'_id': {'avg_len': 24,
                           'count': 1493,
                           'dtcount': 1493,
                           'max_len': 24,
                           'ratio': 1.0,
                           'type': [('ObjectId', 1.0)]},
                  u'deleted': {'avg_len': 4,
                               'count': 189,
                               'dtcount': 2,
                               'max_len': 4,
                               'top5':[(True,180),(False,9)],
                               'ratio': 0.12659075686537175,
                               'type': [('bool', 1.0)]},
                  u'key': {'avg_len': 52,
                           'count': 1493,
                           'dtcount': 1493,
                           'max_len': 53,
                           'ratio': 1.0,
                           'type': [('unicode', 1.0)]},
                  u'size': {'avg_len': 4,
                            'count': 1493,
                            'dtcount': 1493,
                            'max_len': 6,
                            'ratio': 1.0,
                            'type': [('Int64', 0.9986604152712659),
                                     ('int', 0.0013395847287340924)]}}}
    """
    st = time.time()
    fields = {}
    cnt = coll.count()
    if cnt < divide:
        divide = 1
        ratio = 1.0
    target_cnt = cnt * ratio
    if min_cnt > 0 and target_cnt < min_cnt:
        if min_cnt > cnt:
            target_cnt = cnt
        else:
            target_cnt = min_cnt
    if max_cnt > 0 and target_cnt > max_cnt:
        target_cnt = max_cnt
    part_cnt = int(target_cnt / divide) # 검사할 한 덩어리의 수
    skip_cnt = 0 if divide == 1 else int(round(1.0 * (cnt - part_cnt*divide) / (divide-1)+0.5))
    doc_cnt = 0
    def add_count(item):
        k, v = item
        t = type(v).__name__
        group_key = str(v) if isinstance(v, (list, dict, tuple, set)) else v
        fields[k] = fields.get(k) or {'count':0, 'type':{}, 'group':{}, 'sum_of_len':0, 'max_len':0}
        fields[k]['count'] += 1
        fields[k]['group'][group_key] = (fields[k]['group'].get(group_key) or 0) + 1
        s = v.encode('utf8') if type(v) == unicode else str(v)
        fields[k]['sum_of_len'] += len(s)
        fields[k]['max_len'] = fields[k]['max_len'] if fields[k]['max_len'] > len(s) else len(s)
        fields[k]['type'][t] = (fields[k]['type'].get(t) or 0) + 1
    def set_field(f):
        fields[f] = True
    def sort_type_ratio(type_dict, total_cnt):
        type_list = type_dict.items()
        type_list.sort(lambda a,b:b[1]-a[1])
        return map(lambda a:(a[0],1.0*a[1]/total_cnt), type_list)
    if with_stat:
        for i in range(divide):
            cursor = coll.find().sort('_id', 1).skip((part_cnt+skip_cnt)*i).limit(part_cnt)
            # print 'find cnt : '+str(len(list(cursor)))
            for doc in cursor:
                map(add_count, doc.items())
                doc_cnt += 1
        stat = {}
        for k,v in fields.iteritems():
            top5 = v['group'].items()
            top5.sort(lambda a,b:b[1]-a[1])
            top5 = top5[:5] if len(top5) > 0 and top5[0][1] > 1 else []
            stat[k] = {'count':v['count'], 'dtcount':len(v['group']), 'top5':top5, 'ratio':1.0*v['count']/doc_cnt, 'avg_len':v['sum_of_len']/v['count'], 'max_len':v['max_len'], 'type':sort_type_ratio(v['type'], v['count'])}
        res = {
            'elapsed':round(time.time() - st,3),
            'fields':list(fields),
            'stat':stat,
            'total_count':cnt
        }
    else:
        for i in range(divide):
            cursor = coll.find().sort('_id', 1).skip((part_cnt+skip_cnt)*i).limit(part_cnt)
            for doc in cursor:
                map(set_field, doc.keys())
        if printout:
            print('elapsed :', round(time.time() - st,3))
        res = list(fields)
    if printout:
        hanprint(res)
    return res

def mongo_join(base, key, derived, fields, multi=False, outer=True):
    """
        mongodb 유사 join 구현
        base(collection의 결과 rows)에서 특정 key field를 다른 collection에서 찾은 뒤, 그 결과를 base쪽에 통합시킨다.
        base left outer join derived와 비슷한 개념이다.
        base에서의 key는 unique 해야 한다.
        multi가 true인 경우 derived의 결과는 2개 이상일 수 있으며 list로 반환된다.
        multi가 true일 때 derived 의 field가 string이 아니라 list 라면 해당 key들을 가져와서 dict를 만들어 넣는다.
        ex) {'user_name':['name','num']} -> {'user_name':{'name':'a','num':'b'}}(multi=False), {'user_name':[{},{}]}(multi=True)
        outer가 true인 경우 derived에 해당 key가 없어도 base가 유지되고, false인 경우 해당 key가 없으면 base에서도 없어진다.
        
        ex)
        base = [{'phone':'1234'},{'phone':'2345'}]
        derived = [{'num':'1234','name':'윤다은'},{'num':'2345','name':'Kim'},{'num':'2345','name':'Song'}]

        rows = mongo_join(base, {'phone':'num'}, derived, {'user_name':'name'})
        rows => [{'phone':'1234','user_name':'윤다은'},{'phone':'2345','user_name':'Kim'}]
        rows = mongo_join(base, {'phone':'num'}, derived, {'user':['phone','name']})
        rows => [{'phone':'1234','user':{'phone':1234,'name':'윤다은'}},{'phone':'2345','user':{'phone':'2345','name':'Kim'}}]

        rows = mongo_join(base, {'phone':'num'}, derived, {'user_name':'name'}, True)
        rows => [{'phone':'1234','user_name':['윤다은']},{'phone':'2345','user_name':['Kim','Song']}]
    """
    base_key = key.keys()[0] # join을 위한 base측 key
    derived_key = key.values()[0] # join을 위한 derived측 key
    field_arr = list(fields.iteritems())
    key_arr = [doc.get(base_key) for doc in base if doc.get(base_key) != None]
    derived_arr = filter(lambda row:row.get(derived_key) in key_arr, derived) if type(derived) == list else list(derived.find({derived_key:{"$in":key_arr}}))
    
    if multi:
        derived_map = {}
        for doc in derived_arr:
            derived_val = doc.get(derived_key)
            if derived_val != None:
                derived_map[derived_val] = derived_map.get(derived_val) or []
                derived_map[derived_val].append(doc)
    else:
        derived_map = dict([(doc.get(derived_key), doc) for doc in derived_arr if doc.get(derived_key) != None])

    new_base = []
    for d in base: # base에 derived의 요소를 끼워넣는다.
        doc = d.copy()
        base_val = doc.get(base_key) # key용 value. 위의 예제에서는 '1234' 등
        derived_doc = derived_map.get(base_val) # derived 측의 doc
        if base_val and derived_doc:
            for k, v in field_arr: # base 측의 doc에 derived의 요소들을 삽입
                if multi: # multi mode 라면
                    derived_val = [dict([(f, row.get(f)) for f in v]) if type(v) == list else row.get(v) for row in derived_doc]
                else:
                    derived_val = dict([(f, derived_doc.get(f)) for f in v]) if type(v) == list else derived_doc.get(v)
                doc[k] = derived_val
            new_base.append(doc)
        elif outer:
            new_base.append(doc)

    return new_base

def mongo_virtual_collection():
    """
        개념정립단계
        어떤 콜렉션 call_log가 있다고 할 때, 데이터가 많으면 단순검색도 오래 걸리고 삭제시 레플리카에 치명적인 딜레이를 초래한다.
        그래서 기본적으로 일정 기준으로 콜렉션을 나누고, 페이징시 순차적으로 검색하는 방식을 취한다
        기준은 일주월 등의 일자, 갯수 체크후 01 02 03 단순증가식 분리 등이 있다
        전체를 검색하는 쿼리는 레플리카일 경우 콜렉션을 분산요청하여 속도 향상이 가능하지만
        전체를 페이징하며 검색하는 쿼리는 검색결과가 적을 경우 좀 지저분해 질 수 있다
        선택적 update나 delete도 마찬가지이다
        
        이 방식에 적합한 콜렉션은 수정할 일이 없는 단순 로깅 데이터이다. 순차 페이징 외 다른 검색을 할 일이 없으면 더 좋다.
    """
    pass

class Mysql:
    def __init__(self, master=[], slave=[], master_pool=None, slave_pool=None, host='localhost', port=3306, user=None, password=None, db=None, debug=False, **kwargs):
        """
            master, slave 정보를 를 받아서 connection pool로 관리하고 각 쿼리를 실행한다.
            
            :param master[list of dict] host, user, password, db 로 구성된 dict의 list 혹은 dict. multi master일 경우 관련 설정이 되어있어야 한다(auto increment 등)
            :param slave[list of dict] host, user, password, db 로 구성된 dict의 list 혹은 dict.
        """
        self.debug = debug
        
        # pool list로 init 한 경우
        if master_pool or slave_pool:
            self.master_pool = master_pool or {}
            self.slave_pool = slave_pool or {}
            return
        
        if user: # user가 들어온 경우 master로 취급한다
            self.master = [dict(host=host, port=port, user=user, password=password, db=db, **kwargs)]
            self.slave = []
        else:
            self.master = [master] if isinstance(master, dict) else master
            self.slave = [slave] if isinstance(slave, dict) else slave
        
        master_pool = {}
        for m in self.master:
            key = '%s@%s:%s/%s' % (m["user"], m["host"], m.get("port") or 3306, m["db"])
            m['charset'] = m.get("charset", "utf8")
            pool = PooledDB(pymysql, 1, 75, **m)
            conn = pool.connection() # connection이 2번 맺어진다. 하나는 대기용인듯 하며 한 thread당 하나씩이 대기용으로 존재한다. 한 thread에서 5개의 connection을 맺으면 6개가 db상에서 보이게 된다. db의 max_connection에 주의.
            # ping 측정
            st = time.time()
            conn.ping()
            ping = time.time() - st
            master_pool[key] = {'pool':pool, 'ping':ping}
        self.master_pool = master_pool

        slave_pool = {}
        for s in self.slave:
            key = '%s@%s:%s/%s' % (s["user"], s["host"], s.get("port") or 3306, s["db"])
            s['charset'] = s.get("charset", "utf8")
            pool = PooledDB(pymysql, 1, 75, **s)
            conn = pool.connection()
            # ping 측정
            st = time.time()
            conn.ping()
            ping = time.time() - st
            slave_pool[key] = {'pool':pool, 'ping':ping}
        self.slave_pool = slave_pool
        
        # select_by_col, tsafe insert 등을 위한 table column 정보
        self.col_info = {}
        
        """ old setting
        self.dbinfo = dbinfo
        self.transaction = False # transaction 중인지 여부
        self.connection = pymysql.connect(dbinfo["host"], dbinfo["user"], dbinfo["password"], dbinfo["db"], charset=dbinfo.get("charset", "utf8"))
        self.cs = self.connection.cursor() # select 결과물이 list 형태
        self.dict_cs = self.connection.cursor(pymysql.cursors.DictCursor) # select 결과물이 dict 형태
        self.mod_count = 0 # transaction중 select 외의 query 가 사용된 횟수
        self.lock = Lock()
        """
    
    
    ### decorators 
    
    def _reconnect(self):
        """
            decorator, 접속이 끊겼을 때 재연결한다. @staticmethod 는 decorator에 쓸 수 없다.
        """
        f = self # trick for eclipse error message
        def wrap(self, *args, **kwargs):
            try:
                return f(self, *args, **kwargs)
            except (pymysql.err.OperationalError, pymysql.err.InternalError) as e:
                # (2006, "MySQL server has gone away (error(32, 'Broken pipe'))")
                # (2013, 'Lost connection to MySQL server during query')
                # (2014, 'Command Out of Sync')
                # (1927, 'Connection was killed')
                # pymysql.err.InternalError: Packet sequence number wrong - got 0 expected 1 -> db restart
                # pymysql.err.InternalError: (1054, u"Unknown column 'a' in 'field list'") -> skip
                # print e[0]
                # 재연결시 transaction이 존재시 exception을 일으킨다.
                if getattr(self, 'trx_conn', None):
                    raise
                if e[0] not in (2006, 2013, 2014, 1927): # connection 문제가 아닌 경우
                    #print e[0], dir(e), e.args, e.message
                    raise
                self.__init__(master_pool=self.master_pool, slave_pool=self.slave_pool)
                return f(self, *args, **kwargs)
        return wrap
    
    def _try(self):
        """
            decorator, 에러 발생시 쿼리를 출력하고 롤백한다
        """
        f = self # trick for eclipse error message
        def wrap(self, *args, **kwargs):
            try:
                return f(self, *args, **kwargs)
            except Exception as e:
                # 스택의 끝으로 이동.. 하지 않고 현재 프레임의 바로 아래로 이동한다.
                t, o, s = sys.exc_info()
                current_frame = sys._getframe()
                es = s
                while es.tb_next:
                    if es.tb_frame == current_frame:
                        es = es.tb_next
                        #while es.tb_frame.f_code.co_name == 'wrap' or es.tb_frame.f_code.co_name[0] == '_': # 기타 decorator skip
                        #    if es:
                        #        print es.tb_frame.f_code.co_name
                        #    es = es.tb_next
                        break
                    es = es.tb_next
                func_name = es.tb_frame.f_code.co_name
                locals = es.tb_frame.f_locals
                if es.tb_next:
                    args = es.tb_next.tb_frame.f_locals.get('args') # PoolDB 때문인지 execute(es.tb_next.tb_frame.f_code.co_name)는 tough_method로 나온다
                else:
                    args = None
                conn = locals.get('conn')
                # conn은 있어야 한다
                if conn and args and len(args) <= 2:
                    not conn.transaction and locals.get('commit') and conn.rollback()
                    sql = conn.cs.mogrify(*args)
                    print('##### error in Mysql.{} :\n{}\n#####'.format(func_name, sql))
                else:
                    print('##### Mysql._try : error in %s, %s, %s, conn: %s, locals: %s' % (func_name, args, kwargs, conn, locals))
                raise
        return wrap
    
    def _enc(self):
        """
            지정한 컬럼들을 지정한 key로 encrypt한다.
            
            query, query_insert, insert_by_col == insert_many, update_by_col, update_many, iou_by_col 에 해당한다. 
            필요시 query_rows, query_row, query_ones, query_one, select_by_col 에도 사용 가능
        """
        f = self # trick for eclipse error message
        def wrap(self, *args, **kwargs):
            enc_col = kwargs.pop('enc_col', None)
            enc_key = kwargs.pop('enc_key', None)
            params = args[1] if len(args) >= 2 else kwargs.get('params')
            
            if enc_col and enc_key and params:
                nparams = self.encrypt_params(params, enc_col, enc_key)
                if len(args) >= 2:
                    arg_list = list(args)
                    arg_list[1] = nparams
                    args = tuple(arg_list)
                else:
                    kwargs['params'] = nparams
            return f(self, *args, **kwargs)
        return wrap
        
    @staticmethod
    def encrypt_params(params, enc_col, enc_key):
        """
            주어진 parameter를 지정한 key로 encrypt한다.
            
            params가 {'a':1,'b':'c'} 나 [{}] 같은 dict 라면 enc_col은 'a,b' 혹은 ['a','b'] 이어야 하고
            ['as','df'] 나 ('ef','gh') 같은 list, tuple 이라면 enc_col은 [1] 처럼 인덱스값 이어야 한다.
        """
        if enc_col and enc_key and params:
            aes = AES256(key=enc_key)
            if isinstance(enc_col, basestring):
                enc_col = map(lambda a:a.strip(), enc_col.split(','))
            is_dict = False
            if isinstance(params, dict):
                params = [params]
                is_dict = True
            nparams = []
            for i, param in enumerate(params):
                if isinstance(param, dict):
                    nparam = dict([(k, aes.enc(v)) if k in enc_col else (k, v) for k, v in param.items()])
                    nparams.append(nparam)
                else:
                    nparams.append(aes.enc(param) if i in enc_col else param)
            nparams = nparams[0] if is_dict else nparams
            return nparams
        return params
    
    def _dec(self):
        """
            지정한 컬럼들을 지정한 key로 decrypt한다.
            
            query_rows, query_row, query_ones, query_one, select_by_col, objs, obj 에 해당한다. 
        """
        f = self # trick for eclipse error message
        def wrap(self, *args, **kwargs):
            dec_col = kwargs.pop('dec_col', None)
            key = kwargs.pop('dec_key', kwargs.pop('enc_key', None))
            res = f(self, *args, **kwargs)
            return self.decrypt_result(res, dec_col, key)
        return wrap
    
    @staticmethod
    def decrypt_result(res, dec_col, dec_key):
        """
            주어진 result를 지정한 key로 decrypt한다.
            
            result가 {'a':1,'b':'c'} 나 [{}] 같은 dict 라면 dec_col은 'a,b' 혹은 ['a','b'] 이어야 하고
            ['as','df'] 나 'ef' 같은 one, ones 의 결과물 이라면 enc_col은 [0] 이어야 한다.
        """
        if dec_col and dec_key and res:
            aes = AES256(key=dec_key)
            if isinstance(dec_col, basestring):
                dec_col = map(lambda a:a.strip(), dec_col.split(','))
            if isinstance(res, dict):
                nres = dict([(k, aes.dec(v)) if k in dec_col else (k, v) for k, v in res.items()])
                if type(res) == Params:
                    nres = Params(nres)
                return nres
            if isinstance(res, list):
                nres = []
                for row in res:
                    if len(dec_col) == 1 and dec_col[0] == 0: # query_ones 등 단일 값의 list인 경우
                        nrow = aes.dec(row)
                    elif isinstance(row, dict):
                        nrow = dict([(k, aes.dec(v)) if k in dec_col else (k, v) for k, v in row.items()])
                        if type(row) == Params:
                            nrow = Params(nrow)
                    else:
                        nrow = row
                    nres.append(nrow)
                return nres
            if dec_col[0] == 0: # 단일 값을 반환하는 query_one 등
                return aes.dec(res)
        return res
    
    
    ### server select
    
    def _master(self, conn=None, nearest=True):
        if not conn:
            if nearest:
                conn = sorted(self.master_pool.values(), lambda a,b:int((a['ping']-b['ping'])*100))[0]['pool'].connection()
            else:
                conn = random.choice(self.master_pool.values())['pool'].connection()
        if not conn:
            raise Exception('Mysql pool has no master')
        
        conn.transaction = False
        conn.mod_count = 0
        conn.cs = conn.cursor()
        conn.dict_cs = conn.cursor(pymysql.cursors.DictCursor)
        return conn
        
    def _slave(self, conn=None, nearest=True):
        if self.slave_pool:
            if not conn:
                if nearest:
                    conn = sorted(self.slave_pool.values(), lambda a,b:int((a['ping']-b['ping'])*100))[0]['pool'].connection()
                else:
                    conn = random.choice(self.slave_pool.values())['pool'].connection()
            conn.transaction = False
            conn.cs = conn.cursor()
            conn.dict_cs = conn.cursor(pymysql.cursors.DictCursor)
            return conn
        return self._master()

    def _auto(self, sql='select'):
        if self.is_select(sql):
            return self._slave()
        return self._master()
    
    def _nearest(self, sql='select'):
        """
            ping 값이 가장 낮은 db를 반환한다.
            한 지역에 db가 하나일 때 유효하며, 2개 이상일 때는 roundrobin을 하도록 다시 수정해야 한다.
        """
        if self.is_select(sql):
            return self._slave(sorted(self.master_pool.values() + self.slave_pool.values(), lambda a,b:int((a['ping']-b['ping'])*100))[0]['pool'].connection())
        return self._master(sorted(self.master_pool.values(), lambda a,b:int((a['ping']-b['ping'])*100))[0]['pool'].connection())
    
    
    ### db를 용도에 따라 직접 지정하는 경우
    
    def with_master(self, nearest=True):
        if nearest:
            pool = dict([sorted(self.master_pool.items(), lambda a,b:int((a[1]['ping']-b[1]['ping'])*100))[0]])
        else:
            pool = dict([random.choice(self.master_pool.items())])
        return Mysql(master_pool=pool)
        
    def with_slave(self, nearest=True):
        if nearest:
            pool = dict([sorted(self.slave_pool.items(), lambda a,b:int((a[1]['ping']-b[1]['ping'])*100))[0]])
        else:
            pool = dict([random.choice(self.slave_pool.items())])
        return Mysql(slave_pool=pool)
    
    def with_nearest(self, sql='select'):
        if self.is_select(sql):
            pool = dict([sorted(self.master_pool.items() + self.slave_pool.items(), lambda a,b:int((a[1]['ping']-b[1]['ping'])*100))[0]])
            return Mysql(slave_pool=pool)
        return self.with_master()
    
    @staticmethod
    def is_select(sql):
        cmd = re.sub('"""[\s\S]*?"""|#[\s\S].*?\n', '', sql).strip().lower()[:6]
        return cmd == 'select' or cmd[:5] == 'show ' or cmd[:5] == 'desc '
    
    @staticmethod
    def split_list(A, n=1000):
        """
            insert나 update를 무한정 이을 수는 없기에 데이터를 한 번에 넣을 사이즈로 쪼갤 때 사용한다
            for chunk in split_list([..], 30):
                arr..
        """
        return [A[i:i+n] for i in range(0, len(A), n)]
    
    ### 연속적인 작업을 위해 connection이나 cursor를 고정시켜두는 경우
    
    def get_connection(self, master=False):
        """
            커넥션을 하나 반환한다. 커넥션에는 cs와 dict_cs 가 세팅되어있다. master가 필요할 경우 명시적으로 알려줘야 한다.
        """
        if master:
            return self._master()
        return self._nearest()
        
    def get_cursor(self, master=False):
        """
            dict 커서를 하나 반환한다. master가 필요할 경우 명시적으로 알려줘야 한다.
        """
        if master:
            return self._master().dict_cs
        return self._nearest().dict_cs
    
    
    ### 쿼리를 직접 만들어 넣을 때
    
    @_reconnect
    @_enc
    @_try
    def query(self, query, params=None, commit=True, conn=None): # params는 list, tuple, dict, Params, one object 등이 가능하다
        """ CRUD중 CUD 를 수행하고 영항을 받은 row의 수를 반환한다 """
        conn = conn or getattr(self, 'trx_conn', None) or self._nearest(query)
        if self.debug: hanprint(conn.cs.mogrify(query, params))
        conn.cs.execute(query, params)
        not conn.transaction and commit and conn.commit()
        if conn.transaction:
            conn.mod_count += 1
        return conn.cs.rowcount

    @_reconnect
    @_enc
    @_try
    def query_insert(self, query, params=None, commit=True, conn=None): # params는 list, tuple, dict, Params, one object 등이 가능하다
        """ insert 문을 수행하고 생성된 rowid를 반환한다. multiple insert의 경우 가장 처음 생성된 rowid가 반환된다 """
        conn = conn or getattr(self, 'trx_conn', None) or self._master()
        if self.debug: hanprint(conn.cs.mogrify(query, params))
        conn.cs.execute(query, params)
        not conn.transaction and commit and conn.commit()
        if conn.transaction:
            conn.mod_count += 1
        return conn.cs.lastrowid

    @_reconnect
    @_enc
    @_dec
    @_try
    def query_rows(self, query, params=None, commit=True, conn=None, with_cols=False):
        """
            list of dict를 반환한다. 여러 row를 가져오려 할 때 사용한다.
            query_rows("select id, name from user") -> [{'id':1, 'name':'Tom'}, {'id':2, 'name':'John'}]
        """
        conn = conn or getattr(self, 'trx_conn', None) or self._nearest(query)
        if self.debug: hanprint(conn.cs.mogrify(query, params))
        conn.dict_cs.execute(query, params)
        # execute 때문인지, mariadb innodb에서 select만 해도 transaction이 열린다. commit 해주자.
        not conn.transaction and commit and conn.commit()
        if with_cols:
            return conn.dict_cs.fetchall(), [c[0] for c in conn.dict_cs.description]
        return conn.dict_cs.fetchall()

    @_reconnect
    @_enc
    @_dec
    @_try
    def query_row(self, query, params=None, commit=True, conn=None, with_cols=False):
        """
            dict를 반환한다. 하나의 row만 가져오려 할 때 사용한다.
            query_row("select id, name from user limit 1") -> {'id':1, 'name':'Tom'}
        """
        conn = conn or getattr(self, 'trx_conn', None) or self._nearest(query)
        if self.debug: hanprint(conn.cs.mogrify(query, params))
        conn.dict_cs.execute(query, params)
        not conn.transaction and commit and conn.commit()
        if with_cols:
            return conn.dict_cs.fetchone(), [c[0] for c in conn.dict_cs.description]
        return conn.dict_cs.fetchone()
    
    @_reconnect
    @_enc
    @_dec
    @_try
    def query_ones(self, query, params=None, commit=True, conn=None):
        """
            여러 row의 첫 col 만을 list 로 돌려준다.
            query_ones("select id from user where id in (1,2,3)") -> [1,2,3]
        """
        conn = conn or getattr(self, 'trx_conn', None) or self._nearest(query)
        if self.debug: hanprint(conn.cs.mogrify(query, params))
        conn.cs.execute(query, params)
        not conn.transaction and commit and conn.commit()
        return [row[0] for row in conn.cs.fetchall()]

    @_reconnect
    @_enc
    @_dec
    @_try
    def query_one(self, query, params=None, commit=True, conn=None):
        """
            하나의 결과만을 가져오려 할 때 사용한다.
            query_one("select max(id) from user") -> 32
        """
        conn = conn or getattr(self, 'trx_conn', None) or self._nearest(query)
        if self.debug: hanprint(conn.cs.mogrify(query, params))
        conn.cs.execute(query, params)
        not conn.transaction and commit and conn.commit()
        result = conn.cs.fetchone()
        return result[0] if result else None
    
    @_reconnect
    @_try
    def query_many(self, query, params=None, commit=True, conn=None): # 미완, 동작안함
        """
            동일한 쿼리를 반복해서 실행시킬 때, 한 번의 호출만으로 처리할 때 쓰인다.
            
            query_many("insert into tbl(a,b) values (%s, %s)", [(1,2), (3,4)]) -> [{'a':1, 'b':2}, {'a':3, 'b':4}]
        """
        conn = conn or getattr(self, 'trx_conn', None) or self._nearest(query)
        if self.debug: hanprint(conn.cs.mogrify(query, params))
        conn.dict_cs.executemany(query, params)
        
        not conn.transaction and commit and conn.commit()
        if conn.transaction:
            conn.mod_count += 1
        return conn.cs.rowcount
    
    
    ### dict 호환 객체로 쿼리를 자동 조합할 때
    
    @_reconnect
    @_enc
    @_dec
    def select_by_col(self, table, params={}, cols=None, projection='*', desc=True, commit=True, conn=None):
        """
            dict 형태의 데이터를 where절에 사용하여 결과를 가져온다.
            cols가 없고 desc가 True라면 table의 desc를 가져와서 params와 비교하여 실재 존재하는 col 들만 where에 사용한다.
            이 경우 cache가 없다면 2회의 질의가 발생하므로 가능한한 cols를 넣어주는 편이 좋다.
            # select_by_col('tb', {'col_a':'!!', 'col_b':'@@'}, 'col_a,col_b')
            # 'select * from tb where col_a = %s and col_b = %s', (params['col_a'], params['col_b']) 와 같은 역할
        """
        conn = conn or getattr(self, 'trx_conn', None) or self._nearest()
        if params:
            if not cols: # params에서 실제로 존재하는 field만 걸러낸다
                if desc:
                    if not self.col_info.get(table):
                        fields = [row['Field'] for row in self.params("desc "+table)]
                        self.col_info[table] = ','.join(fields)
                    else:
                        fields = self.col_info.get(table).split(',')
                    cols_list = list(set(fields).intersection(set(params.keys())))
                else:
                    cols_list = params.keys() # cols=None, desc=False인 경우는 params의 키를 전부 사용하는 경우
            else:
                if isinstance(cols, basestring):
                    cols_list = cols.split(",")
            q, p = "select "+projection+" from "+table+" where " + " and ".join(["`"+col+"` = %s" for col in cols_list]), [params[k] for k in cols_list]
            if self.debug: hanprint(conn.cs.mogrify(q, p))
            conn.dict_cs.execute(q, p)
        else:
            q = "select "+projection+" from "+table
            if self.debug: hanprint(conn.cs.mogrify(q))
            conn.dict_cs.execute(q)
        not conn.transaction and commit and conn.commit()
        return conn.dict_cs.fetchall()
    
    @_reconnect
    @_dec
    def objs(self, table, **kwargs):
        """
            django의 model 형태에 가깝게 쿼리를 받아서 하나의 테이블에서 결과를 가져온다.
        """
        conn = getattr(self, 'trx_conn', None) or self._nearest()
        items = kwargs.items()
        where = " where " + " and ".join(["`"+k+"` = %s" for k, v in items]) if items else ''
        params = [v for k, v in items] if items else None
        
        cls = Params
        if isinstance(table, type): # class를 준 경우
            q = "select * from "+table.__name__+where
            if self.debug: hanprint(conn.cs.mogrify(q, params))
            conn.dict_cs.execute(q, params)
            cls = table
        else:
            q = "select * from "+table+where
            if self.debug: hanprint(conn.cs.mogrify(q, params))
            conn.dict_cs.execute(q, params)
        not conn.transaction and conn.commit()
        return [cls(row) for row in conn.dict_cs.fetchall()]
    
    @_reconnect
    @_dec
    def obj(self, table, **kwargs):
        """
            django의 model 형태에 가깝게 쿼리를 받아서 하나의 테이블에서 하나의 결과만 가져온다.
        """
        conn = getattr(self, 'trx_conn', None) or self._nearest()
        items = kwargs.items()
        where = " where " + " and ".join(["`"+k+"` = %s" for k, v in items]) if items else ''
        params = [v for k, v in items] if items else None
        
        cls = Params
        if isinstance(table, type): # class를 준 경우
            q = "select * from "+table.__name__+where
            if self.debug: hanprint(conn.cs.mogrify(q, params))
            conn.dict_cs.execute(q, params)
            cls = table
        else:
            q = "select * from "+table+where
            if self.debug: hanprint(conn.cs.mogrify(q, params))
            conn.dict_cs.execute(q, params)
        not conn.transaction and conn.commit()
        row = conn.dict_cs.fetchone()
        return cls(row) if row else None

    @_reconnect
    @_enc
    @_try
    def insert_by_col(self, table, params=[], cols=None, commit=True, conn=None): # params 의 key와 db의 column name 이 일치해야 한다.
        """
            dict or list of dict 형태의 데이터들을 바로 insert 후 가장 처음 생성된 rowid를 반환한다
            # insert_many('tb', [{'col_a':'!!', 'col_b':'@@'},{...}], 'col_a,col_b')
            # 'insert into tb(col_a, col_b) values (%s, %s),(%s, %s)..', (params[0]['col_a'], params[0]['col_b'], params[1]['col_a'], params[1]['col_b'], ..) 와 같은 역할
            insert_many와 통합되었다. params는 dict, list 둘 다 가능하다
        """
        if not params: # 빈 list가 온 경우
            return None
        if isinstance(params, dict):
            params = [params]
        if not isinstance(params, (list, tuple)):
            raise Exception("insert_by_col's params must be dict or list of dict")
        conn = conn or getattr(self, 'trx_conn', None) or self._master()
        params = encode_data(params)

        if not cols:
            cols_list = params[0].keys() # cols=None인 경우는 params[0]의 키를 전부 사용하는 경우
        else:
            if isinstance(cols, basestring):
                cols_list = cols.split(",")
        cols = ','.join(map(lambda c:'`'+c+'`', cols_list))
        
        for arr in self.split_list(params):
            q, p = "insert into "+table+"("+cols+") values "+",".join(["("+",".join(["%s"]*len(cols_list))+")"] * len(arr)), reduce(lambda a,b:a+b, [[p.get(k, None) for k in cols_list] for p in arr])
            if self.debug: hanprint(conn.cs.mogrify(q, p))
            conn.cs.execute(q, p)
        not conn.transaction and commit and conn.commit()
        if conn.transaction:
            conn.mod_count += 1
        return conn.cs.lastrowid

    @_reconnect
    def safe_insert(self, table, params={}, commit=True, conn=None):
        """
            db의 column name을 가져와서 해당 key만 insert 시킨다.
            
            params는 dict, list 둘 다 가능하다
        """
        if not self.col_info.get(table):
            fields = [row['Field'] for row in self.params("desc "+table)]
            self.col_info[table] = ','.join(fields)
        #cols_list = list(set(fields).intersection(set(params.keys())))
        
        return self.insert_by_col(table, params, self.col_info[table], commit, conn)
        
    @_reconnect
    @_enc
    @_try
    def update_by_col(self, table, params={}, cols=None, where=None, where_vals=[], commit=True, conn=None): # params 의 key와 db의 column name 이 일치해야 한다.
        """
            dict 형태의 데이터를 바로 update, target 지정을 위해 where문이 필요하다. 업데이트된 row 수를 반환한다
            # update_by_col('tb', {'col_a':'!!', 'col_b':'@@'}, 'col_a,col_b', where='id = %s', where_vals=[33])
            # 'update tb set col_a = %s, col_b = %s where id = %s', (params['col_a'], params['col_b'], 33) 와 같은 역할
        """
        conn = conn or getattr(self, 'trx_conn', None) or self._master()
        params = encode_data(params)
        if not cols:
            cols_list = params.keys() # cols=None인 경우는 params의 키를 전부 사용하는 경우
        else:
            if isinstance(cols, basestring):
                cols_list = cols.split(",")
        
        q, p = "update "+table+" set "+",".join(["`"+col+"` = %s" for col in cols_list])+(" where " + where if where else ""), [params[k] for k in cols_list] + list(where_vals)
        if self.debug: hanprint(conn.cs.mogrify(q, p))
        conn.cs.execute(q, p)
        not conn.transaction and commit and conn.commit()
        if conn.transaction:
            conn.mod_count += 1
        return conn.cs.rowcount
    
    @_reconnect
    def safe_update(self, table, params={}, cols=None, where=None, where_vals=[], commit=True, conn=None):
        """
            db의 column name을 가져와서 해당 key만 update 시킨다
        """
        if not self.col_info.get(table):
            fields = [row['Field'] for row in self.params("desc "+table)]
            self.col_info[table] = ','.join(fields)
        else:
            fields = self.col_info.get(table).split(',')
        cols_list = list(set(fields).intersection(set(params.keys())))

        return self.update_by_col(table, params, cols or ','.join(cols_list), where, where_vals, commit, conn)
    
    @_reconnect
    @_enc
    @_try
    def update_many(self, table, params=[], cols=None, where_cols=None, commit=True, conn=None):
        """
            list of dict 형태의 데이터들을 한 번에 update 한다
            # update_many('tb', [{'col_a':'!!', 'col_b':'@@'},{...}], 'col_a,col_b', where_cols='col_a')
            
            sb_etf_price 기준 20000개 정도는 버티지만 한계를 넘으면 2006 server has gone away error(32, 'Broken pipe') 가 발생한다. -> 1000개씩 쪼개기로 하여 문제는 없다
        """
        if not params: # 빈 list가 온 경우
            return None
        if isinstance(params, dict):
            params = [params]
        if not isinstance(params, (list, tuple)):
            raise Exception("update_many's params must be dict or list of dict")
        conn = conn or getattr(self, 'trx_conn', None) or self._master()
        params = encode_data(params)
        
        if not cols:
            cols_list = params[0].keys() # cols=None인 경우는 params[0]의 키를 전부 사용하는 경우
        else:
            if isinstance(cols, basestring):
                cols_list = cols.split(",")
        
        cols = ','.join(map(lambda c:'`'+c+'`', cols_list))
        where_cols_list = where_cols.split(",")
        
        def escape_value(v):
            if isinstance(v, (basestring, dict, tuple, list)):
                return str(v).replace("'", "\\'")
            if isinstance(v, (datetime, date)):
                return utc(v).strftime('%Y-%m-%d %H:%M:%S')
            return v
        
        for arr in self.split_list(params):
            # 가상 테이블을 union으로 만든다. 첫 row에만 이름이 필요하다.
            virtual = 'select ' + ','.join([("'{}' ".format(escape_value(arr[0][k])) if arr[0][k] != None else 'null ')+"{}".format(k) for k in cols_list]) + ''.join([' union all select ' + ','.join(["'{}' ".format(escape_value(p[k])) if p[k] != None else 'null ' for k in cols_list]) for p in arr[1:]])
            q = "update "+table+" a join (" + virtual + ") v on " + " and ".join(["a.`"+col+"` = v.`"+col+"`" for col in where_cols_list]) + " set "+",".join(["a.`"+col+"` = v.`"+col+"`" for col in cols_list])
            if self.debug: hanprint(conn.cs.mogrify(q))
            conn.cs.execute(q)
        not conn.transaction and commit and conn.commit()
        if conn.transaction:
            conn.mod_count += 1
        return conn.cs.rowcount


    @_reconnect
    @_enc
    @_try
    def iou_by_col(self, table, params={}, cols=None, where_cols=None, commit=True, conn=None):
        """
            params 의 key와 db의 column name 이 일치해야 한다.
            insert or update, 다수 업데이트도 가능하다
            update된 경우 rowcount, insert된 경우 insert id를 반환한다
            # iou_by_col('tb', {'col_a':'!!', 'col_b':'@@', 'id':'id', 'no':1}, 'col_a,col_b', where_cols='id,no')
        """
        conn = conn or getattr(self, 'trx_conn', None) or self._master()
        params = encode_data(params)
        
        if not cols:
            cols_list = params.keys() # cols=None인 경우는 params의 키를 전부 사용하는 경우
        else:
            if isinstance(cols, basestring):
                cols_list = cols.split(",")
        
        if isinstance(where_cols, basestring):
            where_cols_list = where_cols.split(",")
        else:
            where_cols_list = where_cols
        
        params_list = [params[k] for k in cols_list]
        where_params_list = [params[k] for k in where_cols_list]
        
        q, p = "select 1 from "+table+" where "+" and ".join(["`"+col+"` = %s" for col in where_cols_list])+" limit 1", where_params_list
        if self.debug: hanprint(conn.cs.mogrify(q, p))
        conn.cs.execute(q, p)
        if conn.cs.fetchone(): # row가 있다면
            q, p = "update "+table+" set "+",".join(["`"+col+"` = %s" for col in cols_list])+(" where " + " and ".join(["`"+col+"` = %s" for col in where_cols_list])), params_list + where_params_list
            if self.debug: hanprint(conn.cs.mogrify(q, p))
            conn.cs.execute(q, p)
            res = conn.cs.rowcount
        else:
            res = self.insert_by_col(table, params, cols, commit, conn=conn)
        not conn.transaction and commit and conn.commit()
        if conn.transaction:
            conn.mod_count += 1
        return res
    

    ### Params로 결과를 받을 때
    
    def query_params(self, *args, **kwargs):
        return [Params(row) for row in self.query_rows(*args, **kwargs)]
    def query_param(self, *args, **kwargs):
        row = self.query_row(*args, **kwargs)
        return Params(row) if row else None

        
    ### shortcuts
    select = select_by_col
    insert = insert_many = insert_by_col
    update = update_by_col
    iou = iou_by_col
    param = query_param
    params = query_params
    one = query_one
    ones = query_ones
    many = query_many
    

    ### 쿼리의 결과를 바로 출력할 때
    
    def print_rows(self, query, params=None, commit=True, conn=None):
        hanprint(self.query_rows(query, params=params, commit=commit, conn=conn))
    def print_row(self, query, params=None, commit=True, conn=None):
        hanprint(self.query_row(query, params=params, commit=commit, conn=conn))
    def print_one(self, query, params=None, commit=True, conn=None):
        hanprint(self.query_one(query, params=params, commit=commit, conn=conn))
    def table(self, query, params=None, ellipsis=30, limit=100, commit=True, conn=None, encoding='euc-kr'):
        """
            쿼리 결과를 테이블 형태로 출력한다
            col 사이에는 최소 2칸의 공백을 둔다.
            ellipsis: column data가 너무 길 경우의 제한
            limit: row 수가 많을 경우의 제한
        """
        if ' ' not in query: # table명만 주어졌다고 가정, limit 20을 적용한다
            query = "select * from %s limit 20" % query
            
        conn = conn or getattr(self, 'trx_conn', None) or self._nearest(query)
        if self.debug: hanprint(conn.cs.mogrify(query, params))
        conn.cs.execute(query, params)
        fields = [f[0][:ellipsis-2]+'..' if len(f[0]) > ellipsis else f[0] for f in conn.cs.description] # [(u'id', 3, None, 11, 11, 0, False), (u'comp_id', 3, None, 11, 11, 0, True)] # name, type_code, display_size, internal_size, precision, scale, nullok, flags?
        max_len = [len(f) for f in fields]
        
        rows = [fields]
        for n in range(limit):
            row = conn.cs.fetchone()
            if not row:
                break
            try:
                row = [str(v) for v in encode_data(row, encoding=encoding)] # utf8은 3byte, euckr은 2byte인데 실제 한글 출력은 2칸이므로 euckr 기준으로 진행
            except:
                return self.table(query, params=None, ellipsis=30, limit=100, commit=True, conn=None, encoding='utf8')
            
            rows.append(row)
            for i, col in enumerate(row):
                if len(col) > ellipsis:
                    try:
                        uni = (col[:ellipsis-2]+'..').decode(encoding) # 한글처리 주의
                    except UnicodeDecodeError as e:
                        try:
                            uni = (col[:ellipsis-3]+'...').decode(encoding)
                        except UnicodeDecodeError as e:
                            uni = (col[:ellipsis-4]+'...').decode(encoding)
                    row[i] = uni.encode(encoding)
                    max_len[i] = ellipsis
                else:
                    if len(col) > max_len[i]:
                        max_len[i] = len(col)
        
        for row in rows:
            print(''.join(map(
                lambda a: # 좌측정렬, 최대길이에 맞춘 스트링 표현식에 실제 데이터를 잘라서 넣는다
                    ('%-'+str(max_len[a[0]])+'s  ') % a[1][:max_len[a[0]]],
                enumerate(row))).decode(encoding).encode(sys.stdout.encoding))

        not conn.transaction and commit and conn.commit()
    
    
    ### Excel 파일을 만들 때
    
    def query2xls(self, query, params=None, commit=True, conn=None, limit=None, file=None):
        """
            query를 받아서 엑셀로 바꿔서 반환한다
            필드명이나 데이터의 길이에 맞게 길이를 조절한다. 1자에 1사이즈라고 보면 된다. 최대 길이는 50으로 한다
            @args query[str] 실행할 query
            @args params[list|tuple|object] query 안의 %s 에 적용할 파라미터들
            @args commit[bool] transaction 중일때 commit 할지의 여부
            @args conn[Connection] 현재 db의 커넥션이 아닌 별도의 커넥션을 사용할 때
            @args file[str|IO] 저장할 파일명 혹은 파일객체나 IO를 받아온다. 지정되지 않았다면 workbook을 닫지 않고 반환한다
        """
        import datetime as dt
        conn = conn or getattr(self, 'trx_conn', None) or self._nearest(query)
        if self.debug: hanprint(conn.cs.mogrify(query, params))
        cnt = conn.cs.execute(query, params)
        not conn.transaction and commit and conn.commit()
        
        # Create an new Excel file and add a worksheet.
        wb = xlsxwriter.Workbook(file)
        ws = wb.add_worksheet()
        
        # 스타일 프리셋 추가
        bold = wb.add_format({'bold': True})
        format_d = wb.add_format({'num_format': 'yyyy.mm.dd'})
        format_dt = wb.add_format({'num_format': 'yyyy.mm.dd hh:mm:ss'})
        def write_data(worksheet, y, x, data):
            if type(data) == dt.datetime:
                if data.tzinfo:
                    worksheet.write_datetime(y, x, utc(data, 'kr').replace(tzinfo=None), format_dt)
                else:
                    worksheet.write_datetime(y, x, data, format_dt)
            elif isinstance(data, dt.date):
                worksheet.write_datetime(y, x, data, format_d)
            else:
                worksheet.write(y, x, data)
        
        max_len = []
        for i, col in enumerate(conn.cs.description): # [(u'id', 3, None, 11, 11, 0, False), (u'comp_id', 3, None, 11, 11, 0, True)] # name, type_code, display_size, internal_size, precision, scale, nullok, flags?
            ws.write(0, i, col[0], bold)
            max_len.append(len(col[0]))
        
        # 데이터 부분 작성
        for y in range(limit or cnt):
            row = conn.cs.fetchone()
            if not row:
                break
            for x, col in enumerate(row):
                write_data(ws, y + 1, x, col)
                euc = unicode(col).encode('euckr')
                if len(euc) > max_len[x]:
                    max_len[x] = len(euc)
        
        # max_len에 맞춰서 사이즈를 조절한다
        for i, size in enumerate(max_len):
            ws.set_column(chr(65+i)+':'+chr(65+i), 50 if size > 50 else size if size > 6 else 6)
            
        if file: # 주어진 file에 바로 저장하는 경우
            wb.close()
            if hasattr(file, 'seek'): # 파일 객체라면 되돌린다
                file.seek(0)
            return file
        
        return wb # 닫지 않고 반환한다
    
    
    ### db 현황 파악용
    
    def count(self, table):
        """
            해당 테이블의 전체 row 수
        """
        return self.query_one('select count(1) from '+table)
    
    def count_all(self, sort='name'):
        """
            모든 테이블의 전체 row 수
        """
        from collections import OrderedDict
        cnts = [(t, self.query_one('select count(1) from '+t)) for t in [row.values()[0] for row in self.query_rows('show tables')]]
        if sort == 'cnt':
            return OrderedDict(sorted(cnts, lambda a,b:-a[1]+b[1]))
        return OrderedDict(sorted(cnts))
    
    
    @_reconnect
    def query_multi(self, queries, commit=True, conn=None, enc_col=None, enc_key=None, dec_col=None, dec_key=None):
        """
            쿼리 일괄 실행
            
            네트워크 관련 성능의 향상을 위해 쿼리를 일괄 실행시킨다.
            query_type은 one, row = param, ones, insert, query 등이 있다.
            query 일 경우는 rowcount를, insert 일 경우는 lastrowid를 반환한다.
            # todo : db가 2대 이상이라면 query에 따라 master 와 nearest 등을 분리해서 실행시키되, 트랜잭션의 연속성 및 replication lag 을 고려하고, 여러 slave로의 분산처리의 타당성(ping 등)을 고려한다.
            
            enc_col 은 [0,1] 형태일 수밖에 없다. 여러개의 쿼리를 날릴 때 일괄 적용되므로 순서에 주의해야 한다.
            dec_col 은 one, ones일 경우 [0] 이며 그 외에는 'a,b' 혹은 ['a','b'] 형태이다.
            
            query_multi([
                ('one', "select count(*) from unity",),
                ("select max(id) max_id from user",),
                ('insert', "insert into user(name) values (%s)", ('SSH',)),
                ('param', "select * from user where id = %s", (2,)),
                ('query', "insert into user(name) select nm from unity"),
            ])
            >>> [320, [{"max_id":452}], 453, {'id':2, 'name':'SSH'}, 320]
        """
        conn = conn or getattr(self, 'trx_conn', None) or self._master()

        mogrified = []
        for q in queries:
            if q[0] in ('one', 'row', 'param', 'ones', 'query', 'insert'):
                if enc_col and enc_key and len(q) >= 3: # 암호화할 param이 있는 경우
                    args = list(q[1:])
                    args[1] = self.encrypt_params(args[1], enc_col, enc_key)
                else:
                    args = q[1:]
                mogrified.append(conn.dict_cs.mogrify(*args))
            else:
                if enc_col and enc_key and len(q) >= 2: # 암호화할 param이 있는 경우
                    q = list(q)
                    q[1] = self.encrypt_params(q[1], enc_col, enc_key)
                mogrified.append(conn.dict_cs.mogrify(*q))
        
        conn.dict_cs.execute(';'.join(mogrified))
        
        result = []
        mod_count = 0
        for q in queries:
            if q[0] == 'one':
                k = conn.dict_cs.description[0][0]
                res = conn.dict_cs.fetchone()[k]
            elif q[0] == 'ones':
                k = conn.dict_cs.description[0][0]
                res = [row[k] for row in conn.dict_cs.fetchall()]
            elif q[0] in ('row', 'param'):
                res = Params(conn.dict_cs.fetchone())
            elif q[0] == 'query':
                result.append(c.dict_cs.rowcount) # query(insert, update, delete) 일 때 필요
                mod_count += 1
                conn.dict_cs.nextset()
                continue
            elif q[0] == 'insert':
                result.append(c.dict_cs.lastrowid) # insert 일 때 필요
                mod_count += 1
                conn.dict_cs.nextset()
                continue
            else:
                res = [Params(row) for row in conn.dict_cs.fetchall()]
            
            if dec_col and dec_key and res != None: # 복호화할 result가 있는 경우
                res = self.decrypt_result(res, dec_col, dec_key)
            result.append(res)
            conn.dict_cs.nextset()
        
        not conn.transaction and commit and conn.commit()
        if conn.transaction:
            conn.mod_count += mod_count
        return result if result else None
    multi = query_multi
    """
        테스트 결과
        
        근거리
        >>> st = time.time()
        >>> for i in range(100):
        ...     res = db.api.multi([['select count(*) from user'], ['select count(*) from unity'], ['select 3']])
        ... 
        >>> print 'multi elapsed :', time.time()-st
        multi elapsed : 0.147565841675
        >>> 
        >>> st = time.time()
        >>> for i in range(100):
        ...     res = []
        ...     res.append(db.api.params('select count(*) from user'))
        ...     res.append(db.api.params('select count(*) from unity'))
        ...     res.append(db.api.params('select 3'))
        ... 
        >>> print 'single elapsed :', time.time()-st
        single elapsed : 0.499867916107

        
        
        원거리
        >>> st = time.time()
        >>> for i in range(100):
        ...     res = db.api.multi([['select count(*) from user'], ['select count(*) from unity'], ['select 3']])
        ... 
        >>> print 'multi elapsed :', time.time()-st
        multi elapsed : 1.17539691925
        >>> 
        >>> st = time.time()
        >>> for i in range(100):
        ...     res = []
        ...     res.append(db.api.params('select count(*) from user'))
        ...     res.append(db.api.params('select count(*) from unity'))
        ...     res.append(db.api.params('select 3'))
        ... 
        >>> print 'single elapsed :', time.time()-st
        single elapsed : 4.81271195412

    """
    
    
    ### datatables.js 호환 json result 조합
    
    @_reconnect
    def data_table(self, args, from_, select="*", where=None, groupby=None, params=None, skipCount=False, commit=True, conn=None, enc_col=None, enc_key=None, dec_col=None, dec_key=None):
        """
            MariaDB Data Table 표준 검색형태

            ex) data_table(request.args, 'table t', 'name, max(age) age', 'age >= %s', 'name', (19,), True, True)
            띄어쓰기는 and 를 의미한다. '가 라' -> '가나다라' '가거라' 등이 검색됨
            특정 컬럼을 타깃으로 검색할 수 있다. no:33 은 no컬럼에서만 검색한다.
            
            params는 list type이고, 여기에 추가되는 search_query는 search_field를 key로 하는 형식이라 enc_col은 index와 key 타입이 공존해야 한다.
            그러나 암호화 대상을 잘 고려해보면 부분검색을 하는 경우가 대부분이므로, 전체가 일치해야만 검색이 되는 enc_col 절차와는 잘 맞지 않다.
            그렇지만 no:33 처럼 타겟 쿼리는 암호화 해줘야 한다.
            
            컬럼을 타깃으로 전체 일치 검색을 하는 경우는 결국 dec_col에 실리는 컬럼에 포함된다고 볼 수 있다.
            그래서 no:33 같은 타겟 쿼리는 dec_col을 참조하도록 한다.
            그래서 결국 enc_col 은 [0, 1] 같은 index 형식이며
            dec_col은 key 형식이고 'a,b' 이나 ['a','b'] 둘 다 가능하다.
        """
        search = '1=1'
        target_search = ''
        target_val = []
        aes = None
        if enc_col and enc_key or dec_col and dec_key:
            aes = AES256(key=enc_key or dec_key)
        
        if select == None:
            select = '*'
        if args.get('search_query', '') != '' and not args.get('search_field'):
            # search_query 가 주어졌는데 field가 없는 경우 필터링 없는 전체 결과를 줄 경우 오해할 수 있으므로 빈 결과 반환
            search = '1=0'
            search_query = ''
            search_field = []
        elif args.has_key('search_query') and args.get('search_query') != '':
            search_query = args['search_query']
            search_field = args['search_field'].split(',')
            
            # search_query를 정비한다.
            arr = re.findall(u"[\\w.]+:\\S+", search_query) # target 검색(field:value) 추출
            search_query = re.sub(u"[\\w.]+:\\S+", "", search_query).strip() # 추출 후 남은 부분
            for a in arr:
                col, val = a.split(':')
                # like 검색
                #target_val.append('%'+val.replace('%','\\%').replace('_','\\_').encode('utf8')+'%')
                #target_search += col+" like %s escape '\\\\' and "
                
                # 정확한 검색
                if aes and col in dec_col: # 암호화 대상 컬럼이라면
                    val = aes.enc(val)
                    print(val)
                target_val.append(val)
                col_name = '.'.join(map(lambda a:"`"+a+"`", col.split('.'))) # s.nm, mpb.unity.id 등의 형태에 대비
                target_search += col_name+" = %s and "
                # break # 하나만 인정된다.
            
            if search_query: # target 검색 외에 검색어 존재시
                search_query = re.sub('\\s+','%',search_query.replace('%','\\%').replace('_','\\_')) # like 용 특수기호 %, _ escape
                search = '('+' or '.join([k+" like %s escape '\\\\'" for k in search_field])+')'
                search_query = search_query.encode('utf8')
            else:
                search_field = []
        else:
            search_query = ''
            search_field = []
        skip = int(args.get('iDisplayStart') or 0)
        limit = int(args.get('iDisplayLength') or 15)
        
        if enc_col and params: # 암호화 대상 param이 있다면
            params = [aes.enc(p) if i in enc_col else p for i, p in enumerate(params)]

        # [(sort_col, sort_dir)...] 정렬이 중첩되도 처리가 가능하게 변경
        sort_arr = []
        for sort_num in range(int(args.get('iSortingCols') or 1)):  # 정렬할 column 수
            iSortCol = args.get('iSortCol_%s' % sort_num)  # 정렬하려는 field num
            sSortDir = args.get('sSortDir_%s' % sort_num)  # 정렬조건
            if not iSortCol: break
            col_name = args.get('mDataProp_%s' % iSortCol)
            if '(' not in col_name:
                col_name = '.'.join(map(lambda a:"`"+a+"`", col_name.split('.'))) # s.nm, mpb.unity.id 등의 형태에 대비
            #fprint('sort col', args.get('mDataProp_%s' % iSortCol), iSortCol, sort_num, col_name)
            sort_arr.append('%s %s' % (col_name, sSortDir))
        where_group_for_cnt = " where "+(where+" and " if where else "")+"1=1"+(" group by "+groupby if groupby else '')
        where_group = " where "+(where+" and " if where else "")+target_search+search+(" group by "+groupby if groupby else '')
        #fprint(select, sort_arr)
        
        query = "select "+select+" from "+from_+where_group+(" order by "+','.join(sort_arr) if sort_arr else '')+" limit %s,%s"
        query_cnt = "select ifnull(convert("+("count" if groupby else "sum")+"(cnt), signed), 0) from (select count(*) cnt from "+from_+where_group+") a"
        cnt_all = self.query_one("select ifnull(convert("+("count" if groupby else "sum")+"(cnt), signed), 0) from (select count(*) cnt from "+from_+where_group_for_cnt+") a", tuple(params or ())[select.count('%s'):], conn=conn)
        #print query, (tuple(params or ()))+tuple(target_val)+("%"+search_query+"%",)*len(search_field)+(skip,limit)

        try: # no:33 처럼 지시검색어 때문에 1052 column is ambiguous와 1054 unknown column이 발생할 수 있다.
            # count(cnt) 는 int가 아니라 decimal.Decimal을 반환하므로 convert를 하든지 int()를 하든지 해야한다.
            data = self.query_rows(query, (tuple(params or ()))+tuple(target_val)+("%"+search_query+"%",)*len(search_field)+(skip,limit), conn=conn)
            # print query, (tuple(params or ()))+tuple(target_val)+("%"+search_query+"%",)*len(search_field)+(skip,limit)
            cnt = cnt_all if skipCount or target_search+search == '1=1' else self.query_one(query_cnt, tuple(params or ())[select.count('%s'):]+tuple(target_val)+("%"+search_query+"%",)*len(search_field), conn=conn)
            #cnt = cnt_all # cnt가 느릴 경우 이걸 사용
        except Exception as e:
            if e.args[0] in [1052, 1054]:
                print(query)
            raise
        
        if dec_col and dec_key and data: # 복호화할 data가 있는 경우
            data = self.decrypt_result(data, dec_col, dec_key)
        
        data_table = {'sEcho':args.get('sEcho'),
            'iTotalRecords':cnt_all,
            'iTotalDisplayRecords':cnt,
            'aaData':data}
        return data_table
    
    def show_tables(self, name=None, *args, **kwargs):
        """
            table 정보를 보여준다. master db를 기준으로 한다.
            
              {
                "ENGINE":"InnoDB", 
                "ROW_FORMAT":"Compact", 
                "UPDATE_TIME":None, 
                "CHECK_TIME":None, 
                "INDEX_LENGTH":16384, 
                "TABLE_COLLATION":"utf8_general_ci", 
                "CREATE_TIME":datetime.datetime(2016, 9, 17, 13, 32, 19), 
                "TABLE_NAME":"admin", 
                "TABLE_ROWS":4, 
                "TABLE_TYPE":"BASE TABLE", 
                "TABLE_SCHEMA":"autocall", 
                "MAX_DATA_LENGTH":0, 
                "DATA_LENGTH":16384, 
                "CREATE_OPTIONS":"", 
                "AVG_ROW_LENGTH":4096, 
                "DATA_FREE":0, 
                "VERSION":10, 
                "AUTO_INCREMENT":None, 
                "TABLE_CATALOG":"def", 
                "TABLE_COMMENT":"관리자", 
                "CHECKSUM":None
              }
        """
        where = '1=1'
        if name:
            where += " and TABLE_NAME = '%s'" % name
        if args or kwargs:
            where = ' and '.join(list(args) + ["`%s` = '%s'" % (k,v) for k,v in kwargs.items()])
        return self.with_master().params("SELECT * FROM information_schema.TABLES WHERE table_schema = %s and "+where, (self.master[0]['db'],))
    
    
    
    ### Transaction 시작, 종료
    
    def start_transaction(self):
        """
            Transaction 시작
            
            master db를 받아오며, transaction 이 끝나면 폐기하고 원래 db를 써야 한다.
            다중 transaction에 대해 count를 증가시키며 commit 혹은 rollback 한다
            rollback이 일어났을 때 trx_count 가 남아있다면 chlid 이고 child는 parent의 상황을 모르므로 전부 rollback 하게되고 parent의 commit 대기분도 날라가게 된다. 이 때는 raise 등으로 알려야 한다. 그렇지 않고 혹여 parent가 commit을 한다면 에러난 child의 에러 직전까지의 내용이 rollback되지 않고 commit 되어버린다.
            자신이 child일지 아닐지 모르는 상황(모듈 등)이라면 항상 raise를 사용하도록 한다. 항상 parent라면 적절한 처리를 거치면 된다.
            
            trx = db.api.start_transaction()
            try:
                trx.commit()
            except:
                trx.rollback()
                raise
            db.api.query(...)
        """
        if getattr(self, 'trx_conn', None) and self.trx_conn.transaction:
            db = self
            db.trx_conn.trx_count += 1
        else:
            db = self.with_master()
            db.trx_conn = db._master()
            db.trx_conn.transaction = True
            db.trx_conn.trx_count = 1
        return db
        
    def commit(self):
        self.trx_conn.trx_count -= 1
        if self.trx_conn.trx_count == 0:
            self.trx_conn.commit()
            self.trx_conn.transaction = False
            self.trx_conn = None

    def rollback(self):
        self.trx_conn.trx_count -= 1
        if self.trx_conn.trx_count == 0:
            self.trx_conn.rollback()
            self.trx_conn.transaction = False
            self.trx_conn = None
    
    def trx(self, f, db_name='api'):
        """
            트랜잭션 간소화
            
            아래의 로직을 트랜잭션을 위해
            db.api.insert()
            db.api.insert()
            
            아래처럼 바꾸던 것을
            trx = db.api.start_transaction()
            try:
                trx.insert() # db.api.insert()
                trx.insert() # db.api.insert()
                trx.commit()
            except:
                trx.rollback()
                raise
            db.api.query(...)
            
            아래처럼 바꿀 수 있다.
            def _t(db):
                db.api.insert()
                db.api.insert()
            db.api.trx(_t, 'api')
        """
        db = DefaultParams()
        if not db_name:
            db_name = 'default'
        db[db_name] = self.start_transaction()
        try:
            res = f(db)
            db[db_name].commit()
            return res
        except Exception as e:
            db[db_name].rollback()
            raise
    with_trx = trx
    
    
    
    ### magic methods
    
    def __del__(self):
        try:
            if getattr(self, 'trx_conn', None):
                self.trx_conn.cs.close()
                self.trx_conn.dict_cs.close()
                self.trx_conn.close()
            for pool in (self.master_pool.values() + self.slave_pool.values()):
                pool['pool'].close()
        except AttributeError as e:
            print('##### Mysql.__del__ raises', e.__repr__())

    def __str__(self):
        s = []
        if self.master_pool:
            s.append('master:['+', '.join(self.master_pool.keys())+']')
        if self.slave_pool:
            s.append('slave:['+', '.join(self.slave_pool.keys())+']')
        return '<Mysql of %s>' % ', '.join(s)
        #return '<Mysql %s@%s/%s (%s, %s) #%s>' % (self.dbinfo['user'], self.dbinfo['host'], self.dbinfo['db'], self.connection.server_version, self.connection.encoding, self.connection.server_thread_id[0])
    __repr__ = __str__
    
    def __enter__(self):
        return self
        
    def __exit__(self, exc_type, exc_val, exc_tb):
        pass
        
    def _test(self):
        self.query('drop table if exists temp_tbl')
        self.query('create table temp_tbl(a int primary key auto_increment)')
        print(self.query_insert('insert into temp_tbl values (1),(2)'))
        print(self.insert_many('temp_tbl', [{'a':3},{'a':4}]))
        print('sleep...', time.sleep(3))
        print(self.query_rows('select * from temp_tbl where a > %s', (2,)))
        print(self.update_by_col('temp_tbl', {'a':0}, where='a = 1'))
        print(self.iou_by_col('temp_tbl', {'a':10}, where_cols='a'))
        print('sleep...', time.sleep(3))
        print(self.query_row("select group_concat(a separator ',') vals from temp_tbl"))
        print(self.query_one("select count(*) from temp_tbl"))
        with self.start_transaction() as db:
            db.query_insert('insert into temp_tbl values(5)')
            db.rollback()
        print('sleep...', time.sleep(3))
        try:
            st = time.time()
            print(self.with_master().query_row("select group_concat(a separator ',') vals from temp_tbl"), time.time() - st)
            st = time.time()
            print(self.with_slave().query_row("select group_concat(a separator ',') vals from temp_tbl"), time.time() - st)
            st = time.time()
            print(self.with_nearest().query_row("select group_concat(a separator ',') vals from temp_tbl"), time.time() - st)
        except Exception as e:
            print('except', e)
        self.query('drop table if exists temp_tbl')




def mongo_metadata(conn, server_name, db_name, coll_name):
    """
        가능한한 많은 정보를 모아서 mongodb collection의 metadata를 작성한다.
    """
    meta = {
        'type':'collection',
        'server':server_name,
        'db':db_name,
        'name':coll_name,
        'text':coll_name,
        'desc':'',
        'show':True,
        'columns':[],
        'order':[],
        'sort':[],
        'search':[]
    }
    stat = mongo_field(conn[db_name].get_collection(coll_name), ratio=0.3, min_cnt=100, max_cnt=1000, with_stat=True)
    if stat['fields']:
        indexes = [idx_info['key'][0][0] for idx, idx_info in conn[db_name].get_collection(coll_name).index_information().items()]
        for col, col_info in stat['stat'].iteritems():
            if stat['total_count'] > 100 and col_info['ratio'] < 0.02: # 출현율 2% 미만은 제외한다.
                continue
            fulltype = 'NoneType'
            for t in col_info['type']:
                if t[0] != 'NoneType':
                    fulltype = t[0]
                    break
            sample = None
            for t in col_info['top5']:
                if t[0] != None:
                    sample = t[0]
                    break
            column = {
                'name':col,
                'text':col,
                'type':field_type_conv(fulltype, sample),
                'fulltype':fulltype,
                'desc':'',
                'sample':sample,
                'sample_max_len':col_info['max_len'],
                'has_index':col in indexes
            }
            meta['columns'].append(column)
            meta['order'].append(col)
            if col in indexes:
                meta['sort'].append(col)
                meta['search'].append(col)
        if '_id' in meta['order']:
            meta['order'].remove('_id')
            meta['order'].insert(0,'_id') # id를 맨 앞으로
    return meta

def mysql_metadata(conn, server_name, db_name, table_name, tbl_comment=''):
    """
        가능한한 많은 정보를 모아서 mysql table의 metadata를 작성한다.
    """
    cols = conn.query_rows("""
        SELECT table_schema, table_name, ordinal_position, column_name, data_type
             , column_type, column_key, extra, column_default, is_nullable, column_comment 
          FROM information_schema.columns
         WHERE table_schema NOT IN ('mysql', 'information_schema', 'performance_schema')
           AND table_schema = %s AND table_name = %s
         ORDER BY ordinal_position
    """, (db_name, table_name))
    indexes = map(lambda a:a['column_name'], conn.query_rows("""
        SELECT DISTINCT column_name
          FROM information_schema.statistics
         WHERE table_schema = %s
           AND table_name = %s
           AND seq_in_index = 1
    """, (db_name, table_name))) # 1순위 인덱스 컬럼명만 가져오기
    meta = {
        'type':'table',
        'server':server_name,
        'db':db_name,
        'name':table_name,
        'text':table_name,
        'desc':tbl_comment,
        'show':True,
        'columns':[],
        'order':[c['column_name'] for c in cols],
        'sort':[],
        'search':[]
    }
    cnt = 5 # conn.query_one("select count(1) from "+db_name+"."+table_name) # 속도가 느리다면 제거
    sample_row = conn.query_row("select * from "+db_name+"."+table_name+" limit "+str(cnt/2)+", 1") # 가운데서 하나
    for col in cols:
        if sample_row:
            sample_col = sample_row.get(col['column_name'], '')
            if type(sample_col) == date: # mongodb에 date type은 넣을 수 없다.
                sample_col = datetime.combine(sample_col, datetime.min.time()).replace(tzinfo=tokyo)
        else:
            sample_col = None
        column = {
            'name':col['column_name'],
            'text':col['column_name'],
            'type':field_type_conv(col['data_type'], db='mysql'),
            'fulltype':col['column_type'],
            'desc':col['column_comment'],
            'sample':sample_col,
            'sample_max_len':len(json.dumps(sample_col, default=json_default)) if sample_col else 0,
            'has_index':col['column_name'] in indexes
        }
        meta['columns'].append(column)
        if col['column_name'] in indexes:
            meta['sort'].append(col['column_name'])
            meta['search'].append(col['column_name'])
    return meta

def sqlite_metadata(db, table_name):
    """
        가능한한 많은 정보를 모아서 sqlite table의 metadata를 작성한다.
    """
    sample_row, cols = db.row("select * from "+table_name+" limit 1", with_cols=True)
    indexes = map(lambda a:str_slice(a['sql'], 'on '+table_name+'\\(', '\\)').split(',')[0].strip(), db.show_master(type='index', tbl_name=table_name))
    meta = {
        'type':'table',
        'server':'',
        'db':'',
        'name':table_name,
        'text':table_name,
        'desc':'',
        'show':True,
        'columns':[],
        'order':cols,
        'sort':[],
        'search':[]
    }
    for col in cols:
        if sample_row:
            sample_col = sample_row.get(col, '')
        else:
            sample_col = None
        column = {
            'name':col,
            'text':col,
            'type':'str',
            'fulltype':'str',
            'desc':'',
            'sample':sample_col,
            'sample_max_len':len(json.dumps(sample_col, default=json_default)) if sample_col else 0,
            'has_index':col in indexes
        }
        meta['columns'].append(column)
        if col in indexes:
            meta['sort'].append(col)
            meta['search'].append(col)
    return meta

def field_type_conv(t, data=None, db=None):
    """
        mongodb, mysql의 type명들을 python에 가까운 것으로 바꾼다
        null, str, json, oid, int, datetime, float, bool, stat, kst
    """
    m = {
        'NoneType':'null', 'unicode':'str', 'dict':'json', 'list':'json', 'ObjectId':'oid', 'Int64':'int', 'Decimal':'float',
        'timestamp':'datetime', 'varchar':'str', 'enum':'str', 'tinyint':'int', 'bigint':'int',
        'double':'float', 'char':'str', 'text':'str', 'mediumtext':'str', 'date':'datetime'
    }
    if t == 'dict' and data: # stat이 될지 판단한다.
        try:
            d = ast.literal_eval(data)
            if type(d) == dict and all(map(lambda a:a in ['group','strrange','numrange','count','necount','nullcount','dtcount','dtgroup','sum','avg','min','max'] or a[:8] in ['groupsum','groupavg','groupmin','groupmax'], d.keys())): # 모든 key가 stat용이라면
                return 'stat'
        except:
            print('except', data)
            return 'json'
    if (t == 'timestamp' or t == 'datetime' or t == 'date') and db and 'mysql' in db: # mysql인지 판단한다. utc라면 datetime, kst라면 kst
        return 'datetime'
    return m.get(t, t)






class Sqlite:
    def __init__(self, file=None):
        if file:
            self.file = file
            #self.connect(file)
        self.pool = {}
    
    def _try(self):
        """
            decorator, 에러 발생시 쿼리를 출력하고 롤백한다
        """
        f = self # trick for eclipse error message
        def wrap(self, *args, **kwargs):
            try:
                return f(self, *args, **kwargs)
            except Exception as e:
                # 스택의 끝으로 이동.. 하지 않고 현재 프레임의 바로 아래로 이동한다.
                t, o, es = sys.exc_info()
                current_frame = sys._getframe()
                while es.tb_next:
                    if es.tb_frame == current_frame:
                        break
                    es = es.tb_next
                locals = es.tb_frame.f_locals # 현재 함수인 wrap 자신
                func_name = es.tb_next.tb_frame.f_code.co_name
                if func_name in ('rows','row','one'):
                    print('##### error in Sqlite.{} :\n{}\n#####'.format(func_name, locals['args'][0].replace('?','%s') % locals['args'][1] if len(locals['args']) > 1 else locals['args'][0]))
                else:
                    print('##### error in Sqlite.{} :\n{}\n#####'.format(func_name, locals['args']))
                raise
        return wrap
    
    def _thread_exclusive(self):
        """
            sqlite의 경우 다른 thread에서의 이용이 금지되어있다.
            다른 thread가 요청하는 경우 새로운 객체를 만들어 준다.
        """
        f = self # trick for eclipse error message
        def wrap(self, *args, **kwargs):
            t_id = threading.current_thread().ident
            if not self.pool.get(t_id):
                conn = sqlite3.connect(self.file)
                cur = conn.cursor()
                self.pool[t_id] = Params(conn=conn, cur=cur)
            return f(self, *args, **kwargs)
        return wrap
    
    @staticmethod
    def dictfetch(cursor, one=False): # https://docs.djangoproject.com/es/1.9/topics/db/sql/#executing-custom-sql-directly
        """Return all rows from a cursor as a dict"""
        columns = [col[0] for col in cursor.description]
        if one:
            row = cursor.fetchone()
            if row:
                return dict(zip(columns, row))
            else:
                return row
        return [dict(zip(columns, row)) for row in cursor.fetchall()]
    
    def connect(self, file):
        self.conn = sqlite3.connect(file)
        self.cur = self.conn.cursor()
    
    @_thread_exclusive
    @_try
    def rows(self, query, params=None, with_cols=False):
        t_id = threading.current_thread().ident
        cc = self.pool[t_id]
        
        if params:
            cc.cur.execute(query, params)
        else:
            cc.cur.execute(query)
        if with_cols:
            return self.dictfetch(cc.cur), [c[0] for c in cc.cur.description]
        return self.dictfetch(cc.cur)
    
    @_thread_exclusive
    @_try
    def row(self, query, params=None, with_cols=False):
        t_id = threading.current_thread().ident
        cc = self.pool[t_id]
        
        if params:
            cc.cur.execute(query, params)
        else:
            cc.cur.execute(query)
        if with_cols:
            return self.dictfetch(cc.cur, True), [c[0] for c in cc.cur.description]
        return self.dictfetch(cc.cur, True)
    
    @_thread_exclusive
    @_try
    def one(self, query, params=None):
        t_id = threading.current_thread().ident
        cc = self.pool[t_id]
        
        if params:
            cc.cur.execute(query, params)
        else:
            cc.cur.execute(query)
        return cc.cur.fetchone()[0]
    
    def show_master(self, *args, **kwargs):
        where = ''
        if args or kwargs:
            where = ' where ' + ' and '.join(list(args) + ["`%s` = '%s'" % (k,v) for k,v in kwargs.items()])
        return self.rows("select * from sqlite_master"+where)
    
    def show_tables(self, name=None, *args, **kwargs):
        """
          {
            "TABLE_NAME":"audit_list", 
            "tbl_name":"audit_list", 
            "sql":"CREATE TABLE...", 
            "type":"table", 
            "name":"audit_list", 
            "rootpage":2
          }
        """
        where = '1=1'
        if name:
            where += " and tbl_name = '%s'" % name
        if args or kwargs:
            where = ' and '.join(list(args) + ["`%s` = '%s'" % (k,v) for k,v in kwargs.items()])
        return self.rows("select tbl_name TABLE_NAME, m.* from sqlite_master m where type = 'table' and "+where)
    
    def params(self, *args, **kwargs):
        return [Params(row) for row in self.rows(*args, **kwargs)]
    
    def param(self, *args, **kwargs):
        row = self.row(*args, **kwargs)
        return Params(row) if row else None
    
    def data_table(self, args, from_, select="*", where=None, groupby=None, params=None, skipCount=False, commit=True):
        """
            MariaDB Data Table 표준 검색형태

            ex) data_table(request.args, 'table t', 'name, max(age) age', 'age >= %s', 'name', (19,), True, True)
            띄어쓰기는 and 를 의미한다. '가 라' -> '가나다라' '가거라' 등이 검색됨
            특정 컬럼을 타깃으로 검색할 수 있다. no:33 은 no컬럼에서만 검색한다.
            
            params는 list type이고, 여기에 추가되는 search_query는 search_field를 key로 하는 형식이라 enc_col은 index와 key 타입이 공존해야 한다.
            그러나 암호화 대상을 잘 고려해보면 부분검색을 하는 경우가 대부분이므로, 전체가 일치해야만 검색이 되는 enc_col 절차와는 잘 맞지 않다.
            그렇지만 no:33 처럼 타겟 쿼리는 암호화 해줘야 한다.
            그래서 결국 enc_col 은 [0, 1, 'name', 'address'] 같은 형식으로 받아야 한다.
            dec_col은 'a,b' 이나 ['a','b'] 둘 다 가능하다.
        """
        search = '1=1'
        target_search = ''
        target_val = []
        
        if select == None:
            select = '*'
        if args.has_key('search_query') and args.get('search_query') != '':
            search_query = args['search_query']
            search_field = args['search_field'].split(',')

            # search_query를 정비한다.
            arr = re.findall(u"[\\w.]+:\\S+", search_query) # target 검색(field:value) 추출
            search_query = re.sub(u"[\\w.]+:\\S+", "", search_query).strip() # 추출 후 남은 부분
            for a in arr:
                col, val = a.split(':')
                # like 검색
                #target_val.append('%'+val.replace('%','\\%').replace('_','\\_').encode('utf8')+'%')
                #target_search += col+" like ? escape '\\\\' and "
                # 정확한 검색
                target_val.append(val)
                target_search += "`"+col+"` = ? and "
                # break # 하나만 인정된다.
            
            if search_query: # target 검색 외에 검색어 존재시
                search_query = re.sub('\\s+','%',search_query.replace('%','\\%').replace('_','\\_')) # like 용 특수기호 %, _ escape
                search = '('+' or '.join([k+" like ? escape '\\'" for k in search_field])+')'
                #search_query = search_query.encode('utf8')
            else:
                search_field = []
        else:
            search_query = ''
            search_field = []
        skip = int(args.get('iDisplayStart') or 0)
        limit = int(args.get('iDisplayLength') or 15)

        # [(sort_col, sort_dir)...] 정렬이 중첩되도 처리가 가능하게 변경
        sort_arr = []
        for sort_num in range(int(args.get('iColumns') or 1)):  # field len
            iSortCol = args.get('iSortCol_%s' % sort_num)  # 정렬하려는 field num
            sSortDir = args.get('sSortDir_%s' % sort_num)  # 정렬조건
            if not iSortCol: break
            sort_arr.append('%s %s' % (args.get('mDataProp_%s' % iSortCol), sSortDir))
        where_group_for_cnt = " where "+(where+" and " if where else "")+"1=1"+(" group by "+groupby if groupby else '')
        where_group = " where "+(where+" and " if where else "")+target_search+search+(" group by "+groupby if groupby else '')
        #fprint(select, sort_arr)
        
        query = "select "+select+" from "+from_+where_group+(" order by "+','.join(sort_arr) if sort_arr else '')+" limit ?,?"
        query_cnt = "select ifnull("+("count" if groupby else "sum")+"(cnt), 0) from (select count(*) cnt from "+from_+where_group+") a"
        cnt_all = self.one("select ifnull("+("count" if groupby else "sum")+"(cnt), 0) from (select count(*) cnt from "+from_+where_group_for_cnt+") a", tuple(params or ())[select.count('?'):])

        try: # no:33 처럼 지시검색어 때문에 1052 column is ambiguous와 1054 unknown column이 발생할 수 있다.
            data = self.rows(query, (tuple(params or ()))+tuple(target_val)+(search_query+"%",)*len(search_field)+(skip,limit))
            cnt = cnt_all if skipCount or target_search+search == '1=1' else self.one(query_cnt, tuple(params or ())[select.count('?'):]+tuple(target_val)+("%"+search_query+"%",)*len(search_field))
            #cnt = cnt_all # cnt가 느릴 경우 이걸 사용
        except Exception as e:
            if e.args[0] in [1052, 1054]:
                print(query)
            raise
        
        data_table = {'sEcho':args.get('sEcho'),
            'iTotalRecords':cnt_all,
            'iTotalDisplayRecords':cnt,
            'aaData':data}
        return data_table






class Redis:
    def __init__(self, host='localhost', port=6379, db=0, socket_timeout=None):
        self.redis = redis.Redis(host=host, port=port, db=db, socket_timeout=socket_timeout)
    def __str__(self):
        return __file__.rsplit('.', 1)[0]+'.'+self.redis.__str__()
    def __repr__(self):
        return __file__.rsplit('.', 1)[0]+'.'+self.redis.__repr__()
    #def __del__():
    # Exception AttributeError: AttributeError("'NoneType' object has no attribute 'error'",) in <bound method Connection.__del__ of Connection<host=localhost,port=6379,db=0>> ignored
    # 위의 에러가 발생하고 redis module의 소스를 보면 except socket.error: 이 부분이 있다.
    # mysql때와 마찬가지로 unload 과정중 import된 socket이 먼저 날아가서 일어나는 현상인듯 하다.
    
    # 공통
    def keys(self, expr='*'): # expr에 해당하는 key 들을 가져옴, expr은 정규식보단 shell쪽에 가까움.
        return self.redis.keys(expr)
    def type(self, k): # k 의 타입을 반환
        return self.redis.type(k)
    def expire(self, k, sec): # 해당 k를 sec초 뒤 expire 시킴
        return self.redis.expire(k, sec)
    def ttl(self, k): # expire까지 몇 초 남았는지 반환
        return self.redis.ttl(k)
    def del_(self, k=''): # 해당하는 key들에 대한 값을 제거함
        keys = self.redis.keys(k)
        for k in keys:
            self.redis.delete(k)
        return self
    def delete(self, k): # 값을 제거함
        self.redis.delete(k)
        return self
    def del_all(self): # 모든 값을 제거함
        keys = self.redis.keys('*')
        for k in keys:
            self.redis.delete(k)
        return self
    def flushdb(self): # 현재 db의 모든 값을 제거함
        self.redis.flushdb()
        return self
    def _test_common(self):
        self.del_all()  
        print(self.set('a','b').setex('c','d',3).keys())
        print(self.type('a'))
        print(self.ttl('c'))
        print(self.del_('c').keys())
        self.del_all()
        
        
    # 커스텀 공용 함수, 값을 자동으로 파악하여 넣고 뺀다.
    def s(self, k, v): # 값을 넣음
        t = type(v)
        if t == list:
            self.lset(k, v)
        elif t == dict:
            self.hsetall(k, v)
        elif t == set:
            self.sadd(k, *v)
        else:
            self.redis.set(k, v)
        return self
    def g(self, k): # 값 가져오기
        t = self.redis.type(k)
        if t == 'string':
            return self.redis.get(k)
        if t == 'list':
            return self.redis.lrange(k, 0, -1)
        if t == 'hash':
            return self.redis.hgetall(k)
        if t == 'set':
            return self.redis.smembers(k)
        if t == 'zset':
            return self.redis.zrange(k, 0, -1)
        return None
    def g_all(self, k=''): # 해당하는 key들을 모두 가져오기
        keys = self.redis.keys(k)
        kv = {}
        for k in keys:
            t = self.redis.type(k)
            if t == 'string':
                kv[k] = self.redis.get(k)
            if t == 'list':
                kv[k] = self.redis.lrange(k, 0, -1)
            if t == 'hash':
                kv[k] = self.redis.hgetall(k)
            if t == 'set':
                kv[k] = self.redis.smembers(k)
            if t == 'zset':
                kv[k] = self.redis.zrange(k, 0, -1)
        # set, zset, none 등이 추가되어야 함
        return kv
    def _test_gs(self):
        self.del_all()
        print(self.s('dict',{'a':1}).s('list',[1,2,3]).s('int',33).s('str','hi').keys())
        print(self.g('dict'))
        print(self.g('list'))
        print(self.g('int'))
        print(self.g('str'))
        self.del_all()
    def json(self, k, v=None, expire=None): # json으로 값을 넣고 읽어옴
        if v == None:
            v = self.redis.get(k)
            if v == None:
                return v
            return json.loads(v)
        if expire:
            self.redis.setex(k, json.dumps(v, default=json_default), expire)
        else:
            self.redis.set(k, json.dumps(v, default=json_default))
        return self
    
    # 단일 KV
    # http://www.redisgate.com/redis/command/strings.php
    def set(self, k, v): # 값을 넣음
        self.redis.set(k, v)
        return self
    def setex(self, k, v, sec): # 값을 넣고 sec 초 후 파기함
        self.redis.setex(k, v, sec)
        return self
    def psetex(self, k, v, msec): # 값을 넣고 msec 밀리초 후 파기함
        self.redis.psetex(k, v, msec)
        return self
    def get(self, k): # 값 가져오기
        return self.redis.get(k)
    def getset(self, k, v): # 기존값을 가져오고 새 값을 넣음
        return self.redis.getset(k, v)
    def incr(self, k, n=1): # n만큼 증가, float으로 증가시킨 뒤 incr 로 증가시키려 하면 에러가 발생한다.
        if type(n) == float:
            v = self.redis.incrbyfloat(k, n)
        else:
            try:
                v = self.redis.incr(k, n)
            except:
                v = self.redis.incrbyfloat(k, n)
        return v
    def decr(self, k, n=1): # n만큼 감소
        if type(n) == float:
            v = self.redis.incrbyfloat(k, -n)
        else:
            try:
                v = self.redis.decr(k, n)
            except:
                v = self.redis.incrbyfloat(k, -n)
        return v
    def append(self, k, str): # str 을 k의 값에 덧붙인다.
        self.redis.append(k)
        return self
    def strlen(self, k): # string의 길이
        return self.redis.strlen(k)
    def mset(self, **kwargs): # 여러 값을 한번에 set, (a=2, b=3) 패턴
        self.redis.mset(**kwargs)
        return self

    # List
    # http://www.redisgate.com/redis/command/lists.php
    def lpush(self, k, *args): # 왼쪽에 넣음, list = [1,2] 일 때 lpush('list',3,4) 는 [4,3,1,2] 가 된다.
        self.redis.lpush(k, *args)
        return self
    def lpop(self, k): # 왼쪽 값을 빼옴
        return self.redis.lpop(k)
    def lget(self, k): # 리스트 전체를 반환
        return self.redis.lrange(k, 0, -1)
    def lset(self, k, li): # 리스트 전체를 세팅
        self.redis.delete(k)
        for i in li:
            self.redis.rpush(k, i)
        return self
    def lindex(self, k, idx): # idx 번째 요소를 가져옴, 리스트는 변화 없음
        return self.redis.lindex(k, idx)
    def llen(self, k): # 리스트의 사이즈 반환
        return self.redis.llen(k)
    def rpush(self, k, *args): # 오른쪽에 넣음, list = [1,2] 일 때 rpush('list',3,4) 는 [1,2,3,4] 가 된다.
        self.redis.rpush(k, *args)
        return self
    def rpop(self, k): # 오른쪽 값을 빼옴
        return self.redis.rpop(k)
    def rpoplpush(self, from_k, to_k): # from 리스트 오른쪽에서 pop 하여 to 리스트 왼쪽으로 push
        return self.redis.rpoplpush(from_k, to_k)
    def lrange(self, k, st, ed): # 해당 리스트의 해당 범위 값들을 리스트로 반환
        return self.redis.lrange(k, st, ed)

    # Hash
    # http://www.redisgate.com/redis/command/hashes.php
    def hset(self, k, f, v): # f 필드에 값을 넣음
        self.redis.hset(k, f, v)
        return self
    def hmset(self, k, h): # h 의 key, value를 덮어씀
        self.redis.hmset(k, h)
        return self
    def hsetnx(self, k, f, v): # f 필드에 값이 없을 경우에만 값을 넣음
        self.redis.hsetnx(k, f, v)
        return self
    def hsetall(self, k, h): # h의 내용을 k Hash에 넣음(치환)
        self.redis.delete(k)
        for f, v in h.iteritems():
            self.redis.hset(k, f, v)
        return self
    def hincr(self, k, f, n=1): # f 필드의 값을 n 만큼 증가
        if type(n) == float:
            self.redis.hincrbyfloat(k, f, n)
        else:
            try:
                self.redis.hincr(k, f, n)
            except:
                self.redis.hincrbyfloat(k, f, n)
        return self
    def hget(self, k, f): # f 필드값을 가져옴. 필드값은 get 과 마찬가지로 str, int 의 구분이 없다.
        return self.redis.hget(k, f)
    def hmget(self, k, *args): # 여러 필드값을 가져옴. ['1', '2'] 형태
        return self.redis.hmget(k, *args)
    def hgetall(self, k): # k Hash 전체를 반환
        return self.redis.hgetall(k)
    def hstrlen(self, k, f): # f 필드의 str의 길이를 반환
        return self.redis.hstrlen(k, f)
    def hexists(self, k, f): # k Hash 에서 f 필드가 있는지 확인
        return self.redis.hexists(k, f)
    def hdel(self, k, f): # k Hash 에서 f 필드를 제거
        self.redis.hdel(k, f)
        return self
    def hdelall(self, k): # k Hash 전체를 제거
        self.redis.delete(k)
        return self
    def hkeys(self, k): # key들을 리스트로 반환
        return self.redis.hkeys(k)
    def hvals(self, k): # value들을 리스트로 반환
        return self.redis.hvals(k)
    def hlen(self, k): # key들의 갯수를 반환
        return self.redis.hlen(k)
    
    
    # Set
    # http://www.redisgate.com/redis/command/sets.php
    
    
    # ZSet
    # http://www.redisgate.com/redis/command/zsets.php
    
    
    # Pub Sub, db와 무관하다(connection은 db속성을 갖긴 한다)
    class Listener(threading.Thread):
        def __init__(self, r, channels, worker=None):
            threading.Thread.__init__(self)
            self.redis = r
            self.pubsub = self.redis.pubsub()
            self.pubsub.subscribe(channels)
            self.worker = worker or self._worker
        def _worker(self, channel, message):
            print('need to implement worker, channel: %s, data: %s' % (channel, message))
        def run(self):
            for message in self.pubsub.listen():
                # print message # {'pattern': None, 'type': 'message', 'channel': 'a', 'data': 'a'}
                if message['data'] == "__KILL__":
                    self.pubsub.unsubscribe()
                    break
                elif message['type'] == 'message':
                    self.worker(message['channel'], message['data'])
        def stop(self):
            self.pubsub.unsubscribe()
    def listener(self, channel, worker=None, start=True):
        """
            리스너를 얻는다
            
            @example
                r = Redis()
                sub = r.listener('batch', lambda ch,data:hanprint(ch, data))
                r.pub('batch', 'complete')
                sub.stop()
        """
        lis = self.Listener(self.redis, channel if isinstance(channel, list) else [channel], worker)
        lis.daemon = True
        if start:
            lis.start()
        return lis
    def pub(self, channel, message=None):
        """
            publish 하기
            channel = 'ch01' or ['ch01','ch02'] or {'ch01':'hello', 'ch02':'world'}
            message = 'msg' or ['msgs','msgs']
        """
        cnt = 0
        if isinstance(channel, dict):
            for k, v in channel.items():
                cnt += self.redis.publish(k, v)
            return cnt
        if type(channel) == str or type(channel) == unicode:
            channel = [channel]
        if type(message) == str or type(message) == unicode:
            message = [message]
        for ch in channel:
            for msg in message:
                cnt += self.redis.publish(ch, msg)
        return cnt
    def sub_clients(self):
        """
            subscribe 중인 client list
        """
        return [c for c in self.redis.client_list() if c['sub'] == '1']
        
        
if __name__ == "__main__":
    load_db_list()
    #main()

# mysql pool
# https://wiki.openstack.org/wiki/PyMySQL_evaluation
# http://stackoverflow.com/questions/32658679/how-to-create-a-mysql-connection-pool-or-any-better-way-to-initialize-the-multip
# https://dev.mysql.com/doc/connector-python/en/connector-python-connection-pooling.html
# https://geert.vanderkelen.org/2010/mysql-connectorpython-and-database-pooling/
# https://pypi.python.org/pypi/PyMySQL
# http://www.sqlalchemy.org/
# http://docs.sqlalchemy.org/en/rel_1_0/core/pooling.html
"""
import sqlalchemy.pool as pool
import pymysql
pymysql = pool.manage(pymysql)
conn = pymysql.connect()
"""

## mysql connection pool 사용

# import sqlalchemy.pool as pool
# connection pool을 적용하여 connection time 낭비를 줄인다
# connection pool을 적용한다고 해도 이미 연결된 connection을 동시에 사용 가능한 것은 아니다. 매번 connect 해야 한다.
# pymysql = pool.manage(pymysql, pool_size=5, max_overflow=100)
## pymysql.connect(...)
# pool을 쓸 경우 print 문이 작동하지 않는다. logger 설정을 해 줘야 한다.
#logging.getLogger('sqlalchemy.pool.QueuePool').addHandler(logging.StreamHandler())

# dbutils를 쓸 경우 매번 접속정보를 입력해서 connect 할 필요는 없다
#from DBUtils.PooledDB import PooledDB
#pool = PooledDB(pymysql, 5, host='localhost', database='test')
#db = pool.connection()


## 기타정보

#client = MongoClient('localhost') # 하나의 client당 default 100개의 connection pool을 관리하며, 자동으로 thread safe 하게 관리된다. # http://api.mongodb.org/python/current/faq.html
#client.api.authenticate(api['user'],api['password'])
#http://emptysqua.re/blog/requests-in-python-and-mongodb/
