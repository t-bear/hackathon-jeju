const fs = require('fs');
const axios = require('axios');
const mongodb = require('mongodb');
const moment = require('moment-timezone');
const request = require("request");
const iconv = require('iconv-lite');
const charset = require('charset');

var db = {}, debug = false; // debug - 실행시 -d 옵션, dlog 에만 영향을 미침





/**
  realtime function part
**/
async function invNowAll() {
  await doWork(async (doc, r, size, no, proxy) => {
    var last = await invNow(doc.code, doc.url, proxy);
    if (last) {
      dlog(`${size-r}/${size}) [${doc.code}] ${last.close} ${last.diff}(${last.per}) ${doc.url}`);
    } else {
      dlog(`${size-r}/${size}) [${doc.code}] no link or data found`);
    }
  }, await koshoCodes(true), {id:'invNowAll', proxies:120, timeout:15, retry:2, onerror:e=>{
      for (let err of e.slice(0,20)) {
        log(`invNowAll err : ${err.job.code}, ${err.job.url}, ${err.err}`);
      }
    }
  });
}
async function invNow(code, link, proxy) {
  /* investing 의 종목 페이지에서 현재가를 가져온다 */
  if (!code) return;
  if (link == null) {
    link = (await db.fin.collection('code').findOne({code:code})).url;
    if (link == "") return;
  }
  var htmlEN = await G('https://www.investing.com'+link, INVESTING_GET_HEADERS_EN, proxy);
  var curData = slice(htmlEN, 'quotes_summary_current_data', '#quotes_summary_current_data');
  if (!curData) {
    log(`code ${code} has no current data`);
    return;
  }
  curData = curData.replace(/\n/g,'');
  try {
    var last = {
      close:+slice(curData, 'id="last_last".*?>', '<').replace(/,/g,''),
      diff:+slice(curData, 'id="last_last".*?<span.*?>', '<').replace(/,/g,''),
      per:slice(curData, 'parentheses.*?>', '<'),
      currency:slice(curData, 'Currency in .*?>', '<'),
      time:slice(curData, '-time">', '<'),
      state:slice(curData, '-time">.*?class="bold".*?<\\/span>', '(<span|Currency)').replace(' - ', '').trim(),
      save_kst:moment.tz('Asia/Seoul').format('YYYYMMDD HHmmss'),
      save_ts:moment().valueOf(),
      volume:slice(curData, 'Volume:.*?<span.*?>', '<'),
      daily_range:[+slice(curData, "Day's Range:.*?-low.>", '<').replace(/,/g,''), +slice(curData, "Day's Range:.*?-high.>", '<').replace(/,/g,'')],
      type:slice(curData, 'Type:.*?elp" title="', '"'),
    };
    // investing의 시각이 뉴욕(UTC-04:00) 기준이라 timestamp 를 추출한다
    if (last.time.length == 5) { // DD/MM 형태
      last.ts = moment.tz(last.time, 'DD/MM', 'America/New_York').valueOf();
    } else if (last.time.length == 8) { // HH:mm:ss (utc-4) 형식
      var dayNY = moment.tz('America/New_York').format('YYYY-MM-DD');
      last.ts = moment.tz(dayNY + ' ' + last.time, 'YYYY-MM-DD HH:mm:ss', 'America/New_York').valueOf();
      if (last.ts > moment().valueOf()) { // 자정무렵의 일자교체로 인해 미래값을 갖는다면
        last.ts -= 86400000;
      }
    }
    
    if (last.type == 'Equity') {
      Object.assign(last, {
        market:slice(curData, 'Market:.*?elp" title="', '"'),
        isin:slice(curData, 'ISIN:.*?elp" title="', '"'),
      });
      if (last.isin) {
        last.isin = last.isin.replace(/&nbsp;/g, '');
      }
    } else if (last.type == 'Currency') {
      Object.assign(last, {
        market:slice(curData, 'Group:.*?elp" title="', '"'),
        base:slice(curData, 'Base:.*?elp" title="', '"'),
        second:slice(curData, 'Second:.*?elp" title="', '"'),
      });
    } else if (last.type == 'Index') {
      Object.assign(last, {
        market:slice(curData, 'Market:.*?elp" title="', '"'),
        components:slice(curData, '# Components:.*?elp" title="', '"'),
      });
    }
    if (last.volume) { // usd/krw 등은 volume이 없다
      last.volume = +last.volume.replace(/,/g,'');
    }
    await db.fin.collection('investing_last').updateOne({code:code}, {$set:last}, {upsert:true});
    return last;
  } catch(e) {
    dlog(`curData: ${curData.substring(0,200)}`);
    throw e;
  }
}

async function invDetailRealtime(resolution) {
  /*
    5분, 60분 데이터를 최신으로 유지한다.
    
    cron 은 30분 정도로 한다.
    
    5분 데이터가 없는 케이스:
    https://kr.investing.com/equities/zhejiang-jiaao-enprotech-stock-co-l-chart
  */
  const resultStr = {
    0: 'cached',
    1: 'have cache',
    2: 'ok',
    3: 'no data',
    4: 'init',
  };
  var codeSet = await koshoCodes();
  
  await doWork(async (code, r, size) => {
    var res = await prepare_detail('investing', code, null, null, resolution || 5);
    dlog(`${size-r}/${size}) [${code}] ${resolution} min, result: ${resultStr[res]}`);
    return res;
  }, codeSet, {worker:50, id:'invDetailRealtime'});
}

async function koshoCodes(withDoc) {
  var currency = (await db.fin.collection('code').find({src:'investing', type:'currency'}).toArray()).map(e=>e.code);
  var stock = (await db.fin.collection('investing_stock').find().toArray()).map(e=>e.code);
  var koshoEtc = ['8827','178'];
  var koshoStock = (await db.meta.collection('sheet').findOne({id:'stock_info'})).data.map(e=>e.code);
  var koshoIndex = (await db.meta.collection('sheet').findOne({id:'index_crypto'})).data.filter(e=>e.ticker.split(':')[0]=='investing').map(e=>e.ticker.split(':')[1]);
  var codeSet = keys(dict(currency.concat(koshoEtc).concat(stock).concat(koshoStock).concat(koshoIndex).map(e=>[e,1])));
  if (withDoc) {
    return (await db.fin.collection('code').find({src:'investing', code:{$in:codeSet}}).toArray());
  }
  return codeSet;
}
async function checkCode() {
  /*
    kosho 에서는 사용하지만 general code set 에는 없는 code들
  */
  var currency = (await db.fin.collection('code').find({src:'investing', type:'currency'}).toArray()).map(e=>e.code);
  var stock = (await db.fin.collection('investing_stock').find().toArray()).map(e=>e.code);
  var allCode = (await db.fin.collection('code').find({src:'investing'}).toArray()).map(e=>e.code);
  var koshoCurrency = []; // currencies except '8827'
  var koshoStock = (await db.meta.collection('sheet').findOne({id:'stock_info'})).data.map(e=>e.code);
  var koshoIndex = (await db.meta.collection('sheet').findOne({id:'index_crypto'})).data.filter(e=>e.ticker.split(':')[0]=='investing').map(e=>e.ticker.split(':')[1]);
  function sub(a,b){
    var mapB = dict(b.map(e=>[e,1]));
    return keys(dict(a.map(e=>[e,1]))).filter(k=>!mapB[k]);
  }
  log(`investing_stock - invAll: ${sub(stock, allCode)}`);
  log(`kosho stock - investing_stock: ${sub(koshoStock, stock)}`);
  log(`kosho stock - invAll: ${sub(koshoStock, allCode)}`);
  log(`kosho currency - code currency: ${sub(koshoCurrency, currency)}`);
  log(`kosho index - invAll: ${sub(koshoCurrency, currency)}`);
}





/**
  daily function part
**/

async function dailyCoin(resolution) {
  var coins = 'BTC,ETH,ZEC,XRP,LTC,ETC,BCH,BTG,QTUM,XMR,DASH'.split(',');
  coins = coins.concat('EOS,ADA,XLM,IOTA,NEO,TRX,XEM,VEN,OMG,BNB,ICX,LSK,STORM,BTM,XVG'.split(',')); // 20180425 추가
  const currs = 'KRW,USD,JPY,EUR,CNY,GBP,AUD,CAD,SGD,NZD,HKD,BTC,ETH,USDT'.split(',');
  const resultStr = {
    0: 'return cached data',
    1: 'need to fetch fresh data but have cache',
    2: 'fetch fresh data',
    3: 'fetch init data but no data found',
    4: 'fetch init data',
  };
  resolution = resolution && resolution-0 || 1;
  
  var symbols = currs.map(c => coins.map(coin => coin + '/' + c)).reduce((a,b) => a.concat(b)).concat(
    currs.map(c => currs.map(curr => curr + '/' + c)).reduce((a,b) => a.concat(b)));
  symbols = symbols.concat((await db.meta.collection('sheet').findOne({id:'index_crypto'})).data.filter(e=>e.category_id=='crypto').map(e=>e.symbol)); // kosho crypto 추가

  var codes = await db.fin.collection('code').find({'src':'investing', 'org.symbol':{'$in':symbols}}).toArray(), codeMap = {};
  codes.sort((a,b) => a.code - b.code).forEach(e => { codeMap[e.code] = e });
  
  log(`dailyCoin: ${codes.length} codes...`);
  
  await doWork(async (doc, r, size) => {
    var pid = doc.code;
    var res = await prepare_detail('investing', pid, null, null, resolution);
    dlog(`${size-r}/${size}) [${codeMap[pid] && codeMap[pid].name || pid}], pid: ${pid}, resolution: ${resolution} min, result: ${resultStr[res]}`);
    return res;
  }, codes, {worker:50, id:'dailyCoin'});
}

async function invPrepare() {
  /*
    최신 가격 정보는 이제 invNowAll 이 담당한다.
    kosho용 데이터(환율계산기 실시간 환율, 펀드 포트폴리오 실시간 가격 및 전일대비 수익률)를 포함한다.
    
    db.investing_last.ensureIndex({code:1})
    전일대비가 필요하기에 inv_detail을 이용할 수 없다.
  */
  var codeSet = await koshoCodes();
  
  var today = moment().format('YYYYMMDD'), weekAgo = moment().subtract(1, 'weeks').format('YYYYMMDD');
  await doWork(async (code, r, size, no, proxy) => {
    var rows = await prepare('investing', code, weekAgo, today, {check_update:false, proxy:proxy}); // check_update:false - 무조건 업데이트
    var row = rows[rows.length-1] || {};
    dlog(`${size-r}/${size}) [${code}], last_d: ${row.d}, close: ${row.close}, diff: ${row.diff}`);
  }, codeSet, {id:'invPrepare', proxies:100, timeout:30, retry:2, onerror:e=>{
      for (let err of e.slice(0,20)) {
        log(`invPrepare err : ${err.job}, ${err.err}`);
      }
    }
  });
}





/**
  periodical function part
**/

async function invUpdateStockAll() {
  /*
  db.investing_stock.ensureIndex({code:1});
  db.investing_stock.ensureIndex({symbol:1});
  */
  for (var e of '1,2,60,110,130,19,54,103,21,68,131,20,18'.split(',')) {
    await invUpdateStock(e);
  }
  await invUpdateStockMetaAll();
  await invStockCodeOnce();
  await updateNaverStockNameToInv();
}
async function invUpdateStock(exchange) {
  if (!exchange) return;
  const countryByExchange = {
    '0':'0', // 전체
    '1':'5', // 뉴욕
    '2':'5', // 나스닥
    '60':'11', // KOSPI
    '110':'11', // KOSDAQ
    '130':'11', // KONEX
    '19':'36', // 싱가폴 SGX
    '54':'37', // 중국 shanghai
    '103':'37', // 중국 shenzhen
    '21':'39', // 홍콩 hongkong
    '68':'46', // 대만 Taiwan
    '131':'46', // 대만 TPEX
    '20':'35', // 일본 Tokyo
    '18':'25', // 호주 Sydney
  }
  var params = {
    "country[]": countryByExchange[exchange+''],
    "sector": "7,5,12,3,8,9,1,6,2,4,10,11",
    "industry": "81,56,59,41,68,67,88,51,72,47,12,8,50,2,71,9,69,45,46,13,94,102,95,58,100,101,87,31,6,38,79,30,77,28,5,60,18,26,44,35,53,48,49,55,78,7,86,10,1,34,3,11,62,16,24,20,54,33,83,29,76,37,90,85,82,22,14,17,19,43,89,96,57,84,93,27,74,97,4,73,36,42,98,65,70,40,99,39,92,75,66,63,21,25,64,61,32,91,52,23,15,80",
    "equityType": "ORD,DRC,Preferred,Unit,ClosedEnd,REIT,ELKS,OpenEnd,ParticipationShare,CapitalSecurity,PerpetualCapitalSecurity,GuaranteeCertificate,IGC,Warrant,SeniorNote,Debenture,ETF,ADR,ETC,ETN",
    "exchange[]": exchange,
    "pn": "1",
    "order[col]": "eq_market_cap",
    "order[dir]": "d"
  }
  var proxies = await getProxies();
  var en = JSON.parse(await P('https://www.investing.com/stock-screener/Service/SearchStocks', params, INVESTING_POST_HEADERS_JSON_EN, proxies[0].host));
  var pages = Math.ceil(en.totalCount / 50);
  var today = moment().format('YYYYMMDD');
  const q = Array.apply(null, Array(pages)).map((e,i)=>i+1);
  log(`exc ${exchange} pages: ${pages}`);
  
  var done = await doWork(async (pn, r, size, no, proxy, setting)=>{
    var data = JSON.parse(JSON.stringify(params));
    data.pn = pn;
    if (setting.tried) {
      dlog(`pn: ${pn}, setting.tried: ${setting.tried}`);
    }
    var pageEN = JSON.parse(await P('https://www.investing.com/stock-screener/Service/SearchStocks', data, INVESTING_POST_HEADERS_JSON_EN, proxy));
    var pageKO = JSON.parse(await P('https://kr.investing.com/stock-screener/Service/SearchStocks', data, INVESTING_POST_HEADERS_JSON_KO, proxy));
    var pageHK = JSON.parse(await P('https://hk.investing.com/stock-screener/Service/SearchStocks', data, INVESTING_POST_HEADERS_JSON_HK, proxy));
    var pageCN = JSON.parse(await P('https://cn.investing.com/stock-screener/Service/SearchStocks', data, INVESTING_POST_HEADERS_JSON_CN, proxy));   
    var res = [];
    for (var i=0; i<pageEN.hits.length; i++) {
      var stock = {}, en = pageEN.hits[i];
      stock.code = en.pair_ID+'';
      stock.symbol = en.viewData.symbol;
      stock.flag = en.viewData.flag;
      stock.name = en.viewData.name;
      'exchange_ID,stock_symbol,security_type,sector_id,industry_id'.split(',').forEach(k=>{stock[k] = en[k]});
      'eq_market_cap,avg_volume'.split(',').forEach(k=>{stock[k] = en[k]}); // 시가총액, 3개월 평균 volume
      stock.link = en.viewData.link;
      stock.trans_en = {};
      stock.trans_hk = {};
      stock.last_update_d = today;
      'exchange_trans,name_trans,sector_trans,industry_trans'.split(',').forEach(k=>{stock.trans_en[k] = en[k]});
      'exchange_trans,name_trans,sector_trans,industry_trans'.split(',').forEach(k=>{stock.trans_hk[k] = en[k]});
      await db.fin.collection('investing_stock').updateOne({code:stock.code}, {$set:stock}, {upsert:true});
      res.push(stock);
    }
    // Korean
    for (var i=0; i<pageKO.hits.length; i++) { // ko 버전은 순서가 다를 수 있다. code로 접근해야 한다.
      var stock = {}, ko = pageKO.hits[i];
      stock.code = ko.pair_ID+'';
      stock.name = ko.viewData.name;
      stock.trans_ko = {};
      'exchange_trans,name_trans,sector_trans,industry_trans'.split(',').forEach(k=>{stock.trans_ko[k] = ko[k]});
      await db.fin.collection('investing_stock').updateOne({code:stock.code}, {$set:stock}, {upsert:true});
    }
    // Taiwan
    for (var i=0; i<pageHK.hits.length; i++) {
      var stock = {}, hk = pageHK.hits[i];
      stock.code = hk.pair_ID+'';
      stock.trans_hk = {};
      'exchange_trans,name_trans,sector_trans,industry_trans'.split(',').forEach(k=>{stock.trans_hk[k] = hk[k]});
      await db.fin.collection('investing_stock').updateOne({code:stock.code}, {$set:stock}, {upsert:true});      
    }
    // China
    for (var i=0; i<pageCN.hits.length; i++) {
      var stock = {}, cn = pageCN.hits[i];
      stock.code = cn.pair_ID+'';
      stock.trans_cn = {};
      'exchange_trans,name_trans,sector_trans,industry_trans'.split(',').forEach(k=>{stock.trans_cn[k] = cn[k]});
      await db.fin.collection('investing_stock').updateOne({code:stock.code}, {$set:stock}, {upsert:true});      
    }    
    dlog(`exc ${exchange} page ${pn} complete`);
    return res;
  }, q, {id:'invUpdateStock', proxies:80, retry:3});
  
  var res = done.res.reduce((a,b)=>a.concat(b), []); // [{stock}]
  // 추가로 덧씌워지는 정보가 있기에 지웠다가 넣기는 하면 안된다.
  //await db.fin.collection('investing_stock').deleteMany({code:{$in:res.map(e=>e.code)}});
  //await db.fin.collection('investing_stock').insertMany(res);
}

async function invUpdateStockMetaAll(forceAll) {
  var query = forceAll ? {} : {desc_en:{$exists:0}, no_profile:{$exists:0}};
  var codes = await db.fin.collection('investing_stock').find(query).toArray();
  var q = codes, size = q.length;
  const workerCnt = 70;
  
  var done = await doWork(async(doc, r, size, no, proxy)=>{
    dlog(`${size-r}/${size}) ${doc.flag}, ${doc.name}, ${doc.symbol}, ${doc.link} with ${proxy}`);
    await invUpdateStockMeta(doc.code, doc.link, proxy);
  }, q, {id:'invUpdateStockMeta', proxies:100, retry:2});
}

async function invUpdateStockMeta(code, link, proxy) {
  if (!code) return;
  if (!link) {
    link = (await db.fin.collection('investing_stock').findOne({code:code})).link;
  }
  var htmlEN = await G('https://www.investing.com'+link+'-company-profile', INVESTING_GET_HEADERS_EN, proxy);
  if (!~htmlEN.indexOf('class="bold">Profile<')) {
    log(`code ${code} has no profile`);
    await db.fin.collection('investing_stock').updateOne({code:code}, {$set:{no_profile:true}});
    return;
  }
  // 한글 페이지에서도 주소 등은 영문이고, 한글로 된 회사설명은 없어서 굳이 가져올 필요가 없다
  //var htmlKO = await G('https://kr.investing.com'+link+'-company-profile', INVESTING_GET_HEADERS_KO, proxy);
  function _(html){
    var profile = {};
    profile.address_all = [slice(html, 'addressCountry">', '<'), slice(html, 'addressLocality">', '<'), slice(html, 'postalCode">', '<')];
    profile.address = profile.address_all[1] + ', ' + profile.address_all[0];
    profile.telephone = slice(html, 'itemprop="telephone">', '<');
    profile.fax = slice(html, 'itemprop="faxNumber">', '<');
    profile.web = slice(html, 'itemprop="url">', '<');
    profile.employees = slice(html, '<div>Employees<.*?>', '<');
    profile.desc_en = slice(html, 'id="profile-fullStory-showhide".*?>', '<\\/p>') || '';
    return profile;
  }
  //console.log(_(htmlEN), htmlEN.length);
  await db.fin.collection('investing_stock').updateOne({code:code}, {$set:_(htmlEN), $unset:{no_profile:1}});
}

async function invHistoryAll() {
  /*
    전체 일별 데이터를 받아온다.
    invNow 가 매번 실행되기에 새로 편입된 주식들에 대해서만 동작하게 된다.
    invUpdateStockAll, invCoinCode 등으로 code가 추가될 때마다 1회 실행 정도면 된다.
  */
  var codeSet = await koshoCodes();
  
  await doWork(async (code, r, size, no, proxy) => {
    await prepare('investing', code, null, null, {check_update:false, proxy:proxy}); // check_update:false - 무조건 업데이트
    dlog(`${size-r}/${size}) prepare [${code}]`);
  }, codeSet, {id:'invHistoryAll', proxies:100, timeout:30, retry:2});
}

async function invCoinCode() {
  /*
    Coin과 pair인 currency code 채워넣기
    추가된게 있으면 invHistoryAll과 invDetailRealtime을 진행한다
  */
  var coins = 'BTC,ETH,ZEC,XRP,LTC,ETC,BCH,BTG,QTUM,XMR,DASH'.split(',');
  coins = coins.concat('EOS,ADA,XLM,IOTA,NEO,TRX,XEM,VEN,OMG,BNB,ICX,LSK,STORM,BTM,XVG'.split(',')); // 20180425 추가
  const currs = 'KRW,USD,JPY,EUR,CNY,GBP,AUD,CAD,SGD,NZD,HKD,BTC,ETH,USDT'.split(',');
  var symbols = currs.map(c => coins.map(coin => coin + ' ' + c)).reduce((a,b) => a.concat(b));
  symbols = symbols.concat((await db.meta.collection('sheet').findOne({id:'index_crypto'})).data.filter(e=>e.category_id=='crypto').map(e=>e.symbol.replace('/',' '))); // kosho crypto 추가
  
  var done = await doWork(async(s, r, size, no, proxy)=>{
    await invSearch(s, 'Forex', null, proxy);
    dlog(`${size-r}/${size}) search [${s}]`);
  }, symbols, {id:'invCoinCode', proxies:120, timeout:10, retry:2});
}





/**
  once function part
**/

async function invCurrencyOnce() {
  /*
    일회성으로 currency code 채워넣기
  */
  var target = 'EUR,GBP,CAD,AUD,JPY,INR,NZD,CHF,ZAR,RUB,BGN,SGD,HKD,SEK,THB,HUF,CNY,NOK,MXN,DKK,MYR,PLN,BRL,PHP,IDR,CZK,AED,TWD,KRW,ILS,ARS,CLP,EGP,TRY,RON,USD';
  for (var c of target.split(',')) {
    //await invSearch(c, 'Forex');
    await invSearch('USD ' + c, 'Forex');
  }
}
async function invStockCodeOnce() {
  /*
    일회성으로 stock code 채워넣기
    
    진행된 exchange_ID: [1,2,60,110,130,19,54,103,21,68,131,20]
    
    D I Corp 처럼 짧은 경우는 풀네임으로 다시 진행한다.
    
    아래와 같은 코드들이 검색이 되지 않고 있으며 그 이유는 검색을 하면 CFD가 대신 나오기 때문이다. CFD를 제외하고 검색을 하는 방법은 현재로는 딱히 없으며 주식 정보와 연결을 위해서는 이 코드를 써야 하고 추후 분봉등을 위해서는 잘 연결시켜야 한다.
    17086,17206,32377,102884,942652,958115,960400,985560,985622,985623,985624, ...
  */
  /*
  var stocks = await db.fin.collection('investing_stock').find({'exchange_ID':{'$in':[1,2,60,110,130,19,54,103,21]}}, {_id:0, code:1, trans_en:1}).toArray();
  /*/
  var stocks = await db.fin.collection('investing_stock').find().project({_id:0, code:1, symbol:1, trans_en:1, name:1}).toArray();
  var codes = (await db.fin.collection('code').find({code:{$in:stocks.map(e=>e.code)}}).project({_id:0, code:1}).toArray()).map(e=>e.code);
  var not_in_code = stocks.filter(s=>{return !~codes.indexOf(s.code)});
  log('not_in_code len :', not_in_code.length);
  stocks = not_in_code;
  //*/
  
  var done = await doWork(async (doc, r, size, no, proxy) => {
    var exists = false;
    if (doc.symbol) {
      await invSearch(doc.symbol, null, null, proxy);
      exists = !!(await db.fin.collection('code').findOne({code:doc.code}));
    }
    if (!exists) {
      await invSearch(doc.trans_en && doc.trans_en.name_trans || doc.name, 'Stocks');
      exists = !!(await db.fin.collection('code').findOne({code:doc.code}));
    }
    if (!exists) {
      await invSearch((doc.trans_en && doc.trans_en.name_trans || doc.name).split(' ').slice(0,2).join(' '), 'Stocks');
    }
  }, stocks, {id:'invStockToCode', proxies:50, retry:2});
  
  // not_in_code를 재조회하여 임시 code정보를 생성해넣는다
  stocks = await db.fin.collection('investing_stock').find().project({_id:0}).toArray();
  codes = (await db.fin.collection('code').find({code:{$in:stocks.map(e=>e.code)}}).project({_id:0, code:1}).toArray()).map(e=>e.code);
  not_in_code = stocks.filter(s=>{return !~codes.indexOf(s.code)});
  log('still not_in_code len :', not_in_code.length);
  
  for (let s of not_in_code) {
    var mapping = {code:'code', name:'name', eng_name:'name', url:'link', isin:'isin'}
      , doc = {src:'investing', type:'equities', main_key:'close', ohlc_key:'open,high,low,close', fromStock:true};
    items(mapping).forEach(([k,v])=>{
      doc[k] = s[v];
    });
    doc.org = {pair_ID:s.code, link:s.link, symbol:s.symbol, pair_type:'equities', flag:s.flag};
    if (s.trans_en) {
      Object.assign(doc.org, {name:s.trans_en.name_trans, trans_name:s.trans_en.name_trans, exchange_name_short:s.trans_en.exchange_trans});
    }
    if (s.trans_ko) {
      doc.org_ko = {pair_ID:s.code, link:s.link, symbol:s.symbol, pair_type:'equities', flag:s.flag, name:s.trans_ko.name_trans, trans_name:s.trans_ko.name_trans, exchange_name_short:s.trans_ko.exchange_trans};
    }
    doc.save_time = new Date();
    await db.fin.collection('code').updateOne({src:'investing', code:doc.code}, {$set:doc}, {upsert:true});
  }
}

async function invUpdateIsin() {
  var needIsin = await db.fin.collection('code').find({src:'investing', type:{$in:['equities','etf','fund']}, isin:{$exists:0}}).project({_id:0, code:1, url:1}).toArray();
  
  log('need isin:', needIsin.length);
  
  var done = await doWork(async (doc, r, size, no, proxy) => {
    var r = await G('https://www.investing.com'+doc['url'], INVESTING_GET_HEADERS_EN, proxy);
    var isin = slice(r.replace(/\n/g,''), '<span>ISIN:<.*?<span class="elp".*?>', '<');
    await db.fin.collection('code').updateOne({code:doc['code'], src:'investing'}, {$set:{isin:isin}});
  }, needIsin, {id:'invUpdateIsin', proxies:100, retry:2});
}





/**
  miraeasset & krx
**/

async function mirae(code, sd, ed, cnt, dataIndex, returnOnly) {
  /*
    db.mirae.ensureIndex({code:1, d:1})
    db.mirae.ensureIndex({d:1})
    
    // 1일 데이터 쿼리
    gubn=D&code=005930&count=300&date=20180418&unit=1&dataIndex=1&gap=1&dataKind=+
    "a5500": [
      {
        "5023": "2452000", // 종가
        "5027": "201340", // 거래량
        "5028": "492355",
        "5029": "2435000", // 시가
        "5030": "2478000", // 고가
        "5031": "2416000", // 저가
        "5032": "0",
        "5033": "+0.70", // 전일대비
        "5034": "0",
        "5201": "0",
        "5646": "20180329"
      },
    ]
    // dataIndex : [1,2,3,4,5] = [일,주,월,년,분]
    // 분일 때만 gap : [1,3,5,10,30,60] 을 사용 가능하다
    // 일단위 이상 데이터는 다 "a5500" 으로 온다
  */
  if (!code || code.toLowerCase() == 'all') {
    var names = dict((await db.fin.collection('code').find({src:'krx', notFoundOnMirae:{'$exists':0}, market:{'$in':['KOSPI','KOSDAQ']}}).project({_id:0, code:1, name:1}).toArray()).filter(e => e.code.length == 6).map(e => [e.code, e.name]));
    var codes = keys(names);
  } else {
    var codes = [code];
    var names = dict([[code, code]]);
  }
  sd = sd || moment().format('YYYYMMDD');
  ed = ed || sd;
  cnt = cnt || 390;
  dataIndex = dataIndex || '1';
  resolution = ({1:'D',2:'W',3:'M',4:'Y'})[dataIndex];
  
  async function work(code, r, size, no) {
    var rowsCnt = 0, d = ed, err, i = size - r;
    do {
      var res = JSON.parse(await P('https://www.miraeassetdaewoo.com/wts/wtschart.json', 'gubn=D&code='+code+'&count='+cnt+'&date='+d+'&unit=1&dataIndex='+dataIndex+'&gap=1&dataKind=+', MIRAE_POST_HEADERS));
      if (res['result'] == 'error') { // 상장폐지, 없는 종목의 경우
        err = `${i}/${size}) [${code}], error: 없는 종목입니다`;
        break;
      } else if (res['a5500'].length == 1 && res['a5500'][0]['5646'] == '') { // 거래정지인 경우
        err = `${i}/${size}) [${code}], error: 거래정지 종목이거나 너무 과거일자라 데이터가 없습니다`;
        break;
      } else if (!res['a5500'] || !res['a5500'].length) {
        err = `${i}/${size}) [${code}], error: 데이터가 없습니다`;
        break;
      }
      var rows = res['a5500'].map(e => ({'code':code, 'resolution':resolution, 'd':e[5646], 'close':+e[5023], 'open':+e[5029], 'high':+e[5030], 'low':+e[5031], 'volume':+e[5027]}));
      
      if (returnOnly) {
        return rows;
      }
      await db.fin.collection('mirae').deleteMany({code:code, 'resolution':resolution, d:{'$gte':rows[0].d, '$lte':rows[rows.length - 1].d}});
      await db.fin.collection('mirae').insertMany(rows);
      rowsCnt += rows.length;
    } while (rows[0].d < d && sd <= (d = rows[0].d));
    
    if (err && rowsCnt == 0) {
      dlog(err);
      if (returnOnly) {
        return [];
      }
    } else {
      dlog(`${i}/${size}) [${code}], rows: ${rowsCnt}, sd:${rows[0].d}`);
    }
  }
  if (returnOnly && codes.length == 1) { // db 변화 없이 값만 가져올 경우
    var res = await doWork(work, codes, {id:'mirae - returnOnly', fLog:null, worker:1});
    return res.done[0].res;
  } else {
    var res = await doWork(work, codes, {id:'mirae', worker:50});
  }
}

async function dailyMirae() {
  var today = moment().format('YYYYMMDD');
  //for (var i of [1,2,3,4]) {
  for (var i of [1]) { // 나머지는 사용하지 않기에 일봉만 받는다 - 20180516
    await mirae('all', today, today, 5, i);
  }
}

async function miraeDetail(code, sd, ed, cnt, gap) {
  /*
    미래에셋 1분 데이터 가져오기
    cnt는 1500개 정도가 마지노인듯 하다
    "a4500": [
      {
        "4023": "9320", // 종가
        "4027": "0",
        "4028": "0",
        "4029": "9310", // 시가
        "4030": "9320", // 고가
        "4031": "9310", // 저가
        "4032": "1819", // 거래량
        "4033": "",
        "4034": "150800", // 시각
        "4201": "0",
        "4646": "20180319" // 일자
      },
    db.krx_detail.ensureIndex({code:1, resolution:1, time:1})
    db.krx_detail.ensureIndex({kst:1})
    
    // 1일 데이터 쿼리
    gubn=D&code=005930&count=300&date=20180418&unit=1&dataIndex=1&gap=1&dataKind=+
    "a4500"과 함께 "a5500" 이 같이 온다. 과거일자를 선택해도 "a4500"은 오지만 내용은 비어있다.
    "a5500": [
      {
        "5023": "2452000", // 종가
        "5027": "201340", // 거래량
        "5028": "492355",
        "5029": "2435000", // 시가
        "5030": "2478000", // 고가
        "5031": "2416000", // 저가
        "5032": "0",
        "5033": "+0.70", // 전일대비
        "5034": "0",
        "5201": "0",
        "5646": "20180329"
      },
    ]
    // dataIndex : [1,2,3,4,5] = [일,주,월,년,분]
    // 분일 때만 gap : [1,3,5,10,30,60] 을 사용 가능하다
    // 일단위 이상 데이터는 다 "a5500" 으로 온다
  */
  if (!code || code.toLowerCase() == 'all') {
    //var codeExists = (await db.fin.collection('krx_detail').distinct('code'));
    var names = dict((await db.fin.collection('code').find({src:'krx', notFoundOnMirae:{'$exists':0}, market:{'$in':['KOSPI','KOSDAQ']}}).project({_id:0, code:1, name:1}).toArray()).filter(e => e.code.length == 6).map(e => [e.code, e.name]));
    var codes = keys(names);
  } else {
    var codes = [code];
    var names = dict([[code, code]]);
  }
  sd = sd || moment().format('YYYYMMDD');
  ed = ed || sd;
  cnt = cnt || 390;
  gap = (gap || 1)-0;
  
  var workerCnt = 50, q = codes, size = q.length, idx = 0, errorCodes = [];
  
  async function work(code) {
    var rowsCnt = 0, d = ed, err, st, et, i = ++idx;
    do {
      var res = JSON.parse(await P('https://www.miraeassetdaewoo.com/wts/wtschart.json', 'gubn=D&code='+code+'&count='+cnt+'&date='+d+'&unit=1&dataIndex=5&gap='+gap+'&dataKind=+', MIRAE_POST_HEADERS));
      if (res['result'] == 'error') { // 상장폐지, 없는 종목의 경우
        errorCodes.push(code);
        err = `${i}/${size}) [${code}], error: 없는 종목입니다`;
        break;
      } else if (res['a4500'].length == 1 && res['a4500'][0]['4646'] == '') { // 거래정지인 경우
        err = `${i}/${size}) [${code}], error: 거래정지 종목이거나 너무 과거일자라 데이터가 없습니다`;
        break;
      } else if (!res['a4500'] || !res['a4500'].length) {
        err = `${i}/${size}) [${code}], error: 데이터가 없습니다`;
        break;
      }
      var rows = res['a4500'].map(e => ({'code':code, 'resolution':gap, 'kst':e[4646]+' '+e[4034], 'close':+e[4023], 'open':+e[4029], 'high':+e[4030], 'low':+e[4031], 'volume':+e[4032], 'time':moment.tz(e[4646]+' '+e[4034], 'Asia/Seoul').unix()}));
      await db.fin.collection('krx_detail').deleteMany({code:code, resolution:gap, kst:{'$gte':rows[0].kst, '$lte':rows[rows.length - 1].kst}});
      await db.fin.collection('krx_detail').insertMany(rows);
      rowsCnt += rows.length;
      st = rows[0].kst;
      et = et || rows[rows.length - 1].kst;
      //log(`${i+1}/${size}) [${code}], rows: ${rows.length}, st:${rows[0].d}, et: ${rows[rows.length-1].d}, close: ${rows[rows.length-1].close}, elapsed: ${((+new Date - t)/1000).toFixed(1)} s`);
    } while (rows[0].kst.substring(0,8) < d && sd <= (d = rows[0].kst.substring(0,8)));
    
    if (err && rowsCnt == 0) {
      dlog(err);
    } else {
      dlog(`${i}/${size}) [${code}], rows: ${rowsCnt}, st:${st}, et: ${et}`);
    }
  }
  var pool = Array.apply(null, Array(workerCnt)).map(e=>worker(work, q));
  
  var st = +new Date;
  var results = await Promise.all(pool);
  var [done, err] = results.map(r=>[r.done, r.err]).reduce((a,b)=>{return [a[0].concat(b[0]), a[1].concat(b[1])]}, [[],[]]);
  
  dlog(`worker jobs - done:${done.length}, err:${err.length}`);
  log(`miraeDetail now: ${moment().format("YYYY-MM-DD HH:mm:ss")}, total elapsed: ${((+new Date - st)/1000).toFixed(1)} s`);
  
  if (err.length) console.log(err);
  
  if (errorCodes.length) {
    // mark error codes to db.code
    //await db.fin.collection('code').updateMany({src:'krx', code:{"$in":errorCodes}}, {'$set':{notFoundOnMirae:true}});
    //log("error codes marked at fin.code : " + errorCodes.join(','));
  }
}

async function dailyMiraeDetail(code, gap) {
  var today = moment().format('YYYYMMDD');
  await miraeDetail(code, today, today, ({1:390,5:100,60:30})[gap], gap);
}

async function miraeMakeCode() {
  // mirae의 code data - krx의 code 데이터 중 mirae 에 있는 경우만 code에 upsert한다
  var mirae = (await db.fin.collection('mirae').aggregate([{$group:{_id:'$code', cnt:{$sum:1}}}]).toArray()).map(e=>e._id);
  var krxCodes = await db.fin.collection('code').find({src:'krx', code:{$in:mirae}}).project({_id:0, src:0}).toArray();
  var miraeCodes = await db.fin.collection('code').find({src:'mirae'}).toArray();
  function sub(a,b){
    var mapB = dict(b.map(e=>[e,1]));
    return keys(dict(a.map(e=>[e,1]))).filter(k=>!mapB[k]);
  }
  var krxOnly = sub(krxCodes.map(e=>e.code), miraeCodes.map(e=>e.code)); // mirae에 없는 것만 넣는다
  await Promise.all(krxCodes.filter(e=>~krxOnly.indexOf(e.code)).map(async doc=>{
    doc.src = 'mirae';
    await db.fin.collection('code').updateOne({src:'mirae', code:doc.code}, {$set:doc}, {upsert:true});
  }));
}

async function miraeRenewPrice(code, force) {
  /* 
    2018년 5월, 삼성전자(005930) - 액면분할, 리켐(131100) - 무상증자 등의 이벤트가 발생했고
    이 경우 가격이 크게 변하는 등의 문제가 생기기에 미래에셋 등은 과거 데이터도 전부 변경된 가격 기준으로 바꾸게 된다.
    이를 이용하여 60일치 가격을 비교해서 가격이 다른 과거 데이터가 발생하면
    mirae, krx_detail, investing, investing_detail 의 데이터를 전부 파기하고 갱신한다.
    code src:mirae 기준이다.
    
    code - all or '001300,005930' or null
    force - true if want to remove all data and receive all data
    
    20180515 - etf 를 위주로 일반 주식도 포함돼서 미래에셋이 잘못된 데이터를 보내고 있다
      price diff KODEX 자동차[091180]
      price diff KODEX 반도체[091160]
      price diff TIGER 반도체[091230]
      price diff 한국철강[104700]
      price diff KODEX 삼성그룹[102780]
      price diff TIGER 200[102110]
      price diff KINDEX 200[105190]
  */
  if (!code || code == 'all') {
    var codes = (await db.fin.collection('code').find({src:'mirae'}).project({_id:0, code:1, name:1}).toArray());
  } else {
    var codes = code.split(',').map(e=>({code:e, name:e}));
  }
  
  await doWork(async(doc, r, size, no, proxy)=>{
    // daily 데이터를 기준으로 체크한다
    var res = await mirae(doc.code, null, null, 60, 1, true);
    if (!res.length) {
      return;
    }
    var d = res.map(e=>e.d);
    var resMap = dict(res.map(e=>[e.d,e]));

    var price = await db.fin.collection('mirae').find({code:doc.code, resolution:'D', d:{$in:d}}).project({_id:0}).sort({d:1}).toArray();
    var diffCnt = 0;
    
    if (force) {
      diffCnt = 1;
    } else if (!price.length || price.length < res.length - 3) { // 기존 데이터가 너무 적은 경우
      diffCnt = 1;
    } else {
      price.pop(); // 맨 마지막(가장 최신)은 제거
      price.forEach(e=>{
        if (e.close != resMap[e.d].close) {
          diffCnt++;
          dlog(`price diff ${doc.name}[${doc.code}] ${e.d} : ${e.close} vs ${resMap[e.d].close}`);
        }
      });
    }
    if (diffCnt > 0) {
      return log(`price diff ${doc.name}[${doc.code}]`);
      var invCode = await db.fin.collection('code').findOne({src:'investing', 'org.symbol':doc.code, 'org.flag':'South_Korea', 'type':'equities'});
      if (invCode) {
        await db.fin.collection('investing').deleteMany({src:'investing', code:invCode.code});
        await db.fin.collection('investing_detail').deleteMany({src:'investing', code:invCode.code});
        await db.fin.collection('prepare').deleteMany({src:'investing', code:invCode.code});
        await db.fin.collection('prepare_detail').deleteMany({src:'investing', code:invCode.code});
        await prepare('investing', invCode.code, null, null, {proxy:proxy});
        await prepare_detail('investing', invCode.code, null, null, 5);
        await prepare_detail('investing', invCode.code, null, null, 60);
      }
      await db.fin.collection('mirae').deleteMany({code:doc.code});
      await db.fin.collection('krx_detail').deleteMany({code:doc.code});
      var today = moment().format('YYYYMMDD');
      await mirae(doc.code, '19850101', today, 1000, 1);
      //await mirae(doc.code, '19850101', today, 1000, 2);
      //await mirae(doc.code, '19850101', today, 1000, 3);
      //await mirae(doc.code, '19850101', today, 1000, 4);
      await miraeDetail(doc.code, '19850101', today, 1000, 1);
      await miraeDetail(doc.code, '19850101', today, 1000, 5);
      await miraeDetail(doc.code, '19850101', today, 1000, 60);
    }
    //dlog(`${size-r}/${size}) check price [${doc.code}]`);
  }, codes, {id:'miraeRenewPrice', proxies:1, retry:1, timeout:600, fLog:null});
}



/**
  investing
**/

async function invSearch(q, tab_id, country_id, proxy) {
  /*
      investing 의 정보를 검색한다
      
      검색된 정보는 db.fin.code에 남겨둔다
      tab_id: All, Indices, Stocks, ETFs, Funds, Commodities, Forex, Bonds
      country_id: 0-전체,
  */
  tab_id = tab_id || 'All'
  country_id = country_id || 0
  var en = JSON.parse(await P('https://www.investing.com/search/service/search', data='search_text='+q+'&term='+q+'&country_id='+country_id+'&tab_id='+tab_id, INVESTING_POST_HEADERS_JSON_EN, proxy));
  var ko = JSON.parse(await P('https://kr.investing.com/search/service/search', data='search_text='+q+'&term='+q+'&country_id='+country_id+'&tab_id='+tab_id, INVESTING_POST_HEADERS_JSON_KO, proxy));
  
  var codes = en.All.map(e=>e.pair_ID+'');
  var codeMapKo = dict(ko.All.map(e=>[e.pair_ID, e]));
  var codeExists = dict((await db.fin.collection('code').find({src:'investing', code:{'$in':codes}}).project({_id:0, code:1, org:1, org_ko:1, isin:1, isin_not_found:1}).toArray()).map(e => [e.code, e])); // {code:{code,org,org_ko..}}
  
  var data = [], updateCnt = 0;
  for (var row of en.All) {
    var code = row['pair_ID']+'';
    var old = codeExists[code];
    var obj = {
      code:code,
      name:row['name'],
      eng_name:row['trans_name'],
      type:row['pair_type'],
      url:row['link'],
      src:'investing',
      org:row,
      org_ko:codeMapKo[code],
      save_time:new Date(),
      main_key:'close',
      ohlc_key:'open,high,low,close',
    }
    data.push(obj);
    if (old && old.org && old.org_ko) {
      //continue // 정보가 바뀌기도 하여 그냥 업데이트한다
    }
    await db.fin.collection('code').updateOne({code:obj['code'], src:'investing'}, {$set:obj}, {upsert:true});
    updateCnt++;
  }
  
  dlog(`invSearch: ${q}, found ${data.length} codes, updated ${updateCnt} codes`);
  return data;
}

async function invUpdateStockNewsAll(forceAll) {
  /*
  db.investing_news.ensureIndex({code:1});
  db.investing_news.ensureIndex({symbol:1});
  */
  var newsCodes = (await db.fin.collection('investing_news').find({}).toArray()).map(e=>e.code);
  var query = forceAll ? {} : {code:{$nin:newsCodes}};
  var codes = await db.fin.collection('investing_stock').find(query).toArray();
  
  await doWork(async (doc, r, size, no, proxy) => {
    dlog(`${size-r}/${size}) ${doc.flag}, ${doc.name}, ${doc.symbol}, ${doc.link}`);
    await invUpdateStockNews(doc.code, doc.link, 'en', proxy);
    await invUpdateStockNews(doc.code, doc.link, 'ko', proxy);
  }, codes, {id:'invStockNews', proxies:150, retry:2});
}
async function invUpdateStockNews(code, link, lang, proxy) {
  if (!code) return;
  if (!link) {
    link = (await db.fin.collection('investing_stock').findOne({code:code})).link;
  }
  if (~link.indexOf('?')) { // link에 cid가 있는 경우 : /equities/bank-of-china-ss?cid=9217
    link = link.replace('?', '-news?');
  } else {
    link = link + '-news';
  }
  var html = await G('https://'+(lang=='ko'?'kr':'www')+'.investing.com'+link, lang=='ko'?INVESTING_GET_HEADERS_KO:INVESTING_GET_HEADERS_EN, proxy);
  var data = slice(html, '<div class="mediumTitle1">', '<script>'), tags = data ? split(data, '<article.*?>', '<\\/article') : null;
  if (!tags) return;// dlog(`        code ${code} has no news of '${lang}'`);
  var news = tags.map(e=>{
    var n = {};
    n.href = slice(e, '<a href="', '"');
    n.img = slice(e, '<img src="', '"');
    n.title = slice(e, '<a.*?class="title".*?>', '<');
    n.by = slice(e, 'class="articleDetails">.*?<span>', '<').replace('부터 ','');
    n.date = slice(e, 'class="articleDetails">.*?<span class="date">', '<');
    n.date = n.date ? n.date.replace('&nbsp;-&nbsp;', '') : n.date;
    n.detail = slice(e, 'class="articleDetails">[\\s\\S]*?<p>', '<');
    return n;
  });
  //console.log(news);
  var doc = {};
  lang = lang || 'en';
  doc['news_'+lang] = news
  await db.fin.collection('investing_news').updateOne({code:code}, {$set:doc}, {upsert:true});
}

async function checkInvStockLink() {
  /* 
    stock의 link와 code의 url을 비교한다
    
    stock 중복체크:
    var dup = db.investing_stock.aggregate([{$group:{_id:'$code', total:{$sum:1}}}, {$match:{total:{$gte:2}}}]).toArray();
    stock 중복제거:
    dup.forEach(e=>db.investing_stock.deleteOne({code:e._id}));
    // 이후 invMeta, 한글주식->inv 를 다시 실행해야 한다
  */
  var stocks = (await db.fin.collection('investing_stock').find().toArray());
  var codes = (await db.fin.collection('code').find({src:'investing', code:{$in:stocks.map(e=>e.code)}}).toArray());
  var codeMap = dict(codes.map(e=>[e.code, e]));
  
  stocks.forEach(e=>{
    if (codeMap[e.code] && e.link != codeMap[e.code].url) {
      log(`link diffs: code - ${e.code}, url - ${codeMap[e.code].url}, link - ${e.link}`);
    }
  });
}

async function setInvCodeOrgLink() {
  /* 
    code의 url과 org.link를 비교한다
    db.code.find({src:'investing', $where:"this.org && this.url != this.org.link"}).count()
  */
  var codes = (await db.fin.collection('code').find({src:'investing', $where:"this.org && this.url != this.org.link"}).toArray());
  
  Promise.all(codes.map(async doc=>{
    await db.fin.collection('code').updateOne({code:doc.code}, {$set:{url:doc.org.link}})
  }));
  log(`${codes.length} link fixed`);
}





/**
  naver
**/

async function naverUpdateAll(func) {
  /*
    db.fin.collection('code').find({src:'krx'}).toArray().then(e=>{krx=e});
    db.fin.collection('code').find({src:'investing', type:{$in:['equities','etf']}, 'org.country_ID':11}).toArray().then(e=>{inv=e});
    function sub(a,b){
      var mapB = dict(b.map(e=>[e,1]));
      return keys(dict(a.map(e=>[e,1]))).filter(k=>!mapB[k]);
    }
    var krxOnly = sub(krx.map(e=>e.code), inv.map(e=>e.org.symbol)); // 815
    var invOnly = sub(inv.map(e=>e.org.symbol), krx.map(e=>e.code)); // [ '194860', '232360' ]
  */
  var codes = (await db.fin.collection('code').find({src:'krx'}).toArray()).map(e=>e.code), size = codes.length;
  var f = ({meta:naverUpdateStockMeta, news:naverUpdateStockNews})[func] || naverUpdateStockNews;
  
  async function work(code, remainCnt) {
    dlog(`${size-remainCnt}/${size}) ${code} ${await f(code)}`);
  }
  var res = await workerPool(work, codes, 50);
  log(`naverUpdateAll ${func} ${moment().format("YYYY-MM-DD HH:mm:ss")}] done:${res.done.length}, err:${res.err.length}, elapsed:${res.elapsed.toFixed(1)} s`);
  res.err.length && console.log(res.err.slice(0,10));
}
async function naverUpdateStockMeta(code) {
  /*
    db.naver_stock.ensureIndex({code:1})
  */
  var r = (await GE('http://finance.naver.com/item/coinfo.nhn?code='+code, NAVER_GET_HEADERS)).replace(/\n/g,'');
  var info = slice(r, '"summary_info".*?h4>', '<div');
  if (!info) return dlog(`${code} 의 기업정보가 없습니다`) || false;
  var inv_code = (await db.fin.collection('code').find({src:'investing', 'org.symbol':code, 'org.flag':'South_Korea'}).toArray());
  if (inv_code.length != 1) {
    if (inv_code.length > 1) {
      dlog(`krx:${code} has multiple investing code :  inv_code.map(e=>e.code).join(', ')`);
    }
    inv_code = null;
  } else {
    // 252940 외에는 모두 존재
    inv_code = inv_code[0].code;
  }
  var doc = {
    code:code,
    inv_code:inv_code,
    name:slice(r, '<dd>종목명 ','<'),
    exchange:slice(r, '<dd>종목코드 ','<').split(' ')[1],
    desc:split(info, '<p>', '<\\/p>').join('\n'),
    info_from:slice(r, '"txt_notice">', '<').replace('출처 : ',''),
  };
  await db.fin.collection('naver_stock').updateOne({code:code}, {$set:doc}, {upsert:true});
  return doc.info_from;
}

async function naverUpdateStockNews(code, page) {
  page = page || 1;
  var r = (await GE('http://finance.naver.com/item/news_news.nhn?code='+code+'&page='+page+'&sm=title_entity_id.basic&clusterId=', NAVER_GET_HEADERS)).replace(/\n/g,'');
  var list = slice(r, '<caption>종목뉴스.*?<tbody>', '종목뉴스 끝');
  if (!list) return dlog(`${code} 의 종목뉴스가 없습니다`) || false;
  
  list = list.replace(/<table>.*?<\/table>/g, ''); // 연관기사 제거
  var trs = split(r, '<tr.*?>', '<\\/tr>') || [];
  var news = trs.map(e=>{
    return {
      title:slice(e, '<a.*?target.*?>', '<'),
      href:slice(e, '<a href="', '"'),
      info:slice(e, '<td class="info">', '<'),
      date:slice(e, '<td class="date">', '<'),
    };
  }).filter(e=>e.title);
  if (news) {
    await db.fin.collection('naver_news').updateOne({src:'mirae', code:code}, {$set:{news:news}}, {upsert:true});
  }
  return news.length;
}

async function checkInvKrx() {
  /* 
    investing 에는 있지만 krx에는 없는 주식 찾기
    'Aspac Oil Co Ltd 1006422 232360 equities',
    'SY Innovation Co Ltd 1006376 194860 equities'
  */
  var codes = (await db.fin.collection('code').find({src:'krx'}).toArray()).map(e=>e.code);
  var invCodes = (await db.fin.collection('code').find({src:'investing', 'org.symbol':{$nin:codes}, 'org.flag':'South_Korea', 'type':'equities'}).toArray()).map(e=>`${e.name} ${e.code} ${e.org.symbol} ${e.type}`);
  console.log(invCodes);
}

async function updateNaverStockNameToInv() {
  /* naver의 한글이름을 investing으로 */
  var names = (await db.fin.collection('naver_stock').find({inv_code:{$exists:1}}).toArray());
  for (let doc of names) {
    await db.fin.collection('code').updateOne({src:'investing', code:doc.inv_code, 'org_ko.name':{$exists:1}}, {$set:{'org_ko.name':doc.name, 'org_ko.trans_name':doc.name}});
    await db.fin.collection('investing_stock').updateOne({code:doc.inv_code, 'trans_ko.name_trans':{$exists:1}}, {$set:{'trans_ko.name_trans':doc.name}});
  };
}



/**
  proxy collect part
**/
async function getProxies(cnt, score) {
  return await db.proxy.collection('proxy').find({score:{$gte:score||50}}).sort({score:-1}).limit(cnt||1000).toArray();
}

async function xroxy() {
  var res = await G('http://www.xroxy.com/proxylist.php?ssl=ssl&latency=5000&reliability=9000&sort=reliability&desc=true&pnum=0');
  var cnt = +slice(res.replace(/\n/g,''), "<small><b>", '<'), pages = parseInt((cnt+9)/10);
  var works = await doWork(async (page, r, size) => {
    var res = await G('http://www.xroxy.com/proxylist.php?ssl=ssl&latency=5000&reliability=9000&sort=reliability&desc=true&pnum='+page);
    var trs = split(res.replace(/\n/g,''), "title='View this Proxy details'", "<\\/tr>");
    var prxs = trs.map(tr=>{
      var td = split(tr, '<td>', '<');
      var td_a = split(tr, '<td><a.*?>', '<');
      return {
        src:'xroxy.com',
        ip:slice(tr, '>', '<'),
        port:+td_a[0],
        host:slice(tr, '>', '<')+':'+td_a[0],
        org:{
          type:td_a[1],
          ssl:td_a[2],
          latency:+td[3],
          reliability:+td[4],
        },
        last_kst:moment.tz('Asia/Seoul').format('YYYYMMDD HHmmss'),
      }
    });
    dlog(prxs.length, prxs[0].ip+':'+prxs[0].port);
    return prxs;
  }, Array.apply(null, Array(pages)).map((e,i)=>i), {worker:1, id:'xroxy fetchProxyPage'});
  var prxs = works.res.reduce((a,b)=>a.concat(b), []);
  
  dlog(`xroxy ssl proxies : ${prxs.length}`);
  
  for (let p of prxs) {
    await db.proxy.collection('proxy').updateOne({host:p.host}, {$set:p}, {upsert:true});
  }
  
  await testProxy();
  
  return prxs;
}

async function freeProxyList() {
  // 300 proxies only
  try {
    var res = await G('https://free-proxy-list.net');
  } catch(e) { // 1 more try
    var res = await G('https://free-proxy-list.net');
  }
  var tbody = slice(res.replace(/\n/g,''), 'table-bordered.*?tbody>', '<\\/tbody'),
      trs = split(tbody, '<tr>', '<\\/tr>');
  var prxs = trs.map(tr=>{
    var tds = split(tr, '<td.*?>', '<\\/td');
    return {
      src:'free-proxy-list.net',
      ip:tds[0],
      port:+tds[1],
      host:tds[0]+':'+tds[1],
      org:{
        code:tds[2],
        country:tds[3],
        anonymity:tds[4],
        google:tds[5],
        https:tds[6],
        last:tds[7],
      },
      last_kst:moment.tz('Asia/Seoul').format('YYYYMMDD HHmmss'),
    }
  }).filter(p=>p.org.https=='yes');
  
  dlog(`ssl proxies : ${prxs.length}`);
  
  for (let p of prxs) {
    await db.proxy.collection('proxy').updateOne({host:p.host}, {$set:p}, {upsert:true});
  }
  
  await testProxy();
  
  return prxs;
}

async function nordvpn() {
  var res = await G('https://nordvpn.com/wp-admin/admin-ajax.php?searchParameters%5B0%5D%5Bname%5D=proxy-country&searchParameters%5B0%5D%5Bvalue%5D=&searchParameters%5B1%5D%5Bname%5D=proxy-ports&searchParameters%5B1%5D%5Bvalue%5D=&searchParameters%5B2%5D%5Bname%5D=https&searchParameters%5B2%5D%5Bvalue%5D=on&offset=0&limit=1000&action=getProxies');
  var j = JSON.parse(res);
  var prxs = j.map(e=>{
    return {
      src:'nordvpn.com',
      ip:e.ip,
      port:+e.port,
      host:e.ip+':'+e.port,
      org:{
        country:e.country,
        country_code:e.country_code,
        anonymity:e.anonymity,
        type:e.type,
      },
      last_kst:moment.tz('Asia/Seoul').format('YYYYMMDD HHmmss'),
    }
  });
  
  dlog(`nordvpn ssl proxies : ${prxs.length}`);
  
  for (let p of prxs) {
    await db.proxy.collection('proxy').updateOne({host:p.host}, {$set:p}, {upsert:true});
  }
  
  await testProxy();
  
  return prxs;
}

async function gatherproxy() {
  var res = await G('http://www.gatherproxy.com/');
  var html = slice(res.replace(/\n/g, ''), '<table', '<\\/table');
  var prxs = split(html, 'gp.insertPrx\\(', '\\);').map(e=>{
    var j = JSON.parse(e);
    return {
      src:'gatherproxy.com',
      ip:j.PROXY_IP,
      port:parseInt(j.PROXY_PORT, 16),
      host:j.PROXY_IP+':'+parseInt(j.PROXY_PORT, 16),
      org:{
        country:j.PROXY_COUNTRY,
        status:j.PROXY_STATUS,
        type:j.PROXY_TYPE,
        response_time:j.PROXY_TIME,
        uptime:j.PROXY_UPTIMELD,
      },
      last_kst:moment.tz('Asia/Seoul').format('YYYYMMDD HHmmss'),
    }
  });
  
  dlog(`gatherproxy proxies : ${prxs.length}`);
  
  for (let p of prxs) {
    await db.proxy.collection('proxy').updateOne({host:p.host}, {$set:p}, {upsert:true});
  }
  
  await testProxy();
  
  return prxs;
}

async function pubproxy() {
  var res = await G('http://pubproxy.com/api/proxy?google=true&https=true&limit=20');
  var j = JSON.parse(res);
  var prxs = j.data.map(e=>{
    return {
      src:'pubproxy.com',
      ip:e.ip,
      port:+e.port,
      host:e.ipPort,
      org:{
        country:e.country,
        last_checked:e.last_checked,
        proxy_level:e.proxy_level,
        type:e.type,
        speed:e.speed,
        support:e.support,
      },
      last_kst:moment.tz('Asia/Seoul').format('YYYYMMDD HHmmss'),
    }
  });
  
  dlog(`pubproxy proxies : ${prxs.length}`);
  
  for (let p of prxs) {
    await db.proxy.collection('proxy').updateOne({host:p.host}, {$set:p}, {upsert:true});
  }
  
  await testProxy();
  
  return prxs;
}

async function freeProxy() {
  var pages = 5;
  var works = await doWork(async (page, r, size) => {
    var res = await G('http://free-proxy.cz/en/proxylist/country/all/https/speed/all/'+page, {
      'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
      'Accept-Encoding': 'gzip, deflate',
      'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36'
    });
    var html = slice(res.replace(/\n/g, ''), '<tbody', '<\\/tbody');
    if (!html) {
      log(res);
    }
    var trs = split(html, "<tr>", "<\\/tr>");
    var prxs = trs.map(tr=>{
      var td = split(tr, '<td', '<\\/td');
      if (td.length < 3) return;
      var ip = Buffer.from(slice(td[0], 'Base64.decode."', '"'), 'base64').toString('ascii'),
          port = +slice(td[1], '<span.*?>', '<');
      return {
        src:'free-proxy.cz',
        ip:ip,
        port:+port,
        host:ip+':'+port,
        org:{
          protocol:slice(td[2], '<small>', '<'),
          country:slice(td[3], '<a.*?>', '<'),
          region:slice(td[4], '<small>', '<'),
          city:slice(td[5], '<small>', '<'),
          anonymity:slice(td[6], '<small>', '<'),
          speed:slice(td[7], '<small>', '<'),
          uptime:slice(td[8], '<small>', '<'),
          response:slice(td[9], '<small>', '<'),
          last_checked:slice(td[10], '<small>', '<'),
        },
        last_kst:moment.tz('Asia/Seoul').format('YYYYMMDD HHmmss'),
      }
    });
    dlog(prxs.length, prxs[0].ip+':'+prxs[0].port);
    return prxs.filter(e=>e);
  }, Array.apply(null, Array(pages)).map((e,i)=>i+1), {worker:1, id:'freeProxy fetchProxyPage'});
  var prxs = works.res.reduce((a,b)=>a.concat(b), []);
  
  dlog(`freeProxy proxies : ${prxs.length}`);
  
  for (let p of prxs) {
    await db.proxy.collection('proxy').updateOne({host:p.host}, {$set:p}, {upsert:true});
  }
  
  //await testProxy();
  
  return prxs;
}

async function ip4() {
  await doWork(async(i,r,s,n,p)=>{
    await G('http://ip4.ssh.works', {}, p);
  }, 1050, {id:'ip4', proxies:250});
}

async function testProxy() {
  var prxs = await db.proxy.collection('proxy').find().toArray();
  prxs = prxs.filter(e=>e.score || !e.inv_search || e.inv_search.length < 5);
  var works = await doWork(async (prx, r, size) => {
    var url = 'https://www.investing.com/search/service/search';
    var res = {time:new Date(), elapsed:999.9, success:false};
    try {
      var [data, success, elapsed] = await timeoutRequest({method:'POST', url:url, body:'search_text=&term=&country_id=0&tab_id=All', gzip:true, headers:INVESTING_POST_HEADERS_EN, timeout:7000, 'proxy':`http://${prx.ip}:${prx.port}`}, 7000);
      if (success && data == '{}') {
        res = {time:new Date(), elapsed:elapsed/1000, success:true};
        dlog(`${size-r}/${size}) ${prx.ip}:${prx.port} size:${data.length}`);
      } else if (success) {
        res = {time:new Date(), elapsed:999.9, success:false, data:data.substring(0,200)};
        dlog(`${size-r}/${size}) ${prx.ip}:${prx.port} data:${data.substring(0,80)}`);
      } else {
        dlog(`${size-r}/${size}) ${prx.ip}:${prx.port} err:timeout`);
      }
    } catch(e) {
      dlog(`${size-r}/${size}) ${prx.ip}:${prx.port} err:${e}`);
    }
    await db.proxy.collection('proxy').updateOne({host:prx.host}, {$push:{inv_search:{$each:[res], $slice:-100}}});
  }, prxs, {worker:prxs.length, id:'testProxy inv_search'});
  
  works = await doWork(async (prx, r, size) => {
    var url = 'https://www.investing.com/search/service/search';
    var res = {time:new Date(), elapsed:999.9, success:false};
    try {
      var [data, success, elapsed] = await timeoutRequest({method:'POST', url:'https://www.investing.com/instruments/HistoricalDataAjax', body:'action=historical_data&curr_id=1&st_date=01%2F02%2F2018&end_date=01%2F05%2F2018&interval_sec=Daily', gzip:true, headers:INVESTING_POST_HEADERS_EN, timeout:7000, 'proxy':`http://${prx.ip}:${prx.port}`}, 7000);
      if (success && ~data.indexOf('id="results_box"')) {
        res = {time:new Date(), elapsed:elapsed/1000, success:true};
        dlog(`${size-r}/${size}) ${prx.ip}:${prx.port} size:${data.length}`);
      } else if (success) {
        res = {time:new Date(), elapsed:999.9, success:false, data:data.substring(0,200)};
        dlog(`${size-r}/${size}) ${prx.ip}:${prx.port} data:${data.substring(0,80)}`);
      } else {
        dlog(`${size-r}/${size}) ${prx.ip}:${prx.port} err:timeout`);
      }
    } catch(e) {
      dlog(`${size-r}/${size}) ${prx.ip}:${prx.port} err:${e}`);
    }
    await db.proxy.collection('proxy').updateOne({host:prx.host}, {$push:{inv_history:{$each:[res], $slice:-100}}});
  }, prxs, {worker:prxs.length, id:'testProxy inv_history'});
  
  works = await doWork(async (prx, r, size) => {
    var res = {time:new Date(), elapsed:999.9, success:false};
    try {
      var [data, success, elapsed] = await timeoutRequest({url:'https://ipinfo.io/json', gzip:true, timeout:7000, 'proxy':`http://${prx.ip}:${prx.port}`}, 7000);
      if (success && JSON.parse(data).ip == prx.ip) {
        res = {time:new Date(), elapsed:elapsed/1000, success:true};
        dlog(`${size-r}/${size}) ${prx.ip}:${prx.port} ip:${prx.ip}`);
      } else if (success) {
        res = {time:new Date(), elapsed:elapsed/1000, success:true, ip:JSON.parse(data).ip};
        dlog(`${size-r}/${size}) ${prx.ip}:${prx.port} ip:${JSON.parse(data).ip} != ${prx.ip}`);
      } else {
        dlog(`${size-r}/${size}) ${prx.ip}:${prx.port} err:timeout`);
      }
    } catch(e) {
      dlog(`${size-r}/${size}) ${prx.ip}:${prx.port} err:${e}`);
    }
    await db.proxy.collection('proxy').updateOne({host:prx.host}, {$push:{ipinfo:{$each:[res], $slice:-100}}});
  }, prxs, {worker:prxs.length, id:'testProxy ipinfo'});

  await calcProxy();
}

async function calcProxy() {
  var prxs = await db.proxy.collection('proxy').find().toArray();
  for (let p of prxs) {
    var stat = {}, score = 0;
    'inv_search,inv_history,ipinfo'.split(',').map(k=>{
      var cnt = p[k].length;
      var avg = p[k].map(e=>e.elapsed).reduce((a,b)=>a+b) / cnt;
      var avail = p[k].filter(e=>e.success).length / cnt;
      stat[k+'_cnt'] = cnt;
      stat[k+'_avg'] = avg;
      stat[k+'_avail'] = avail;
      stat[k+'_weighted'] = p[k].map((e,i)=>e.success?(i+1):0).reduce((a,b)=>a+b) / ((1+cnt) * cnt / 2);
      score += stat[k+'_weighted'] * (cnt > 5 ? 1 : 0.5) * (avg < 5 ? 1 : 0.9 * (999 - avg) / 999);
    });
    score = score / 3 * 100;
    await db.proxy.collection('proxy').updateOne({host:p.host}, {$set:{score, stat}});
  }
}











/** sub function part **/

async function prepare(src, code, sd, ed, setting) {
  /*
    주어진 code 와 주어진 기간에 따른 데이터를 준비한다.
    
    기존 이력을 살펴서 db에 있다면 바로 해당 데이터를 반환하고 해당 데이터가 없다면 새로 받아온다.
    필요할 경우 code에 해당하는 code정보는 search등을 통해서 미리 마련해야 한다.
    
    check_update_hour 의 경우 해당 시각보다 더 큰 주기로 원 source에 최신 데이터가 추가됨이 보장되어야 한다.
    즉 하루 한 번 업데이트 되는 소스를 일주일 기한을 최신 데이터 기간으로 두고 업데이트하면 안된다는 것
    반면 check_update_hour 보다 잦은 주기로 배치를 통한 업데이트를 실시한다면 fin 의 경우 대부분 빠르게 DB에서 데이터를 가져올 수 있다.
    
    map(lambda a:prepare('krx', a), db.qara.ones("select distinct code from stock_price"))
    map(lambda a:invSearch(a), db.qara.ones("select distinct code from fidx_price"))
    
    @args src[str]`krx` 가격을 가져오려는 source. source:code 형태로 사용할 수도 있다.
    @args code[str] 각 데이터 소스별 구분코드
    @args sd[str] YYYYMMDD 형식의 시작일자
    @args ed[str] YYYYMMDD 형식의 종료일자
    @args setting[object]
      check_update[bool=True] True일 경우 업데이트 시간을 고려해서 새 데이터를 가져올지를 결정한다.
      proxy[str] history 함수에 건네줄 proxy
  */
  setting = Object.assign({check_update:true}, setting && typeOf(setting) == 'object' ? setting : {});
  var CONFIG = {
    'investing': {sd:'19700101', func:inv_history, check_update_hour:1},
  }
  var cfg = CONFIG[src];
  if (!cfg) {
    log(`src [${src}] not found`);
    return;
  }
  code = code + '';
  
  var now = moment();
  var coll = db.fin.collection(src);
  sd = sd || cfg.sd;
  ed = ed || now.format('YYYYMMDD');
  
  // prepare 상에서 sd, ed 포함 여부를 확인한다.
  // prepare가 최신 상태로 업데이트 되고 있음을 보장받는다면 ed는 prepare보다 커도 무시할 수 있다
  var rows = await db.fin.collection('prepare').find({code:code, src:src}).project({_id:0}).toArray();
  if (rows.length) {
    var pp = rows[0];
    
    if (pp['sd'] <= sd && (pp['ed'] > ed || setting.check_update && pp['last_update'] > now - cfg.check_update_hour * 60 * 60 * 1000)) {
      //dlog('return cached data');
      return await coll.find({code:code, d:{'$gte': sd, '$lte': ed}}).project({_id:0}).toArray();
    } else {
      // 보강이 필요하다
      var need_sd = pp['ed'];
      var need_ed = pp['sd'];
      if (pp['sd'] > sd)
        need_sd = sd;
      if (pp['ed'] <= ed && (!setting.check_update || pp['last_update'] < now - cfg.check_update_hour * 60 * 60 * 1000))
        need_ed = ed;
      if (need_sd <= need_ed) { // 가져올 필요가 있을 경우만
        //dlog('need to fetch fresh data');
        var idxs = await cfg.func(code, need_sd, need_ed, setting);
        var res_sd, res_ed, res_cnt = 0;
        if (idxs && idxs.length) {
          // investing이고 code가 있고 한국주식 이라면 주말을 제거한다
          if (src == 'investing') {
            var info = await db.fin.collection('code').find({src:src, code:code, type:{$in:['equities','etf']}, 'org.country_ID':11}).toArray();
            if (info.length) { // 한국주식이다
              idxs = idxs.filter(e=>![0,6].includes(moment(e.d).day())); // 일(0), 토(6) 제외
            }
          }
          await coll.deleteMany({'code':code, 'd':{'$gte':need_sd, '$lte':need_ed}});
          await coll.insertMany(idxs);
          var days = idxs.map(e => e.d);
          res_sd = Math.min.apply(null, days);
          res_ed = Math.max.apply(null, days);
          res_cnt = days.length;
        } else if (!setting.check_update) { // 캐시를 사용하지 않는 경우 실제로 가져온 데이터를 보내준다
          return [];
        }
        var cnt = await coll.find({'code': code}).count();

        // prepare에 기록한다. investing 등은 diff 계산을 위해 sd 가 cfg.sd 가 아닌 이상 실제로 값이 있는 sd를 넣는다 -> ?
        var pp_sd = need_sd < pp['sd'] ? need_sd : pp['sd'];
        // cfg.sd 가 아닌 이상 이전 데이터를 계속 가져오려 해서 일단은 비활성화(ex: 2010까지만 있는 데이터를 2001년부터 가져오려 하면 계속 시도)
        var pp_ed = need_ed > pp['ed'] && idxs.length ? need_ed : pp['ed'];
        var last_row = await coll.find({'code': code}).project({_id:0}).sort({d: -1}).limit(1).toArray();
        if (last_row.length) {
          /*
            긴 연휴 중간에 가져오려고 시도할 경우 prepare ed 가 앞으로 생길 수 있는 데이터보다 클 경우가 있을 수 있다.
            매번 최신의 데이터를 위해 가져오게 되더라도 ed는 보수적으로 지정한다.
            대신 last_update 와의 차이를 가지고 업데이트 할 지를 판단한다.
          */
          pp_ed = last_row[0]['d'];
        }
        await db.fin.collection('prepare').updateOne({'code':code, 'src':src},
          {'$set': {'sd': pp_sd, 'ed': pp_ed, 'cnt': cnt, 'last_update': now.toDate(), 'last_row':last_row},
           '$push': {'log': {'$each':[{'sd': need_sd, 'ed': need_ed, 'res_sd': res_sd, 'res_ed': res_ed, 'res_cnt':res_cnt, 'save_time': now.toDate()}], '$slice':-50}}});
      }
      var res = await coll.find({code:code, d:{'$gte': sd, '$lte': ed}}).project({_id:0}).toArray();
      //dlog('res 0', res[0]);
      return res;
    }
  } else {
    //dlog('fetch init data');
    // 없는 경우 우선 주어진 기간을 받는다
    var idxs = await cfg.func(code, sd, ed, setting);
    var res_sd, res_ed, res_cnt = 0;
    if (idxs && idxs.length) {
      // investing이고 code가 있고 한국주식 이라면 주말을 제거한다
      if (src == 'investing') {
        var info = await db.fin.collection('code').find({src:src, code:code, type:{$in:['equities','etf']}, 'org.country_ID':11}).toArray();
        if (info.length) { // 한국주식이다
          idxs = idxs.filter(e=>![0,6].includes(moment(e.d).day())); // 일(0), 토(6) 제외
        }
      }
      idxs.sort((a,b) => (a.d - b.d));
      var days = idxs.map(e => e.d);
      res_sd = Math.min.apply(null, days);
      res_ed = Math.max.apply(null, days);
      res_cnt = days.length;

      // prepare만 없는 경우에 대비해서 해당 code의 데이터를 날린다.
      await coll.deleteMany({'code': code});
      await coll.insertMany(idxs) // idxs에 _id가 추가된다
      idxs.forEach(e => {delete e._id});
      
      var pp_ed = idxs[idxs.length - 1]['d'];
      await db.fin.collection('prepare').updateOne({'code': code, 'src': src},
          {'$set': {'sd': sd, 'ed': pp_ed, 'cnt': idxs.length, 'last_update': now.toDate(), 'last_row':idxs[idxs.length-1]},
           '$push': {'log': {'$each':[{'sd': sd, 'ed': ed, 'res_sd': res_sd, 'res_ed': res_ed, 'res_cnt':res_cnt, 'save_time': now.toDate()}], '$slice':-50}}}, {upsert:true});
      //dlog('init data writed');
    }
    return idxs;
  }
}

async function prepare_detail(src, code, st, et, resolution) {
  /*
    주어진 code 와 주어진 기간에 따른 데이터를 준비한다.
    prepare의 detail 버전이다.
    최소 분단위의 데이터를 가져올 수 있으며, 1분, 15분 등의 resolution은 src에 따라 상이하다.

    @args src[str]`investing` 가격을 가져오려는 source. source:code 형태로 사용할 수도 있다.
    @args code[str] 각 데이터 소스별 구분코드
    @args st[int] 시작 unixtime or YYYY-MM-DD HH:mm:ss(tz)
    @args et[int] 종료 unixtime or YYYY-MM-DD HH:mm:ss(tz)
    @args resolution[int or str=1] 데이터의 최소단위. 분단위의 int or D, W, M.
    @args tz[str or tzinfo=KR] 'KR' 등의 국가코드, 'Asia/Seoul' 등의 타임존, 혹은 pytz.utc 등의 타임존 객체
    @args check_update[bool=True] True일 경우 업데이트 시간을 고려해서 새 데이터를 가져올지를 결정한다.
    @kwargs *[*] 추가 인자
  */
  var check_update = true;
  var CONFIG = {
    'investing': {st:54000, func:inv_detail, check_update_minute:{1:5, 5:10, 15:15, 30:20, 60:20, 300:60, 'D':240, 'W':240, 'M':1440}[resolution]}
  }
  var cfg = CONFIG[src];
  code = code + '';

  var now = moment();
  var coll = db.fin.collection(src + '_detail');
  st = st || cfg.st;
  et = et || parseInt(new Date/1000);
  resolution = resolution && resolution-0 || 1;
  
  // prepare 상에서 st, et 포함 여부를 확인한다.
  // prepare가 최신 상태로 업데이트 되고 있음을 보장받는다면 et는 prepare보다 커도 무시할 수 있다
  var rows = await db.fin.collection('prepare_detail').find({code:code, src:src, resolution:resolution}).toArray();
  if (rows.length) {
    var pp = rows[0];
    
    if (pp['st'] <= st && (pp['et'] > et || check_update && pp['last_update'] > now - cfg.check_update_minute * 60 * 1000)) {
      //log('return cached data');
      //return await coll.find({code:code, resolution:resolution, time:{'$gte': st, '$lte': et}}).toArray();
      return 0;
    } else {
      // 보강이 필요하다
      var resCode = 1;
      var need_st = pp['et'];
      var need_et = pp['st'];
      if (pp['st'] > st)
        need_st = st;
      if (pp['et'] <= et && (!check_update || pp['last_update'] < now - cfg.check_update_minute * 60 * 1000))
        need_et = et;
      
      if (need_st <= need_et) { // 가져올 필요가 있을 경우만
        //log('need to fetch fresh data');
        var idxs = await cfg.func(code, need_st, need_et, resolution);
        var res_st, res_et, res_cnt = 0;
        if (idxs && idxs.length) {
          await coll.deleteMany({'code': code, 'resolution': resolution, 'time': {'$gte': need_st, '$lte': need_et}});
          await coll.insertMany(idxs);
          var times = idxs.map(e => e.time);
          res_st = Math.min.apply(null, times);
          res_et = Math.max.apply(null, times);
          res_cnt = times.length;
        }
        var cnt = await coll.find({'code': code, 'resolution': resolution}).count();

        // prepare에 기록한다. investing 등은 diff 계산을 위해 st 가 cfg.st 가 아닌 이상 실제로 값이 있는 st를 넣는다 -> ?
        var pp_st = need_st < pp['st'] ? need_st : pp['st'];
        // cfg.st 가 아닌 이상 이전 데이터를 계속 가져오려 해서 일단은 비활성화(ex: 2010까지만 있는 데이터를 2001년부터 가져오려 하면 계속 시도)
        var pp_et = need_et > pp['et'] && idxs.length ? need_et : pp['et'];
        var last_row = await coll.find({'code': code, 'resolution': resolution}).sort({time: -1}).limit(1).toArray();
        if (last_row.length) {
          /*
            긴 연휴 중간에 가져오려고 시도할 경우 prepare et 가 앞으로 생길 수 있는 데이터보다 클 경우가 있을 수 있다.
            매번 최신의 데이터를 위해 가져오게 되더라도 et는 보수적으로 지정한다.
            대신 last_update 와의 차이를 가지고 업데이트 할 지를 판단한다.
          */
          pp_et = last_row[0]['time'];
        }
        await db.fin.collection('prepare_detail').updateOne({'code': code, 'src': src, 'resolution': resolution},
          {'$set': {'st': pp_st, 'et': pp_et, 'cnt': cnt, 'last_update': now.toDate(), 'last_row':last_row},
           '$push': {'log': {'$each':[{'st': need_st, 'et': need_et, 'res_st': res_st, 'res_et': res_et, 'res_cnt':res_cnt, 'save_time': now.toDate()}], '$slice':-50}}});
        //log('data writed');
        resCode = 2;
      }
      //var res = await coll.find({code:code, resolution:resolution, time:{'$gte': st, '$lte': et}}).toArray();
      //return res;
      return resCode;
    }
  } else {
    var resCode = 3;
    //log('fetch init data');
    // 없는 경우 우선 주어진 기간을 받는다
    var idxs = await cfg.func(code, st, et, resolution);
    var res_st, res_et, res_cnt = 0;
    if (idxs && idxs.length) {
      idxs.sort((a,b) => (a.time - b.time));
      var times = idxs.map(e => e.time);
      res_st = Math.min.apply(null, times);
      res_et = Math.max.apply(null, times);
      res_cnt = times.length;

      // prepare만 없는 경우에 대비해서 해당 code의 데이터를 날린다.
      await coll.deleteMany({'code': code, 'resolution': resolution});
      await coll.insertMany(idxs) // idxs에 _id가 추가된다
      idxs.forEach(e => {delete e._id});

      var pp_et = idxs[idxs.length - 1]['time'];
      await db.fin.collection('prepare_detail').updateOne({'code': code, 'src': src, 'resolution': resolution},
          {'$set': {'st': st, 'et': pp_et, 'cnt': idxs.length, 'last_update': now.toDate(), 'last_row':idxs[idxs.length-1]},
           '$push': {'log': {'$each':[{'st': st, 'et': et, 'res_st': res_st, 'res_et': res_et, 'res_cnt':res_cnt, 'save_time': now.toDate()}], '$slice':-50}}}, {upsert:true});
      //log('init data writed');
      resCode = 4;
    }
    //return idxs;
    return resCode;
  }
}

async function inv_history(pid, sd, ed, setting) {
  /*
    주어진 pid 와 주어진 기간에 따른 데이터를 반환한다
    
    5000 row의 한도가 있으므로 그 이상의 데이터를 가져올 경우 반복하여 호출해야 한다
    
    @args pid[int or string] investing의 pair ID
    @args sd[str] YYYYMMDD 형식의 시작일자
    @args ed[str] YYYYMMDD 형식의 종료일자
    @args setting
      proxy[str] 이용할 프록시 서버
  */
  sd = sd || '19700101'
  ed = ed || moment().format('YYYYMMDD');
  pid = pid + ''
  setting = setting || {}
  
  // YYYY/MM/DD 형식으로 바꾼다
  sd = sd.substring(4,6)+'/'+sd.substring(6,8)+'/'+sd.substring(0,4);
  ed = ed.substring(4,6)+'/'+ed.substring(6,8)+'/'+ed.substring(0,4);
  
  async function req_until(sd, ed) {
    var res = await P('https://www.investing.com/instruments/HistoricalDataAjax', 'action=historical_data&curr_id='+pid+'&st_date='+sd+'&end_date='+ed+'&interval_sec=Daily', INVESTING_POST_HEADERS_EN, setting.proxy);
    // 날짜, 현재가, 오픈, 고가, 저가, 거래량
    var table = slice(res, 'id="curr_table"', '<\\/table');
    if (!table) {
      dlog('https://www.investing.com/instruments/HistoricalDataAjax', 'action=historical_data&curr_id='+pid+'&st_date='+sd+'&end_date='+ed+'&interval_sec=Daily');
      dlog('table not found', res.length, res.substring(0, 1000));
      return [];
    }
    var tbody = slice(table, '<tbody', '<\\/tbody');
    if (!tbody) {
      dlog('tbody not found', res.length, res.substring(0, 1000));
      return [];
    }
    var trs = split(tbody, '<tr', '<\\/tr');
    if (!~tbody.indexOf('data-real-value')) {
      //raise Exception('no data found')
      dlog('tbody has no data-real-value', pid, sd, ed);
      return [];
    }
    
    dlog(`pid:${pid}, ${sd} ~ ${ed}, response byte:${res.length}, trs records:${trs.length}`);
    
    var recent_trs = [];
    if (trs.length == 4999) {
      var next_sd = moment((+slice(trs[0], 'data-real-value="', '"') + 24 * 3600) * 1000).format('MM/DD/YYYY');
      recent_trs = await req_until(next_sd, ed);
    }
    return recent_trs.concat(trs);
  }
  
  var trs = await req_until(sd, ed);
  
  var rows = [];
  trs.forEach(tr => {
    try {
      var time = slice(tr, 'data-real-value="', '"');
      var d = moment(time * 1000).format('YYYYMMDD');
    } catch(e) {
      dlog(pid, 'has no time');
      return;
    }
    if (slice(tr, 'data-real-value="', '"', 2).replace(/,/g,'') == '') { // close
      // https://www.investing.com/equities/sungwoo-hitech-co-ltd-historical-data
      // 2018.03 참조, 값이 비어있는 경우가 있다
      return;
    }

    var row = {
      code:pid,
      d: d,
      close: +slice(tr, 'data-real-value="', '"', 2).replace(/,/g,''),
      open: +slice(tr, 'data-real-value="', '"', 3).replace(/,/g,''),
      high: +slice(tr, 'data-real-value="', '"', 4).replace(/,/g,''),
      low: +slice(tr, 'data-real-value="', '"', 5).replace(/,/g,''),
      y: +slice(tr, '<td.*?class="bold.*?>', '<').replace('%','').replace(',',''),
      volume: slice(tr, 'data-real-value="', '"', 6)
    }
    if (row.volume) {
      row.volume = +row.volume.replace(/,/g,'');
    }
    rows.push(row);
  });
  rows.sort((a,b) => a.d-b.d);
  
  var bef_close = null;
  rows.forEach(r => {
    bef_close = bef_close == null ? r.close / (1 + r.y/100.0) : bef_close;
    r.diff = round(r.close - bef_close, 6)
    r.y = bef_close == null ? null : r.diff / bef_close * 100;
    bef_close = r.close;
  });
  
  return rows
}

async function inv_detail(pid, st, et, resolution) {
  /*
    @args pid[int or str] pid
    @args st[*int] 시작 unixtime or YYYY-MM-DD HH:mm:ss(tz)
    @args et[*int] 종료 unixtime or YYYY-MM-DD HH:mm:ss(tz)
    @args resolution[int or str=1] 해상도, DWM or 분단위 숫자
    @args tz[*str or tzinfo=KR] 'KR' 등의 국가코드, 'Asia/Seoul' 등의 타임존, 혹은 pytz.utc 등의 타임존 객체
  */
  var updateLast = et == null || et > parseInt(new Date/1000) - 60;
  et = et || parseInt(new Date/1000);
  resolution = resolution && resolution-0 || 1;
  var range = resolution, et_chunk = et, st_chunk, data = [], t = +new Date;
  st_chunk = st = st || et - range * 60 * 5000;
  
  
  while (true) {
    var res = await inv_price_stream(pid, st_chunk, et_chunk, resolution);
    if (res && st) {
      data = res.concat(data);
      et_chunk = res[0]['time'] - range * 60;
      st_chunk = et_chunk - range * 60 * 5000;
      if (st_chunk < st) {
        st_chunk = st;
      }
    } else {
      break;
    }
  }
  
  // dlog(`investing [${codeMap[pid] && codeMap[pid].name || pid}], pid: ${pid}, resolution: ${resolution} min, elapsed: ${((+new Date - t)/1000).toFixed(1)} s`);
  
  return data.filter(e => e['close'] != 0);
}

async function inv_price_stream(pid, st, et, resolution, cb) {
  /*
    streaming chart 데이터를 이용하여 분단위 데이터를 가져온다.

    단위는 1, 5, 15, 30, 60, 300, D, W, M 등이 있다.
    45는 15를 이용하고 2h, 4h는 1h를 이용하여 재편집한다.
    한 번에 최대 5000개까지 가져올 수 있다. 다음 데이터는 시간을 조절해서 가져와야 한다.

    휴장시간 등에 따라 min, hour, day 데이터에는 gap이 발생할 수 있다.
    1, 5분의 경우 10일 정도의 데이터를 받을 수 있다.
    15분은 1년, 30분은 약 584일, 1시간은 640일의 데이터가 있다.
    5시간 부터는 양상이 바뀌어, snp500은 약 9년, kospi는 약 5년의 데이터를 보여준다.
    D, W, M 의 경우는 가지고 있는 historical data와 비슷하게 나온다.

    데이터는 {
    t:[], # timestamp
    c:[], # close
    o:[], # open
    h:[], # high
    l:[], # low
    v:[], # volume
    vo:[], # volume 누적, 무시한다
    } 형태이다.
  */
  var range = resolution, bef_et = et - range * 60 * 5001, st = st < bef_et ? bef_et : st;
  var url = 'https://tvc4.forexpros.com/f161465393cd871032f6582d21b8cd05/1510577317/1/1/8/history?' + `symbol=${pid}&resolution=${resolution}&from=${parseInt(st)}&to=${parseInt(et)}`;
  var j = await J(url, INVESTING_GET_HEADERS_JSON_KO);

  if (typeOf(j) == 'array' || j['s'] == 'no_data' || !j['t']) {
    return null; // no data
  }
  
  var data = [], code = pid + '';
  j['t'].forEach((t, i) => {
    data.push({code:code, time:t, kst:moment.tz(t*1000, 'Asia/Seoul').format('YYYYMMDD HHmmss'), resolution:resolution, close:j['c'][i], open:j['o'][i], high:j['h'][i], low:j['l'][i], volume:j['v'][i]})
  });

  return data
}





/**
  util part
**/

async function G(url, headers, proxy) {
  return requestWithOption({url:url, headers:headers, proxy:proxy});
}

async function GE(url, headers, proxy) {
  var data = await requestWithOption({url:url, headers:headers, encoding:null, proxy:proxy});
  return iconv.decode(data, 'euc-kr');
}

async function J(url, headers, proxy){
  var res = await G(url, headers, proxy);
  try {
    return JSON.parse(res);
  } catch(e) {
    throw new Error('on [J] result is not json');
  }
}

async function P(url, params, headers, proxy){
  // 기본적으로 params 형태는 query string(a=b)
  if (typeOf(params) == 'object') { // object 라면 변경해준다.
    params = items(params).map(([k,v])=>k+'='+encodeURIComponent(v)).join('&');
  }
  return requestWithOption({method:'POST', url:url, body:params, headers:headers, proxy:proxy});
}

function requestWithOption(option) {
  return new Promise((resolve, reject) => {
    var options = {gzip:true, timeout:10000};
    Object.assign(options, option || {});
    if (options.proxy) {
      if (options.proxy.substring(0,7) != 'http://' || options.proxy.substring(0,8) != 'https://') {
        options.proxy = 'http://' + options.proxy; // default to http proxy
      }
    }
    request(options, (error, response, data) => {
      if (error) {
        if (options.proxy) {
          // 프록시가 있다면 1회 더 시도
          request(options, (error, response, data) => {
            if (error) reject(error);
            else resolve(data);
          });
        } else {
          reject(error);
        }
      }
      else resolve(data)
    });
  });
}

async function timeoutRequest(option, timeout) {
  var [data, success, elapsed] = await Promise.race([
    new Promise((res, rej)=>{
      var st = +new Date;
      request(option, (error, response, data) => {
        if (error) rej(error);
        else res([data, true, +new Date - st]);
      });
    }),
    new Promise((res, rej) => setTimeout(() => res([null, false, timeout]), timeout))
  ]);
  return [data, success, elapsed];
}

function sleep(s) {
  return new Promise(function (resolve, reject) {
    setTimeout(resolve, s * 1000);
  });
}

// object의 value array
function values(o){return Object.keys(o).map(function(k){return o[k]})};

// console.log의 실행위치와 함께 로그를 남긴다
function log() {
  console.log.apply(console, values(arguments));
  //console.log.apply(console, values(arguments).concat([new Error().stack.split('\n')[2].trim()]));
}
function dlog() {
  debug && console.log.apply(console, values(arguments));
}

// 종류 체크. string, number, boolean, array, object, class name의 lower case 등을 반환
function typeOf(v) {
  var t = typeof v;
  if (t == 'object') {
    t = Object.prototype.toString.call(v).match(/\[object (.*)\]/)[1].toLowerCase();
  }
  return t;
}

// Object.keys 는 string으로만 key를 반환하므로 주의해야 한다. 물론 기본적인 Object도 key에 숫자를 넣으면 스트링화 되기는 마찬가지이다.
// array(list) to object(dict). arr은 [[key,value],...] 형태이다
function dict(arr){var o={};arr.forEach(function(e){ if (typeOf(e) == 'array') o[e[0]]=e[1] });return o};

// object(dict) to array(list). return은 [[key,value],...] 형태이다
function items(o){return Object.keys(o).map(function(k){return [k, o[k]]})};

// object의 key array
function keys(o){return Object.keys(o)};

// object의 value array
function values(o){return Object.keys(o).map(function(k){return o[k]})};

// object, arguments의 array화
function list(o){if(typeOf(o)=='object') return keys(o); else return values(o);}

// 줄바꿈을 포함하여, str에서 pre(some)post 를 찾아서 some 만 반환한다. 1부터 시작하는 index 지정이 가능하다.
function slice(str, pre, post, idx) {
  var reg = eval("/"+pre+"([\\s\\S]*?)"+post+"/g");
  var s = str.match(reg);
  idx = idx?idx:1;
  if (s && s.length >= idx) return s[idx-1].replace(reg, "$1");
  else return null;
}

// 줄바꿈을 포함하여, str에서 pre(some)post 를 찾아서 some의 묶음을 반환한다.
function split(str, pre, post) {
  var reg = eval("/"+pre+"([\\s\\S]*?)"+post+"/g");
  var arr = str.match(reg);
  if (arr)
    for (var i=0; i<arr.length; i++)
      arr[i] = arr[i].replace(reg, "$1");
  return arr;
}

// 반올림
function round(n, pos) {
  if (!pos) pos = 0;
  var digits = Math.pow(10, pos),
      num = Math.round(n * digits) / digits;
  return num;
}

async function worker(work, feed, size, no, setting) {
  /*
    single thread 기준의 worker 패턴.
    아래와 같이 사용한다.
    
    const workerCnt = 5, q = Array.apply(null, Array(10)).map((e,i)=>i);
    async function work(no) { return no * no; }
    var pool = Array.apply(null, Array(workerCnt)).map(e=>worker(work, q));
    
    var st = +new Date;
    var results = await Promise.all(pool);
    var [done, err] = results.map(r=>[r.done, r.err]).reduce((a,b)=>{return [a[0].concat(b[0]), a[1].concat(b[1])]}, [[],[]]);
    
    log(`worker jobs - done:${done.length}, err:${err.length}`);
    log(`now: ${moment().format("YYYY-MM-DD HH:mm:ss")}, total elapsed: ${((+new Date - st)/1000).toFixed(1)} s`);
  */
  var result = {
    done: [],
    err: [],
  }, job, proxy = setting && setting.proxies && setting.proxies[no] || undefined;
  while ((job = feed.shift()) != null) {
    var t = +new Date;
    try {
      if (setting && setting.timeout) {
        try {
          var res = await Promise.race([
            work(job, feed.length, size, no, proxy, setting),
            new Promise((res, rej) => setTimeout(() => rej(new Error('timeout '+setting.timeout)), setting.timeout * 1000))
          ]);
        } catch(e) {
          result.err.push({job:job, err:e, elapsed: ((+new Date - t)/1000), proxy:proxy});
          continue;
        }
      } else {
        var res = await work(job, feed.length, size, no, proxy, setting);
      }
      result.done.push({job:job, res:res, elapsed: ((+new Date - t)/1000)});
    } catch(e) {
      result.err.push({job:job, err:e, elapsed: ((+new Date - t)/1000), proxy:proxy});
    }
  }
  return result;
}

async function workerTest() {
  const workerCnt = 5, q = Array.apply(null, Array(10)).map((e,i)=>i);
  async function work(no) { if (no == 5) throw new Error('num is 5'); return no * no; }
  var pool = Array.apply(null, Array(workerCnt)).map(e=>worker(work, q));
  
  var st = +new Date;
  var results = await Promise.all(pool);
  var [done, err] = results.map(r=>[r.done, r.err]).reduce((a,b)=>{return [a[0].concat(b[0]), a[1].concat(b[1])]}, [[],[]]);
  
  log(`worker jobs - done:${done.length}, err:${err.length}`);
  log(`now: ${moment().format("YYYY-MM-DD HH:mm:ss")}, total elapsed: ${((+new Date - st)/1000).toFixed(1)} s`);
  
  var res = done.map(e=>e.res);
  log('result:', res);
  if (err.length) console.log(err);
}
async function workerPool(work, feed, workerCnt, setting) {
  /*
    좀 더 간략화한 worker 패턴.
    
    async function work(no) { return no * no; }
    workerPool(work, [1,2,3,4,5], 2).then(e=>{console.log(e.done.length, e.err.length)});
  */
  var size = feed.length;
  var pool = Array.apply(null, Array(workerCnt)).map((e,i)=>worker(work, feed, size, i, setting));
  var st = +new Date;
  var results = await Promise.all(pool);
  var [done, err] = results.map(r=>[r.done, r.err]).reduce((a,b)=>{return [a[0].concat(b[0]), a[1].concat(b[1])]}, [[],[]]);
  var res = done.map(e=>e.res);
  return {done:done, err:err, res:res, elapsed:(+new Date - st)/1000};
}
async function doWork(work, feed, setting) {
  /*
    로그를 포함한 최종 worker 패턴
    
    doWork(async no=>no+1, [1,2,3,4,5], {worker:2});
  */
  if (typeof(feed) == 'number') feed = Array.apply(null, Array(feed)).map((e,i)=>i); // 숫자만 준 경우 [0, 1, 2, ..] 인 array 생성
  setting = Object.assign({
      id:'',
      worker:10,
      size:feed.length,
      timeout:null,
      callback:null,
      onerror:(err,s)=>{console.log(err.slice(0,3), err.length, 'errors' + (s.id?' with '+s.id:''))},
      fLog:(res,s)=>{log(`${s.id?s.id+' ':''}${moment().format("YYYY-MM-DD HH:mm:ss")}] done:${res.done.length}, err:${res.err.length}, elapsed:${res.elapsed.toFixed(1)} s`);}
    }, setting && typeOf(setting) == 'object' ? setting : {});
  if (setting.proxies) {
    if (typeof(setting.proxies) == 'number') {
      setting.proxies = (await getProxies(setting.proxies)).map(e=>e.host);
    }
    if (setting.timeout == null) {
      setting.timeout = 20; // proxy를 사용하는 경우 timeout은 기본 20초
    }
    setting.worker = setting.proxies.length;
  }
  var res = await workerPool(work, feed.slice(), setting.worker, setting);
  
  if (res.err.length && setting.retry) { // 실패한 작업을 다시 진행한다
    var tried = 0, retryHistory = [{proxy:setting.proxies.length, err:res.err.length}];
    while (setting.retry > tried++ && res.err.length) {
      if (setting.proxies) { // 실패한 프록시는 제거한다
        var orgProxies = setting.proxies.slice(), i;
        res.err.forEach(p=>{
          if ((i = setting.proxies.indexOf(p.proxy)) > -1) {
            setting.proxies.splice(i, 1);
          }
        });
        if (setting.proxies.length == 0) { // 프록시가 없으면 원래의 것으로 진행
          dlog(`no proxy left, recover original list`);
          setting.proxies = orgProxies;
        }
      }
      failedJob = res.err.map(e=>e.job);
      setting.tried = tried;
      var retryRes = await workerPool(work, failedJob, setting.proxies.length, setting);
      res.done = res.done.concat(retryRes.done);
      res.err = retryRes.err;
      res.res = res.done.map(e=>e.res);
      res.elapsed += retryRes.elapsed;
      retryHistory.push({proxy:setting.proxies.length, err:res.err.length});
    }
    
    log(`${setting.id || 'doWork'} retry history : ` + retryHistory.map(e=>`${e.proxy} proxy ${e.err} err`).join(' => '));
  }
  
  setting.onerror && res.err.length && setting.onerror(res.err, setting);
  setting.callback && setting.callback(res, setting);
  setting.fLog && setting.fLog(res, setting);
  return res;
}
async function doWorkTest() {
  await doWork(async no=>{
    if (no>2) {
      return no+1;
    } else throw new Error('error!');
  }, [1,2,3,4,5], {id:'doWorkTest', proxies:10, timeout:10, retry:2});
}



/**
  const, init, main
**/

const INVESTING_GET_HEADERS_JSON_KO = {
  'Accept': 'application/json, text/javascript, */*; q=0.01',
  'Accept-Language': 'ko,en-US;q=0.9,en;q=0.8',
  'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0',
  'Accept-Encoding': 'gzip, deflate, text/plain',
  'Referer': 'https://kr.investing.com/',
  'X-Requested-With': 'XMLHttpRequest',
}
const INVESTING_GET_HEADERS_EN = {
  'Host': 'www.investing.com',
  'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36',
  'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
  'Accept-Encoding': 'gzip, deflate, br',
  'Accept-Language': 'en-US;q=0.9,en;q=0.8',
}
const INVESTING_GET_HEADERS_KO = {
  'Host': 'kr.investing.com',
  'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36',
  'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
  'Accept-Encoding': 'gzip, deflate, br',
  'Accept-Language': 'ko,en-US;q=0.9,en;q=0.8',
}
const INVESTING_POST_HEADERS_EN = {
  'X-Requested-With': 'XMLHttpRequest',
  'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0',
  'Content-Type': 'application/x-www-form-urlencoded',
  'Accept-Encoding': 'gzip, deflate, br',
  'Referer': 'https://www.investing.com/'
}
const INVESTING_POST_HEADERS = {
  'X-Requested-With': 'XMLHttpRequest',
  'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0',
  'Content-Type': 'application/x-www-form-urlencoded',
  'Accept-Encoding': 'gzip, deflate, br',
  'Referer': 'https://kr.investing.com/'
}
const INVESTING_POST_HEADERS_JSON_EN = {
  'Host': 'www.investing.com',
  'Accept': 'application/json, text/javascript, */*; q=0.01',
  'Origin': 'https://www.investing.com',
  'X-Requested-With': 'XMLHttpRequest',
  'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Mobile Safari/537.36',
  'Content-Type': 'application/x-www-form-urlencoded',
  'Referer': 'https://www.investing.com',
  'Accept-Encoding': 'gzip, deflate, br',
  'Accept-Language': 'en-US;q=0.9,en;q=0.8',
}
const INVESTING_POST_HEADERS_JSON_KO = {
  'Host': 'kr.investing.com',
  'Accept': 'application/json, text/javascript, */*; q=0.01',
  'Origin': 'https://kr.investing.com',
  'X-Requested-With': 'XMLHttpRequest',
  'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Mobile Safari/537.36',
  'Content-Type': 'application/x-www-form-urlencoded',
  'Referer': 'https://kr.investing.com',
  'Accept-Encoding': 'gzip, deflate, br',
  'Accept-Language': 'ko,en-US;q=0.9,en;q=0.8',
}
const INVESTING_POST_HEADERS_JSON_HK = {
  'Host': 'hk.investing.com',
  'Accept': 'application/json, text/javascript, */*; q=0.01',
  'Origin': 'https://hk.investing.com',
  'X-Requested-With': 'XMLHttpRequest',
  'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Mobile Safari/537.36',
  'Content-Type': 'application/x-www-form-urlencoded',
  'Referer': 'https://hk.investing.com',
  'Accept-Encoding': 'gzip, deflate, br',
  'Accept-Language': 'hk,en-US;q=0.9,en;q=0.8',
}
const INVESTING_POST_HEADERS_JSON_CN = {
  'Host': 'cn.investing.com',
  'Accept': 'application/json, text/javascript, */*; q=0.01',
  'Origin': 'https://cn.investing.com',
  'X-Requested-With': 'XMLHttpRequest',
  'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Mobile Safari/537.36',
  'Content-Type': 'application/x-www-form-urlencoded',
  'Referer': 'https://cn.investing.com',
  'Accept-Encoding': 'gzip, deflate, br',
  'Accept-Language': 'zh-cn,en-US;q=0.9,en;q=0.8',
}
const MIRAE_POST_HEADERS = {
  'Accept': 'text/plain, */*; q=0.01',
  'Origin': 'https://www.miraeassetdaewoo.com',
  'X-Requested-With': 'XMLHttpRequest',
  'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36',
  'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
  'Referer': 'https://www.miraeassetdaewoo.com/advancement/jsp/trading/wtsView.jsp?readyToMdi=&readyToStc=&dev=&headChk=&readyToTabs=',
  'Accept-Encoding': 'gzip, deflate, br',
  'Accept-Language': 'ko,en-US;q=0.9,en;q=0.8',
}
const NAVER_GET_HEADERS = {
  'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
  'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36',
  'Referer': 'http://finance.naver.com/',
  'Accept-Encoding': 'gzip, deflate',
  'Accept-Language': 'ko,en-US;q=0.9,en;q=0.8',
}
async function initDB() {
  var conn = await mongodb.MongoClient.connect('mongodb://db.qara.kr/');
  db.fin = conn.db('fin');
  db.meta = conn.db('kosho_meta');
  db.proxy = conn.db('proxy');
}

async function main() {
  //node의 console 모드와 파일 실행 모드는 global(=this) 객체가 다르다. 파일 실행시는 module 형태로 실행되고 global은 비어있게 된다.
  //console.log(Object.keys(global).filter(e=>{return typeof(global[e])=='function'}));
  await initDB();
  
  var func = process.argv[2], args = process.argv.slice(3), quite = false, res = null;
  if (~args.indexOf('-q')) { args.splice(args.indexOf('-q'), 1); quite = true; }
  if (~args.indexOf('-d')) { args.splice(args.indexOf('-d'), 1); debug = true; }
  if (func && typeof(eval(func)) == 'function') {
    log(`Run ${func} with [${args}]`);
    res = await eval(func).apply(null, args);
  } else {
    if (func) {
      log(`func ${func} : ${eval(func)}`)
    }
    log('Usage: node fin.js function_name');
  }
  return quite ? null : res;
}


process.on('uncaughtException', function (err) {
  // socket error일 때 강제로 프로세스가 종료되는 것을 방지
  // https://stackoverflow.com/questions/17245881/node-js-econnreset
  console.error(err.stack);
  //console.log("Node NOT Exiting...");
});
main().then(e => { e != null && console.log(e); process.exit(0); }).catch(e => {console.log('err:', e); process.exit(0);});
