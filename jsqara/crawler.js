const fs = require('fs');
const axios = require('axios');
const mongodb = require('mongodb');
const moment = require('moment-timezone');
const request = require("request");
const iconv = require('iconv-lite');
const charset = require('charset');

var db = {}, debug = false; // debug - 실행시 -d 옵션, dlog 에만 영향을 미침

const INVESTING_GET_HEADERS_JSON_KO = {
  'Accept': 'application/json, text/javascript, */*; q=0.01',
  'Accept-Language': 'ko,en-US;q=0.9,en;q=0.8',
  'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0',
  'Accept-Encoding': 'gzip, deflate, text/plain',
  'Referer': 'https://kr.investing.com/',
  'X-Requested-With': 'XMLHttpRequest',
}
const INVESTING_GET_HEADERS_EN = {
  'Host': 'www.investing.com',
  'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36',
  'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
  'Accept-Encoding': 'gzip, deflate, br',
  'Accept-Language': 'en-US;q=0.9,en;q=0.8',
}
const INVESTING_GET_HEADERS_KO = {
  'Host': 'kr.investing.com',
  'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36',
  'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
  'Accept-Encoding': 'gzip, deflate, br',
  'Accept-Language': 'ko,en-US;q=0.9,en;q=0.8',
}
const INVESTING_POST_HEADERS_EN = {
  'X-Requested-With': 'XMLHttpRequest',
  'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0',
  'Content-Type': 'application/x-www-form-urlencoded',
  'Accept-Encoding': 'gzip, deflate, br',
  'Referer': 'https://www.investing.com/'
}
const INVESTING_POST_HEADERS = {
  'X-Requested-With': 'XMLHttpRequest',
  'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0',
  'Content-Type': 'application/x-www-form-urlencoded',
  'Accept-Encoding': 'gzip, deflate, br',
  'Referer': 'https://kr.investing.com/'
}
const INVESTING_POST_HEADERS_JSON_EN = {
  'Host': 'www.investing.com',
  'Accept': 'application/json, text/javascript, */*; q=0.01',
  'Origin': 'https://www.investing.com',
  'X-Requested-With': 'XMLHttpRequest',
  'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Mobile Safari/537.36',
  'Content-Type': 'application/x-www-form-urlencoded',
  'Referer': 'https://www.investing.com',
  'Accept-Encoding': 'gzip, deflate, br',
  'Accept-Language': 'en-US;q=0.9,en;q=0.8',
}
const INVESTING_POST_HEADERS_JSON_KO = {
  'Host': 'kr.investing.com',
  'Accept': 'application/json, text/javascript, */*; q=0.01',
  'Origin': 'https://kr.investing.com',
  'X-Requested-With': 'XMLHttpRequest',
  'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Mobile Safari/537.36',
  'Content-Type': 'application/x-www-form-urlencoded',
  'Referer': 'https://kr.investing.com',
  'Accept-Encoding': 'gzip, deflate, br',
  'Accept-Language': 'ko,en-US;q=0.9,en;q=0.8',
}
const INVESTING_POST_HEADERS_JSON_HK = {
  'Host': 'hk.investing.com',
  'Accept': 'application/json, text/javascript, */*; q=0.01',
  'Origin': 'https://hk.investing.com',
  'X-Requested-With': 'XMLHttpRequest',
  'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Mobile Safari/537.36',
  'Content-Type': 'application/x-www-form-urlencoded',
  'Referer': 'https://hk.investing.com',
  'Accept-Encoding': 'gzip, deflate, br',
  'Accept-Language': 'hk,en-US;q=0.9,en;q=0.8',
}
const INVESTING_POST_HEADERS_JSON_CN = {
  'Host': 'cn.investing.com',
  'Accept': 'application/json, text/javascript, */*; q=0.01',
  'Origin': 'https://cn.investing.com',
  'X-Requested-With': 'XMLHttpRequest',
  'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Mobile Safari/537.36',
  'Content-Type': 'application/x-www-form-urlencoded',
  'Referer': 'https://cn.investing.com',
  'Accept-Encoding': 'gzip, deflate, br',
  'Accept-Language': 'zh-cn,en-US;q=0.9,en;q=0.8',
}
const MIRAE_POST_HEADERS = {
  'Accept': 'text/plain, */*; q=0.01',
  'Origin': 'https://www.miraeassetdaewoo.com',
  'X-Requested-With': 'XMLHttpRequest',
  'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36',
  'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
  'Referer': 'https://www.miraeassetdaewoo.com/advancement/jsp/trading/wtsView.jsp?readyToMdi=&readyToStc=&dev=&headChk=&readyToTabs=',
  'Accept-Encoding': 'gzip, deflate, br',
  'Accept-Language': 'ko,en-US;q=0.9,en;q=0.8',
}
const NAVER_GET_HEADERS = {
  'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
  'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36',
  'Referer': 'http://finance.naver.com/',
  'Accept-Encoding': 'gzip, deflate',
  'Accept-Language': 'ko,en-US;q=0.9,en;q=0.8',
}


async function invUpdateStockNewsAll(forceAll) {
  /*
  db.investing_news.ensureIndex({code:1});
  db.investing_news.ensureIndex({symbol:1});
  */
  var newsCodes = (await db.fin.collection('investing_news').find({}).toArray()).map(e=>e.code);
  var query = forceAll ? {} : {code:{$nin:newsCodes}};
  var codes = await db.fin.collection('investing_stock').find(query).toArray();
  
  await doWork(async (doc, r, size, no, proxy) => {
    dlog(`${size-r}/${size}) ${doc.flag}, ${doc.name}, ${doc.symbol}, ${doc.link}`);
    await invUpdateStockNews(doc.code, doc.link, 'en', proxy);
    await invUpdateStockNews(doc.code, doc.link, 'ko', proxy);
  }, codes, {id:'invStockNews', proxies:150, retry:2});
}


async function invUpdateStockNews(code, link, lang, proxy) {
  if (!code) return;
  if (!link) {
    link = (await db.fin.collection('investing_stock').findOne({code:code})).link;
  }
  if (~link.indexOf('?')) { // link에 cid가 있는 경우 : /equities/bank-of-china-ss?cid=9217
    link = link.replace('?', '-news?');
  } else {
    link = link + '-news';
  }
  var html = await G('https://'+(lang=='ko'?'kr':'www')+'.investing.com'+link, lang=='ko'?INVESTING_GET_HEADERS_KO:INVESTING_GET_HEADERS_EN, proxy);
  var data = slice(html, '<div class="mediumTitle1">', '<script>'), tags = data ? split(data, '<article.*?>', '<\\/article') : null;
  if (!tags) return;// dlog(`        code ${code} has no news of '${lang}'`);
  var news = tags.map(e=>{
    var n = {};
    n.href = slice(e, '<a href="', '"');
    n.img = slice(e, '<img src="', '"');
    n.title = slice(e, '<a.*?class="title".*?>', '<');
    n.by = slice(e, 'class="articleDetails">.*?<span>', '<').replace('부터 ','');
    n.date = slice(e, 'class="articleDetails">.*?<span class="date">', '<');
    n.date = n.date ? n.date.replace('&nbsp;-&nbsp;', '') : n.date;
    n.detail = slice(e, 'class="articleDetails">[\\s\\S]*?<p>', '<');
    return n;
  });
  //console.log(news);
  var doc = {};
  lang = lang || 'en';
  doc['news_'+lang] = news
  await db.fin.collection('investing_news').updateOne({code:code}, {$set:doc}, {upsert:true});
}

async function checkInvStockLink() {
  /* 
    stock의 link와 code의 url을 비교한다
    
    stock 중복체크:
    var dup = db.investing_stock.aggregate([{$group:{_id:'$code', total:{$sum:1}}}, {$match:{total:{$gte:2}}}]).toArray();
    stock 중복제거:
    dup.forEach(e=>db.investing_stock.deleteOne({code:e._id}));
    // 이후 invMeta, 한글주식->inv 를 다시 실행해야 한다
  */
  var stocks = (await db.fin.collection('investing_stock').find().toArray());
  var codes = (await db.fin.collection('code').find({src:'investing', code:{$in:stocks.map(e=>e.code)}}).toArray());
  var codeMap = dict(codes.map(e=>[e.code, e]));
  
  stocks.forEach(e=>{
    if (codeMap[e.code] && e.link != codeMap[e.code].url) {
      log(`link diffs: code - ${e.code}, url - ${codeMap[e.code].url}, link - ${e.link}`);
    }
  });
}

async function setInvCodeOrgLink() {
  /* 
    code의 url과 org.link를 비교한다
    db.code.find({src:'investing', $where:"this.org && this.url != this.org.link"}).count()
  */
  var codes = (await db.fin.collection('code').find({src:'investing', $where:"this.org && this.url != this.org.link"}).toArray());
  
  Promise.all(codes.map(async doc=>{
    await db.fin.collection('code').updateOne({code:doc.code}, {$set:{url:doc.org.link}})
  }));
  log(`${codes.length} link fixed`);
} 


/**
  naver
**/

async function naverUpdateAll(func) {
  /*
    db.fin.collection('code').find({src:'krx'}).toArray().then(e=>{krx=e});
    db.fin.collection('code').find({src:'investing', type:{$in:['equities','etf']}, 'org.country_ID':11}).toArray().then(e=>{inv=e});
    function sub(a,b){
      var mapB = dict(b.map(e=>[e,1]));
      return keys(dict(a.map(e=>[e,1]))).filter(k=>!mapB[k]);
    }
    var krxOnly = sub(krx.map(e=>e.code), inv.map(e=>e.org.symbol)); // 815
    var invOnly = sub(inv.map(e=>e.org.symbol), krx.map(e=>e.code)); // [ '194860', '232360' ]
  */
  var codes = (await db.fin.collection('code').find({src:'krx'}).toArray()).map(e=>e.code), size = codes.length;
  var f = ({meta:naverUpdateStockMeta, news:naverUpdateStockNews})[func] || naverUpdateStockNews;
  
  async function work(code, remainCnt) {
    dlog(`${size-remainCnt}/${size}) ${code} ${await f(code)}`);
  }
  var res = await workerPool(work, codes, 50);
  log(`naverUpdateAll ${func} ${moment().format("YYYY-MM-DD HH:mm:ss")}] done:${res.done.length}, err:${res.err.length}, elapsed:${res.elapsed.toFixed(1)} s`);
  res.err.length && console.log(res.err.slice(0,10));
}


async function naverUpdateStockNews(code, page) {
  page = page || 1;
  var r = (await GE('http://finance.naver.com/item/news_news.nhn?code='+code+'&page='+page+'&sm=title_entity_id.basic&clusterId=', NAVER_GET_HEADERS)).replace(/\n/g,'');
  var list = slice(r, '<caption>종목뉴스.*?<tbody>', '종목뉴스 끝');
  if (!list) return dlog(`${code} 의 종목뉴스가 없습니다`) || false;
  
  list = list.replace(/<table>.*?<\/table>/g, ''); // 연관기사 제거
  var trs = split(r, '<tr.*?>', '<\\/tr>') || [];
  var news = trs.map(e=>{
    return {
      title:slice(e, '<a.*?target.*?>', '<'),
      href:slice(e, '<a href="', '"'),
      info:slice(e, '<td class="info">', '<'),
      date:slice(e, '<td class="date">', '<'),
    };
  }).filter(e=>e.title);
  if (news) {
    await db.fin.collection('naver_news').updateOne({src:'mirae', code:code}, {$set:{news:news}}, {upsert:true});
  }
  return news.length;
}
