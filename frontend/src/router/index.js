import Vue from 'vue';
import Router from 'vue-router';
import intro from '../components/intro.vue';
import main from '../components/main.vue';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'intro',
      component: intro
    },
    {
      path: '/main',
      name: 'main',
      component: main,
      props: route => ({ code: route.query.code })
    }
  ]
});
