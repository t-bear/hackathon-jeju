// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import App from './App.vue';
import router from './router';
import MLChart from './components/MLChart.vue';

Vue.config.productionTip = false;

Vue.component('ml-chart', MLChart);

Vue.mixin({
  methods: {
    setHighchartDefaultOptions() {
      window.Highcharts.setOptions({
        global: {
          useUTC: false,
        },
      });
    },
    loadHighchartJs(src) {
      return new Promise(((resolve, reject) => {
        const script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = src;
        script.onload = function () {
          resolve();
        };
        document.body.appendChild(script);
      }));
    },
    doneHighcharts (f) {
      if (!window.hasOwnProperty('Highcharts')) {
        this.loadHighchartJs('static/js/highcharts-all-5.0.2.js')
        .then(
          this.loadHighchartJs('static/js/js/highcharts-more.js')
        )
        .then(
          this.loadHighchartJs('static/js/no-data-to-display.js')
        )
        .then(e => {
          this.setHighchartDefaultOptions();
          f();
        });
      } else {
        this.setHighchartDefaultOptions();
        f();
      }
    }
  }
});

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
});
