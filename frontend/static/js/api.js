const BASE_URL = 'http://localhost:5001';
function callGet (url) {
  return $.ajax(BASE_URL + url, {
    type: 'GET',
    dataType: 'json',
    contentType: 'application/json; charset=utf-8',
    xhrFields: { withCredentials: true },
    complete () { }
  });
}
window.API = {
  getTickerPrediction (mdId, ticker) {
    return $.when(callGet(`/api/ticker/report?md_id=${mdId}&ticker=${encodeURIComponent(ticker)}`));
  },
  getNewsSearch (keywords) {
    return $.when(callGet(`/api/news/tag?tags=${encodeURIComponent(keywords)}`));
  },
  getFinSeries (ticker, days) {
    return $.when(callGet(`/api/fin/series?ticker=${ticker}&days=${days || 1095}`));
  },
  getChartSeries (ticker, days, freq) {
    return $.when(callGet(`/api/chart/series?ticker=${ticker}&days=${days}&freq=${freq}`));
  },
  getInvestingLast (codes) {
    return $.when(callGet(`/api/fin/inv_last?codes=${codes}`));
  },
  getFinLast (ticker) {
    return $.when(callGet(`/api/fin/last?ticker=${ticker}`));
  }
};
